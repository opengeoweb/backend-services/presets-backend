"""disable soft delete

Revision ID: 2925db1346a8
Revises: 90caa770bb39
Create Date: 2024-11-18 11:00:12.653811

"""
import sqlalchemy as sa
from alembic import op



# revision identifiers, used by Alembic.
revision = '2925db1346a8'
down_revision = '90caa770bb39'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # Remove "deleted" column
    op.drop_column('workspacepresets', 'deleted')
    op.drop_column('viewpresets', 'deleted')


def downgrade() -> None:
    # Add empty "deleted" column
    op.add_column('workspacepresets',
                  sa.Column('deleted', sa.String(length=50), nullable=True))
    op.add_column('viewpresets',
                  sa.Column('deleted', sa.String(length=50), nullable=True))
