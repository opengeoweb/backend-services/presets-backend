"""Throw away existing workspace presets

Revision ID: df7db7bcb0aa
Revises: f83fa1329d93
Create Date: 2023-04-17 09:16:11.378789

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'df7db7bcb0aa'
down_revision = 'f83fa1329d93'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute(sa.text("""DELETE FROM workspacepresets"""))


def downgrade() -> None:
    pass
