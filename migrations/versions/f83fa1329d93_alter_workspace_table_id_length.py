"""alter workspace table id length

Revision ID: f83fa1329d93
Revises: 81b1278cbefe
Create Date: 2023-03-21 16:54:14.345865

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'f83fa1329d93'
down_revision = '81b1278cbefe'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column(table_name='workspacepresets',
                    column_name='preset_id',
                    type_=sa.String(length=100))


def downgrade() -> None:
    pass
