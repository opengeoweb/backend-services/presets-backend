"""edit taf workspace title

Revision ID: 7bfe2286cfe1
Revises: df7db7bcb0aa
Create Date: 2023-05-15 10:44:08.846726

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '7bfe2286cfe1'
down_revision = 'df7db7bcb0aa'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute(
        sa.text("""DELETE FROM workspacepresets WHERE title='Taf Example'"""))


def downgrade() -> None:
    pass
