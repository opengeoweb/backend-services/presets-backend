"""allow soft delete

Revision ID: 90caa770bb39
Revises: 74bd3feda536
Create Date: 2024-10-02 08:46:15.380742

"""
import sqlalchemy as sa
from alembic import op



# revision identifiers, used by Alembic.
revision = '90caa770bb39'
down_revision = '74bd3feda536'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # Add empty "deleted" column
    op.add_column('workspacepresets',
                  sa.Column('deleted', sa.String(length=50), nullable=True))
    op.add_column('viewpresets',
                  sa.Column('deleted', sa.String(length=50), nullable=True))


def downgrade() -> None:
    # Remove "deleted" column
    op.drop_column('workspacepresets', 'deleted')
    op.drop_column('viewpresets', 'deleted')
