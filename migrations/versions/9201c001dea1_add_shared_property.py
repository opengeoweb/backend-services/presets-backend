"""add shared property

Revision ID: 9201c001dea1
Revises: 2925db1346a8
Create Date: 2025-02-26 13:41:05.370274

"""
import sqlalchemy as sa
from alembic import op



# revision identifiers, used by Alembic.
revision = '9201c001dea1'
down_revision = '2925db1346a8'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.add_column('viewpresets',
                  sa.Column('is_shared', sa.Boolean(), nullable=True))
    op.add_column('workspacepresets',
                  sa.Column('is_shared', sa.Boolean(), nullable=True))


def downgrade() -> None:
    op.drop_column('viewpresets', 'is_shared')
    op.drop_column('workspacepresets', 'is_shared')
