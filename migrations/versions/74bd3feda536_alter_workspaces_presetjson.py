"""alter_workspaces_presetjson

Revision ID: 74bd3feda536
Revises: 189e05beafad
Create Date: 2023-08-07 10:54:07.440458

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '74bd3feda536'
down_revision = '189e05beafad'
branch_labels = None
depends_on = None


def upgrade() -> None:
    '''Alter presetjson column to type jsonb'''
    op.execute(
        sa.text(
            """alter table workspacepresets alter column presetjson set data type jsonb"""
        ))


def downgrade() -> None:
    '''Revert altering presetjson column to type jsonb back to json'''
    op.execute(
        sa.text(
            """alter table workspacepresets alter column presetjson set data type json"""
        ))
