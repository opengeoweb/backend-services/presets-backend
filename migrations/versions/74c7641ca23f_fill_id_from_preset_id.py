"""fill id from preset_id

Revision ID: 74c7641ca23f
Revises: 06a95cbbd0ed
Create Date: 2022-12-06 13:22:19.701353

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '74c7641ca23f'
down_revision = '06a95cbbd0ed'
branch_labels = None
depends_on = None


def upgrade() -> None:
    ''''''
    op.execute(
        sa.text(
            """alter table viewpresets alter column presetjson set data type jsonb"""
        ))
    op.execute(
        sa.text(
            """UPDATE viewpresets SET presetjson = JSONB_SET(presetjson, '{id}', to_jsonb(preset_id))
                          WHERE presetjson->>'id' is NULL"""))


def downgrade() -> None:
    pass
