"""workspace presets table

Revision ID: ad03344d969b
Revises: 3b5728fbe66b
Create Date: 2023-03-13 15:53:06.923788

"""
import json
from datetime import datetime

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'ad03344d969b'
down_revision = '3b5728fbe66b'
branch_labels = None
depends_on = None

# Create an ad-hoc table to use for insert statement
temp_table = sa.sql.table('viewpresets', sa.sql.column('preset_id', sa.String),
                          sa.sql.column('username', sa.String),
                          sa.sql.column('preset_type', sa.String),
                          sa.sql.column('title', sa.String),
                          sa.sql.column('keywords', sa.String),
                          sa.sql.column('created_on', sa.TIMESTAMP),
                          sa.sql.column('scope', sa.String),
                          sa.sql.column('presetjson', sa.JSON))


def upgrade() -> None:

    # create workspace-preset table
    op.create_table(
        'workspacepresets',
        sa.Column('preset_id', sa.String(length=50), nullable=False),
        sa.Column('username', sa.String(length=50), nullable=False),
        sa.Column('abstract', sa.String(length=500), nullable=True),
        sa.Column('title', sa.String(length=50), nullable=False),
        sa.Column('keywords', sa.String(length=256), nullable=True),
        sa.Column('created_on', sa.TIMESTAMP(), nullable=False),
        sa.Column('presetjson', sa.JSON(), nullable=False),
        sa.Column('scope', sa.String(length=50), nullable=False),
        sa.PrimaryKeyConstraint('preset_id'))

    # add workspace specific view-presets
    bulk_input = []
    with open('./staticpresets/view-presets-for-workspace.json',
              mode='rb') as f:
        view_presets: list = json.load(f)

        for view_preset in view_presets:

            view_preset['username'] = 'global'
            view_preset['preset_type'] = 'map'
            view_preset['keywords'] = 'workspace'

            _input = {
                'preset_id': view_preset['id'],
                'username': 'global',
                'preset_type': 'map',
                'title': view_preset['title'],
                'keywords': 'workspace',
                'created_on': datetime.now(),
                'scope': 'system',
                'presetjson': view_preset
            }

            bulk_input.append(_input.copy())

    op.bulk_insert(temp_table, bulk_input)


def downgrade() -> None:
    op.drop_table('workspacepresets')  # remove when development finished
    # pass
