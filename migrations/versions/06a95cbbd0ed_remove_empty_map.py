"""Remove empty map

Revision ID: 06a95cbbd0ed
Revises: c90d94dc8e42
Create Date: 2022-07-19 16:13:40.074613

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '06a95cbbd0ed'
down_revision = 'c90d94dc8e42'


def upgrade():
    '''remove Empty Map'''
    op.execute(
        sa.text("""DELETE FROM viewpresets
                          WHERE preset_id='Empty'"""))


def downgrade():
    '''add Empty Map'''
    op.execute(
        sa.text("""INSERT INTO viewpresets
                (preset_id, username, preset_type, title, keywords, created_on, scope, presetjson)
                VALUES (
                    'Empty',
                    'global',
                    'map',
                    'Empty map',
                    'WMS',
                    current_timestamp,
                    'system',
                    '{
                        "componentType": "Map",
                        "id": "Empty",
                        "keywords": "WMS",
                        "scope": "system",
                        "title": "Empty map",
                        "initialProps": {
                            "mapPreset": {
                                "layers": [],
                                "proj": {
                                "srs": "EPSG:3857",
                                "bbox": {
                                    "left": -450651.2255879827,
                                    "bottom": 6393842.957153378,
                                    "right": 1428345.8183648037,
                                    "top": 7342085.640241
                                }
                            },
                            "shouldAutoUpdate": false
                        }
                    }
                }')
            """))
