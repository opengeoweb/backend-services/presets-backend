"""remove viewpresets for workspacepresets

Revision ID: 81b1278cbefe
Revises: ad03344d969b
Create Date: 2023-03-20 10:21:18.992947

"""

import json

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '81b1278cbefe'
down_revision = 'ad03344d969b'
branch_labels = None
depends_on = None


def upgrade() -> None:

    # remove workspace specific presets to prevent conflicts

    with open('./staticpresets/view-presets-for-workspace.json',
              mode='rb') as f:
        view_presets: list = json.load(f)

        for view_preset in view_presets:
            id_to_remove = view_preset['id']

            op.execute(
                sa.text(f"""DELETE FROM viewpresets
                        WHERE preset_id='{id_to_remove}'"""))

    # also remove duplicate presets
    # these will be loaded with knmi.workspace-presets.json
    list_to_remove = [
        'HarmoniePrecipitation', 'HarmonieTemperature', 'DWDWarnings',
        'HarmonieWindPressure', 'Radar', 'RenewalWarnings', 'Satellite',
        'SigmetAirmet', 'harmSfcObsSingleMapScreenConfig'
    ]
    for id_to_remove in list_to_remove:
        op.execute(
            sa.text(f"""DELETE FROM viewpresets
                        WHERE preset_id='{id_to_remove}'"""))

    # also remove development workspace presets
    list_to_remove = ['emptyMap', 'screenConfigRadarTemperature']
    for id_to_remove in list_to_remove:
        op.execute(
            sa.text(f"""DELETE FROM workspacepresets
                        WHERE preset_id='{id_to_remove}'"""))


def downgrade() -> None:
    pass
