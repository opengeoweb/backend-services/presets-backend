"""initial migration

Revision ID: c90d94dc8e42
Revises:
Create Date: 2022-07-06 15:42:03.444774

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = 'c90d94dc8e42'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'viewpresets',
        sa.Column('preset_id', sa.String(length=50), nullable=False),
        sa.Column('username', sa.String(length=50), nullable=False),
        sa.Column('preset_type', sa.String(length=50), nullable=False),
        sa.Column('title', sa.String(length=50), nullable=False),
        sa.Column('keywords', sa.String(length=50), nullable=False),
        sa.Column('created_on', sa.TIMESTAMP(), nullable=False),
        sa.Column('presetjson', sa.JSON(), nullable=False),
        sa.Column('scope', sa.String(length=50), nullable=False),
        sa.PrimaryKeyConstraint('preset_id'))


def downgrade():
    op.drop_table('viewpresets')
