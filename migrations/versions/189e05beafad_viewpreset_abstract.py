"""Viewpreset abstract

Revision ID: 189e05beafad
Revises: 7bfe2286cfe1
Create Date: 2023-06-08 17:39:13.833506

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '189e05beafad'
down_revision = '7bfe2286cfe1'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # Add the abstract text field
    op.add_column('viewpresets',
                  sa.Column('abstract', sa.String(length=500), nullable=True))


def downgrade() -> None:
    # Remove abstract text field
    op.drop_column('viewpresets', 'abstract')
