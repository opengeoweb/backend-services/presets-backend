BEGIN;
CREATE TABLE viewpresets (
    preset_id VARCHAR(50) NOT NULL,
    username VARCHAR(50) NOT NULL,
    preset_type VARCHAR(50) NOT NULL,
    title VARCHAR(50) NOT NULL,
    keywords VARCHAR(50) NOT NULL,
    created_on TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    presetjson JSON NOT NULL,
    scope VARCHAR(50) NOT NULL,
    PRIMARY KEY (preset_id)
);
COMMIT;
