# View-presets data-model

## Table of Contents

[[_TOC_]]

## Mapping

### Database fields

The database schema for the view-presets. This model corresponds to the viewpresets table in the database.

| Field         | Type      | Description                                                                                     | Example value                | Notes                                                                                            |
|---------------|:----------|-------------------------------------------------------------------------------------------------|------------------------------|--------------------------------------------------------------------------------------------------|
| `preset_id`   | string    | Unique identifier for the preset, serving as the primary key in the database.                   | `radarView`                  |                                                                                                  |
| `username`    | string    | The username of the user who created the preset. For presets provided by the system, this is set to `global`. | `global`       |                                                                                                  |
| `preset_type` | string    | Type of presets, all static presets are of type map.                                            | `map`                        | Deprecated: All static presets are of type 'map'. This field will be removed in future versions. |
| `title`       | string    | The name assigned to the preset by the user, describes the purpose or content of the preset.    | `Radar View`                 |                                                                                                  |
| `abstract`    | string    | A brief description of the preset, provided by the user. Used to enhance searchability.         | `null`                       |                                                                                                  |
| `keywords`    | string    | Comma-separated search terms provided by the user to facilitate searching for the preset.       | `Harmonie,Observations`      |                                                                                                  |
| `created_on`  | timestamp | Timestamp in UTC format indicating when the preset was created in the database.                 | `2024-07-01 13:26:31.041592` |                                                                                                  |
| `presetjson`  | object    | A JSON blob containing the payload consumed by the frontend, including all necessary configuration for the preset. | valid JSON document |                                                                                        |
| `scope`       | string    | Indicates if a preset is available application-wide (`system`) or created by and available for individual users only (`user`). | `user` |                                                                                         |

### presetjson

The `presetjson` object is a JSON payload consumed by the frontend, containing all necessary configurations for the preset. For detailed information on the structure and usage of this object, refer to the frontend type [ViewPreset](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/ViewPreset.html).

| Field           | Type   | Description                                                                                                                                       | Example value           | Notes |
|-----------------|:-------|---------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|-------|
| `id`            | string | The unique identifier for the preset, matching the primary key in the database.                                                                   | `radarView`             | From the frontend type [id](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/ViewPreset.html#id) |
| `scope`         | string | Defines the scope of the preset, indicating whether it is user-specific or available application-wide, matching the scope field in the database.  | `user`                  | From the frontend type [scope](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/ViewPreset.html#scope) |
| `title`         | string | The title of the preset, matching the title field in the database.                                                                                | `Radar View`            | From the frontend type [title](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/ViewPreset.html#title) |
| `keywords`      | string | Keywords associated with the preset, aiding in search and categorization, matching the keywords field in the database.                            | `Harmonie,Observations` | From the frontend type [keywords](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/ViewPreset.html#keywords) |
| `initialProps`  | object | A JSON object containing the initial properties and configuration for the preset.                                                                 | valid JSON document     | From the frontend type [initialProps](https://opengeoweb.gitlab.io/opengeoweb/docs/core/types/InitialProps.html) |
| `componentType` | string | Specifies the type of the preset. Possible values include Map, MultiMap, ModelRunInterval, HarmonieTempAndPrecipPreset, TimeSlider or TimeSeries. | `Map`                   | From the frontend type [componentType](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/ViewPreset.html#componentType) |

### initialProps object

Part of presetjson object. For detailed information on the structure and usage of this object, refer to the frontend type [initialProps](https://opengeoweb.gitlab.io/opengeoweb/docs/core/types/InitialProps.html).

| Field                       | Type            | Description                                                                                                            | Example value                        | Notes |
|-----------------------------|:----------------|------------------------------------------------------------------------------------------------------------------------|--------------------------------------|-------|
| `mapPreset`                 | object or array | Object of mapPreset or array of mapPreset objects.                                                                     | valid JSON document                  |       |
| `syncGroupsIds`             | array           | Synchronize two or more windows with area or time, defined in workspace preset.                                        | `["Area_radarTemp, Time_radarTemp"]` |       |
| `mapControls`               | object          | Optional React node for map controls.                                                                                  | `<Controls />`                       |       |
| `shouldDisplayDrawControls` | bool            | Optional boolean to display drawing controls on the map.                                                               | `true`                               |       |
| `displaySearchButtonInMap`  | bool            | Optional boolean to display a search button on the map.                                                                | `false`                              |       |
| `shouldShowZoomControls`    | bool            | Optional boolean to display zoom controls on the map.                                                                  | `true`                               |       |
| `layers`                    | object or array | Array of map layers or configuration for map layers in the view, including synchronization groups.                     | valid JSON document                  |       |
| `multiLegend`               | bool            | Optional boolean to display multiple legends on the map.                                                               | `true`                               |       |
| `interval`                  | int             | Units of hours between maps in the ModelRunInterval FE component.                                                      | `60`                                 |       |
| `startTimeIncrement`        | int             | Optional increment for the start time of the model runs.                                                               | `15`                                 |       |
| `sliderPreset`              | object          | Preset configuration for the slider, including map ID.                                                                 | `{ mapId: 'map1' }`                  |       |
| `plotPreset`                | object          | Preset configuration for the time series plot.                                                                         | valid JSON document                  |       |
| `services`                  | array           | Array of services to be used in the time series plot.                                                                  | valid JSON array                     |       |
| `hideLocationMarkers`       | boolean         | Optional boolean to initially hide the time series location markers on the map. False is the same as default behavior. | `true`                               |       |

### mapPreset object

Part of initialProps object. For detailed information on the structure and usage of this object, refer to the frontend type [mapPreset](https://opengeoweb.gitlab.io/opengeoweb/docs/store/interfaces/MapPreset.html).

| Field                          | Type   | Description                                                                      | Example value                             | Notes |
|--------------------------------|:-------|----------------------------------------------------------------------------------|-------------------------------------------|-------|
| `layers`                       | array  | Array of layers.                                                                 | valid JSON array                          |       |
| `activeLayerId`                | string | Predefined layer ID that is active when the preset is opened.                    | `radar_precipitation_intensity_nordic_id` |       |
| `autoTimeStepLayerId`          | string | Predefined layer ID for auto timestep.                                           | `layer_id`                                |       |
| `autoUpdateLayerId`            | string | Predefined layer ID for auto update.                                             | `layer_id`                                |       |
| `proj`                         | object | Object containing spatial reference system (SRS) and bounding box (bbox).        | valid JSON document                       |       |
| `dimensions`                   | array  | Array of dimensions for the map preset.                                          | valid JSON array                          |       |
| `shouldAnimate`                | bool   | State variable indicating if the map view should animate.                        | `true`                                    |       |
| `shouldAutoUpdate`             | bool   | State variable indicating if the map view should auto-update.                    | `true`                                    |       |
| `showTimeSlider`               | bool   | State variable indicating if the timeslider should be shown.                     | `true`                                    |       |
| `displayMapPin`                | bool   | Optional field to display a map pin.                                             | `true`                                    |       |
| `shouldShowZoomControls`       | bool   | State variable indicating if the zoom controls should be shown.                  | `true`                                    |       |
| `toggleTimestepAuto`           | bool   | State variable indicating if the timestep should toggle automatically.           | `true`                                    |       |
| `animationPayload`             | object | Object containing animation properties.                                          | valid JSON document                       |       |
| `shouldShowLegend`             | bool   | State variable indicating if the legend should be shown.                         | `true`                                    |       |
| `shouldShowLayerManager`       | bool   | State variable indicating if the layer manager should be shown.                  | `true`                                    |       |
| `shouldShowDockedLayerManager` | bool   | State variable indicating if the docked layer manager should be shown.           | `true`                                    |       |
| `dockedLayerManagerSize`       | string | List of predefined sizes for the layer manager in docked state that are allowed. | `sizeMedium`                              |       |

#### layers array

Part of mapPreset object. For detailed information on the structure and usage of this object, refer to the frontend type [Layer](https://opengeoweb.gitlab.io/opengeoweb/docs/store/interfaces/layerTypes.Layer.html).

See [Layers object](#layers-object).

#### proj object

Part of mapPreset object. For detailed information on the structure and usage of this object, refer to the frontend type [proj](https://opengeoweb.gitlab.io/opengeoweb/docs/store/interfaces/MapPreset.html#proj).

| Field  | Type   | Description                                      | Example value | Notes |
|--------|:-------|--------------------------------------------------|---------------|-------|
| `srs`  | string | Spatial Reference System identifier for the map. | `EPSG:3857`   |       |
| `bbox` | object | Bounding Box defining the visible map area.      |               |       |

##### bbox object

Part of proj object. For detailed information on the structure and usage of this object, refer to the frontend type [Bbox](https://opengeoweb.gitlab.io/opengeoweb/docs/store/interfaces/mapTypes.Bbox.html).

| Field    | Type   | Description                                    | Example value | Notes                                                          |
|----------|:-------|------------------------------------------------|---------------|----------------------------------------------------------------|
| `top`    | float  | Northernmost coordinate of the bounding box.   | `7438773.776` | Value depends on the projection system used (e.g., EPSG:3857). |
| `left`   | float  | Westernmost coordinate of the bounding box.    | `-450651.226` | Value depends on the projection system used (e.g., EPSG:3857). |
| `right`  | float  | Easternmost coordinate of the bounding box.    | `1428345.818` | Value depends on the projection system used (e.g., EPSG:3857). |
| `bottom` | float  | Southernmost coordinate of the bounding box.   | `6490531.093` | Value depends on the projection system used (e.g., EPSG:3857). |

#### dimensions array
Part of mapPreset object. For detailed information on the structure and usage of this object, refer to the frontend type [Dimension](https://opengeoweb.gitlab.io/opengeoweb/docs/store/interfaces/mapTypes.Dimension.html).

| Field                | Type    | Description                                                                 | Example value | Notes                                      |
|----------------------|---------|-----------------------------------------------------------------------------|---------------|--------------------------------------------|
| `currentValue`       | string  | The current value of the dimension.                                         | `2023-10-01`  |                                            |
| `maxValue`           | string  | The maximum value that the dimension can have.                              | `2023-12-31`  |                                            |
| `minValue`           | string  | The minimum value that the dimension can have.                              | `2023-01-01`  |                                            |
| `name`               | string  | The name of the dimension.                                                  | `Time`        |                                            |
| `synced`             | bool    | Indicates whether the dimension is synced with other dimensions.            | `true`        |                                            |
| `timeInterval`       | object  | The time interval associated with the dimension.                            |               |                                            |
| `units`              | string  | The units of measurement for the dimension.                                 | `seconds`     |                                            |
| `validSyncSelection` | bool    | Indicates whether the current selection is valid for synchronization.       | `true`        |                                            |
| `values`             | string  | A list of possible values for the dimension.                                | `["2023-10-01", "2023-10-02"]` |                           |

#### animationPayload object

Optional part of mapPreset object. For detailed information on the structure and usage of this object, refer to the frontend type [AnimationPayloadType](https://opengeoweb.gitlab.io/opengeoweb/docs/store/interfaces/mapTypes.AnimationPayloadType.html).

| Field                   | Type    | Description                                                                                                                                                                            | Example value | Notes                                             |
|-------------------------|:--------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------|---------------------------------------------------|
| `speed`                 | int     | Sets the animation delay. The default value is 4, which translates to an animation delay of 250 ms (1000 ms / 4).                                                                      | `2`           | possible values are 0.1, 0.2, 0.5, 1, 2, 4, 8, 16 |
| `endTime`               | string  | The rightmost and maximum time value of the animation, indicating where the animation ends.                                                                                            | `NOW+PT2H00M` | ISO-8601 derivative value                         |
| `duration`              | int     | The total duration of the animation in minutes.                                                                                                                                        | `240`         |                                                   |
| `interval`              | int     | The step size of the animation in minutes.                                                                                                                                             | `5`           |                                                   |
| `shouldEndtimeOverride` | bool    | If set to true, overrides the endTime using the preset. This allows the animation to be fixed at a specific point in time, preventing it from advancing even if new data is available. | `true`        |                                                   |

### syncGroupsIds array

Part of the initialProps object. For detailed information on the structure and usage of this object, refer to the frontend type [syncGroupsIds](https://opengeoweb.gitlab.io/opengeoweb/docs/core/interfaces/InitialMapProps.html#syncGroupsIds).

| Field          | Type   | Description               | Example value                  | Notes |
|----------------|--------|---------------------------|--------------------------------|-------|
| `syncGroupsIds`| array  | List of sync group IDs.   | `["syncGroup1", "syncGroup2"]` |       |

### mapControls

Part of the initialProps object. For detailed information on the structure and usage of this object, refer to the frontend type [mapControls](https://opengeoweb.gitlab.io/opengeoweb/docs/core/interfaces/InitialMapProps.html#mapControls).

| Field          | Type    | Description                  | Example value                    | Notes |
|----------------|---------|------------------------------|----------------------------------|-------|
| `mapControls`  | object  | Object containing ReactNode. | `<Controls />`                   |       |

#### Layers object

Part of initialProps object. For detailed information on the structure and usage of this object, refer to the frontend type [Layer](https://opengeoweb.gitlab.io/opengeoweb/docs/store/interfaces/layerTypes.Layer.html)

| Field                        | Type   | Description                                                                 | Example Value                            | Notes                                               |
|------------------------------|--------|-----------------------------------------------------------------------------|------------------------------------------|-----------------------------------------------------|
| `geojson`                    | object | GeoJSON data for the feature layer                                          | `{ "type": "FeatureCollection", ... }`   | Optional, from `FeatureLayer` and `LayerFoundation` |
| `defaultGeoJSONProperties`   | object | Default properties for GeoJSON features                                     | `{ "property1": "value1" }`              | Optional, from `FeatureLayer`                       |
| `selectedFeatureIndex`       | int    | Index of the selected feature                                               | `0`                                      | Optional, from `FeatureLayer`                       |
| `isInEditMode`               | bool   | Flag indicating if the layer is in edit mode                                | `true`                                   | Optional, from `FeatureLayer`                       |
| `drawMode`                   | string | Mode for drawing features                                                   | `"draw"`                                 | Optional, from `FeatureLayer`                       |
| `dimensions`                 | array  | List of dimensions for the layer                                            | `[ { "name": "time", ... } ]`            | Optional, from `ReduxLayer` and `LayerFoundation`   |
| `mapId`                      | string | Identifier for the map                                                      | `"map123"`                               | Optional, from `ReduxLayer`                         |
| `status`                     | string | Status of the layer                                                         | `"active"`                               | Optional, from `ReduxLayer`                         |
| `useLatestReferenceTime`     | bool   | Flag indicating if the latest reference time should be used                 | `true`                                   | Optional, from `ReduxLayer`                         |
| `headers`                    | array  | List of headers for the layer                                               | `[ { "key": "Authorization", ... } ]`    | Optional, from `Layer`                              |
| `tileServer`                 | object | Settings for the tile server                                                | `{ "url": "http://tileserver.com" }`     | Optional, from `TiledLayerFoundation`               |
| `acceptanceTimeInMinutes`    | int    | Acceptance time in minutes                                                  | `30`                                     | Optional, from `LayerFoundation`                    |
| `enabled`                    | bool   | Flag indicating if the layer is enabled                                     | `true`                                   | Optional, from `LayerFoundation`                    |
| `format`                     | string | Image format requested from the WMS service.                                | `"image/png"`                            | Optional, from `LayerFoundation`                    |
| `id`                         | string | Unique identifier for the layer.                                            | `"layer123"`                             | Optional, from `LayerFoundation`                    |
| `layerType`                  | string | Type of the layer                                                           | `"raster"`                               | Optional, from `LayerFoundation`                    |
| `name`                       | string | Name of the layer                                                           | `"Layer Name"`                           | Optional, from `LayerFoundation`                    |
| `opacity`                    | float  | Opacity of the layer (between 0.0 and 1.0)                                  | `0.5`                                    | Optional, from `LayerFoundation`                    |
| `service`                    | string | Service associated with the layer                                           | `"WMS"`                                  | Optional, from `LayerFoundation`                    |
| `style`                      | string | Style of the layer                                                          | `"default"`                              | Optional, from `LayerFoundation`                    |
| `title`                      | string | Title of the layer                                                          | `"Layer Title"`                          | Optional, from `LayerFoundation`                    |
| `type`                       | string | Type of the layer                                                           | `"FeatureLayer"`                         | Optional, from `LayerFoundation`                    |
| `bottomRow`                  | object | Used by HarmonieTempAndPrecipPreset component to arrange views.             |                                          | From `InitialHarmTempAndPrecipProps`                |
| `bottomRowSyncGroups`        | array  | Used by HarmonieTempAndPrecipPreset component to sync view-groups.          | `["timeLayerGroupA", "areaLayerGroupA"]` | Optional, from `InitialHarmTempAndPrecipProps`      |
| `topRow`                     | object | Used by HarmonieTempAndPrecipPreset component to arrange views.             |                                          | From `InitialHarmTempAndPrecipProps`                |
| `topRowSyncGroups`           | array  | Used by HarmonieTempAndPrecipPreset component to sync view-groups.          | `["timeLayerGroupA", "areaLayerGroupA"]` | Optional, from `InitialHarmTempAndPrecipProps`      |

### sliderPreset object

Part of the initialProps object. For detailed information on the structure and usage of this object, refer to the frontend type `sliderPreset`.

| Field          | Type    | Description                                    | Example value         | Notes |
|----------------|---------|------------------------------------------------|-----------------------|-------|
| `sliderPreset` | object  | Object containing slider preset configuration. | `{"mapid": "map123"}` |       |

### plotPreset object

Part of initialProps object. For detailed information on the structure and usage of this object, refer to the frontend type [PlotPreset](https://opengeoweb.gitlab.io/opengeoweb/docs/timeseries/interfaces/PlotPreset.html).

| Field          | Type   | Description                                                                | Example value                  | Notes |
|----------------|:-------|----------------------------------------------------------------------------|--------------------------------|-------|
| `connectedMap` | string | ID of the map view that this plot preset is connected to.                  | `viewA`                        |       |
| `mapId`        | string | ID of the map associated with this plot preset.                            | `TimeseriesMapHap1GroundLevel` |       |
| `parameters`   | array  | List of parameter objects defining the properties and data for each plot.  | valid JSON document            |       |
| `plots`        | array  | List of plot objects defining individual plots within the preset.          | valid JSON document            |       |
 
#### parameters object

Part of parameters array in plotPreset object. For detailed information on the structure and usage of this object, refer to the frontend type [parameters](https://opengeoweb.gitlab.io/opengeoweb/docs/timeseries/interfaces/PlotPreset.html#parameters).

| Field          | Type             | Description                                        | Example value | Notes |
|----------------|:-----------------|----------------------------------------------------|---------------|-------|
| `collectionId` | string           | Identifier for the EDR collection.                 | `ecmwf`       |       |
| `plotId`       | string           | Identifier for the plot.                           | `Plot_1`      |       |
| `plotType`     | object or string | Type of the plot (e.g., line, bar).                | `bar`         |       |
| `propertyName` | string           | Name displayed in the legend for the parameter.    | `Pressure`    |       |
| `serviceId`    | string           | Identifier for the EDR service.                    | `fmi_ecmwf`   |       |
| `color`        | string           | One-letter code representing a predefined color.   | `D`           |       |
| `opacity`      | int              | Opacity level ranging from 1 to 100.               | `70`          |       |
| `enabled`      | bool             | Flag indicating if the parameter is enabled.       | `true`        |       |
| `id`           | string           | Unique identifier for the parameter.               | `param_123`   |       |
| `instanceId`   | string           | Identifier for the instance of the parameter.      | `inst_456`    |       |
| `instances`    | object           | Object containing instances of the parameter.      | `{...}`       |       |
| `unit`         | string           | Unit of measurement for the parameter.             | `hPa`         |       |

#### plots object

Part of plots array in plotPreset object. For detailed information on the structure and usage of this object, refer to the frontend type [plotPreset](https://opengeoweb.gitlab.io/opengeoweb/docs/core/interfaces/InitialTimeSeriesProps.html#plotPreset).

| Field        | Type   | Description                                                                    | Example value | Notes |
|--------------|:-------|--------------------------------------------------------------------------------|---------------|-------|
| `plotId`     | string | Unique identifier for the plot, used to link parameters to this specific plot. | `Plot_1`      |       |
| `title`      | string | Descriptive title of the plot, intended for display purposes.                  | `Plot 1`      |       |
| `enabled`    | bool   | Flag indicating if the parameter is enabled.                                   | `true`        |       |
| `parameters` | object | Object containing instances of the parameter.                                  | `{...}`       |       |


#### services object

Part of the initialProps object. For detailed information on the structure and usage of this object, refer to the frontend type `TimeSeriesService`.

| Field         | Type   | Description                                                            | Example value               | Notes |
|---------------|:-------|------------------------------------------------------------------------|-----------------------------|-------|
| `id`          | string | A unique identifier for the service.                                   | `metno_edr`                 |       |
| `description` | string | A human-readable title of the service for display purposes.            | `METNO EDR`                 |       |
| `url`         | string | The URL endpoint of the service.                                       | `https://interpol-b.met.no` |       |
| `type`        | object | The type of the service, eather `EDR' or 'OGC'.                        | `EDR`                       |       |
| `name`        | string | The name of the service.                                               | `metno_edr`                 |       |
| `scope`       | string | The scope of the service, indicating its usage context (e.g., system). | `system`                    |       |

## Entity Relationship (ER) Diagram

This documentation uses Mermaid syntax to create an entity-relationship (ER) diagram.

```mermaid
erDiagram
    ViewPreset {
        preset_id string PK
        username string
        preset-type string
        title string
        abstract string
        keywords string
        created_on timestamp
        presetjson object
        scope string
    }
    PresetJson {
        componentType string
        id string
        initialProps object
        keywords string
        scope string
        title string
    }
    InitialProps {
        mapPreset list
        syncGroupsIds list
        mapControls object
        shouldDisplayDrawControls bool
        displaySearchButtonInMap bool
        shouldShowZoomControls bool
        layers list
        multiLegend bool
        interval int
        startTimeIncrement int
        sliderPreset object
        plotPreset list
        services list
        hideLocationMarkers bool
    }
    MapPreset {
        layers list
        activeLayerId string
        autoTimeStepLayerId string
        autoUpdateLayerId string
        proj object
        dimensions list
        shouldAnimate bool
        shouldAutoUpdate bool
        showTimeSlider bool
        displayMapPin bool
        shouldShowZoomControls bool
        toggleTimestepAuto bool
        animationPayload object
        shouldShowLegend bool
        shouldShowLayerManager bool
        shouldShowDockedLayerManager bool
        dockedLayerManagerSize string     
    }
    SyncGroupsIds {
        syncGroupsIds list
    }
    AnimationPayload {
        speed int
        endtime string
        duration int
        interval int
        shouldEndtimeOverride bool
    }
    Dimensions {
        currentValue string
        maxValue string
        minValue string
        name string
        synced bool
        timeInterval object
        units string
        validSyncSelection bool
        values string
    }
    Proj {
        srs string
        bbox object
    }
    Bbox {
        up float
        left float
        right float
        bottom float
    }
    Layer {
        geojson object
        defaultGeoJSONProperties object
        selectedFeatureIndex int
        isInEditMode bool
        drawMode string
        dimensions list
        mapId string
        status string
        useLatestReferenceTime bool
        headers list
        tileServer object
        acceptanceTimeInMinutes int
        enabled bool
        format string
        id string
        layerType string
        name string
        opacity float
        service url
        bottomRow self
        bottomRowSyncGroups list
        topRow self
        topRowSyncGroups list
    }
    SliderPreset {
      sliderPreset object
    } 
    PlotPreset {
        connectedMap string
        mapId string
        parameters list
        plots list
    }
    Parameter {
        collectionId string
        plotId string
        plotType object
        propertyName string
        serviceId string
        color string
        opacity int
        enabled bool
        id string
        instanceId string
        instances object
        unit string
    }
    Plot {
        plotId string
        title string
        enabled bool
        parameters object
    }
    Service {
        id string
        description string
        url string
        type string
        name string
        scope string
    }
    ViewPreset only one to one PresetJson: contains
    PresetJson only one to one InitialProps: contains
    InitialProps only one to zero or more MapPreset: "can have"
    InitialProps only one to zero or more PlotPreset: "can have"
    InitialProps only one to zero or more Layer: "can have"
    InitialProps only one to zero or more Service: "can have"
    InitialProps only one to zero or more SliderPreset: "can have"
    InitialProps only one to zero or more SyncGroupsIds: "can have"
    MapPreset only one to zero or more Layer: "can have"
    MapPreset only one to one Proj : "can have"
    Proj only one to one Bbox: contains
    MapPreset only one to zero or more Dimensions: "can have"
    MapPreset only one to zero or one AnimationPayload: "can have"
    PlotPreset only one to one Parameter: contains
    PlotPreset only one to one Plot: contains

```
