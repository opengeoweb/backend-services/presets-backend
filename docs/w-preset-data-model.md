# Workspace-presets data-model

## Table of Contents

[[_TOC_]]

## Mapping

### Database fields

The database schema for the workspace-presets. This model corresponds to the workspacepresets table in the database.

| Field         | Type      | Description                                                                                     | Example value                | Notes                             |
|---------------|:----------|-------------------------------------------------------------------------------------------------|------------------------------|-----------------------------------|
| `abstract`    | string    | A brief description of the preset, provided by the user. Used to enhance searchability.         | `null`                       |                                   |
| `created_on`  | timestamp | Timestamp in UTC format indicating when the preset was created in the database.                 | `2024-07-01 13:26:31.041592` |                                   |
| `keywords`    | string    | Comma-separated search terms provided by the user to facilitate searching for the preset.       | `Harmonie,Observations`      |                                   |
| `presetjson`  | object    | A JSON blob containing the payload consumed by the frontend, including all necessary configuration for the preset. | valid JSON document |                         |
| `preset_id`   | string    | Unique identifier for the preset, serving as the primary key in the database.                   | `radarView`                  |                                   |
| `scope`       | string    | Indicates if a preset is available application-wide (`system`) or created by and available for individual users only (`user`). | `user` |                          |
| `title`       | string    | The name assigned to the preset by the user, describes the purpose or content of the preset.    | `Radar View`                 |                                   |
| `username`    | string    | The username of the user who created the preset. For presets created by the system, this is set to `global`. | `global`        |                                   |

### presetjson

The `presetjson` object is a JSON payload consumed by the frontend, containing all necessary configurations for the preset. For detailed information on the structure and usage of this object, refer to the frontend type [WorkspacePresetFromBE](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html).

| Field        | Type             | Description                                                                                                            | Example value     | Notes                                                                                                                                             |
|--------------|:-----------------|------------------------------------------------------------------------------------------------------------------------|-------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| `abstract`   | string           | A brief summary or description of the preset, matching the abstract field in the database.                             | `null`            | From the frontend type [abstract](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#abstract)          |
| `id`         | string           | The unique identifier for the preset, matching the primary key in the database.                                        | `radarView`       | From the frontend type [id](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#id)                      |
| `mosaicNode` | string or object | Configuration for arranging views in rows or columns.                                                                  | `viewA`           | From the frontend type [mosaicNode](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#mosaicNode)      |
| `isTimeScrollingEnabled` | boolean | Enables/disableds timescrolling in workspace                                                            | `true`           | From the frontend type [isTimeScrollingEnabled](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#isTimeScrollingEnabled) 
| `linking` | object | A JSON object containing a map of panelIds to a list of panelIds representing the links between windows and maps.                                                                 | valid JSON document     | From the frontend type [linking](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#linking)     |
| `scope`      | string           | Defines the scope of the preset, indicating whether it is user-specific or available application-wide, matching the scope field in the database. | `user` | From the frontend type [scope](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#scope) |
| `syncGroups` | array            | Information on how to synchronize views within the workspace preset.                                                   |                   | From the frontend type [syncGroups](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#syncGroups)      |
| `title`      | string           | The title of the preset, matching the title field in the database.                                                     | `Radar View`      | From the frontend type [title](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#title)                |
| `views`      | array            | List of views included in the preset.                                                                                  |                   | From the frontend type [views](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#views)                |
| `viewType`   | string           | Type of view layout, such as `singleWindow`, `tiledWindow`, or `multiWindow`.                                          | `singleWindow`    | From the frontend type [viewType](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#viewType)          |

### mosaicNode object

Part of presetjson object. For detailed information on the structure and usage of this object, refer to the frontend type [mosaicNode](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#mosaicNode).

| Field             | Type   | Description                                                           | Example value | Notes                                            |
|-------------------|:-------|-----------------------------------------------------------------------|---------------|--------------------------------------------------|
| `first`           | string | Identifier for the first view in the mosaic layout.                   | `viewA `      |                                                  |
| `second`          | string | Identifier for the second view in the mosaic layout.                  | `viewB`       |                                                  |
| `direction`       | string | Layout direction of the mosaic, either `row` or `column`.             | `row`         |                                                  |
| `splitPercentage` | float  | Percentage of space allocated to the first view in the mosaic layout. | `null`        |                                                  |

### syncGroups array

Part of the presetjson object. For detailed information on the structure and usage of this object, refer to the frontend type [syncGroups](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#syncGroups).

| Field   | Type   | Description                                                                                   | Example value                                                                                                              | Notes |
|---------|:-------|-----------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------|-------|
| `id`    | string | Identifier for the synchronization group, used to link views that should be synchronized.     | `Area_radarTemp`                                                                                                           |       |
| `title` | string | The title of the preset.                                                                      |                                                                                                                            |       |
| `type`  | string | Type of synchronization to be applied to the group, such as setting the bounding box or time, such as `SYNCGROUPS_TYPE_SETBBOX`, `SYNCGROUPS_TYPE_SETTIME`, `SYNCGROUPS_TYPE_SETLAYERACTIONS`. | `SYNCGROUPS_TYPE_SETTIME` |       |

### views array

Part of the presetjson object. For detailed information on the structure and usage of this object, refer to the frontend type [views](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#views).

| Field          | Type   | Description                                                                         | Example value   | Notes |
|----------------|:-------|-------------------------------------------------------------------------------------|-----------------|-------|
| `mosaicNodeId` | string | Identifier for the mosaic node, used to position the view within the layout.        | `viewA`         |       |
| `viewPresetId` | string | Identifier for the view preset, which defines the content and behavior of the view. | `TimeseriesMap` |       |


### linking object

Part of the presetjson object. For detailed information on the structure and usage of this object, refer to the frontend type [linking](https://opengeoweb.gitlab.io/opengeoweb/docs/workspace/interfaces/WorkspacePresetFromBE.html#linking).

| Field          | Type   | Description                                                                         | Example value   | Notes |
|----------------|:-------|-------------------------------------------------------------------------------------|-----------------|-------|
| `panelId` | array | List of panelIds linked to this panelId        | `[viewA, viewB]`         |       |


## Entity Relationship (ER) Diagram

This documentation uses Mermaid syntax to create an entity-relationship (ER) diagram.

```mermaid
erDiagram
    WorkspacePreset {
        preset_id string PK
        username string
        scope string
        title string
        keywords string
        abstract string
        created_on timestamp
        presetjson object
    }
    PresetJson {
        id string
        scope string
        title string
        views list
        abstract string
        viewType string
        mosaicNode object
        syncGroups list
        isTimeScrollingEnabled boolean
        linking object
    }
    MosaicNode {
        first string
        second string
        direction string
        splitPercentage float
    }
    SyncGroup {
        id string
        title string
        type string
    }
    View {
        mosaicNodeId string
        viewPresetId string
    }
    linking {
        panelId list
    }
    WorkspacePreset only one to one PresetJson: contains
    PresetJson only one to zero or one MosaicNode: "can have"
    PresetJson only one to zero or many SyncGroup: "can have"
    PresetJson only one to zero or many View: "can have"
    PresetJson only one to zero or one isTimeScrollingEnabled: "can have"
    PresetJson only one to zero or one linking: "can have"
```
