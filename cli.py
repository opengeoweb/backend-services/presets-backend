"""Extra CLI commands"""
#!/usr/bin/env python

import json
import logging
import logging.config
import os

import typer
from alembic.command import stamp
from alembic.config import Config
from sqlalchemy import inspect, text
from sqlalchemy.orm import Session

from app.crud.errors import CreateError
from app.crud.view_preset import CRUD as view_preset
from app.crud.workspace_preset import CRUD as workspace_preset
from app.db import dump_sqlalchemy, engine, restore_sqlalchemy
from app.models import ViewPresetCreate, WorkspacePresetCreate

# Use INFO if level not valid
log_level = logging.getLevelName(os.getenv('LOG_LEVEL', 'WARN'))

logging.config.dictConfig({
    'version': 1,
    'formatters': {
        'json': {
            'class': 'app.logger.JsonFormatter'
        }
    },
    'handlers': {
        'stream': {
            'class': 'logging.StreamHandler',
            'formatter': 'json',
            'stream': 'ext://sys.stderr',
        }
    },
    'root': {
        'handlers': ['stream'],
        'level': log_level,
    },
    'loggers': {
        'sqlalchemy': {
            'level': log_level,
        },
        'alembic': {
            'level': log_level,
        },
        'alembic.runtime.migration': {
            'level': log_level,
        },
    },
})

app = typer.Typer()


@app.command()
def enable_alembic() -> None:
    """Inspect the state of the database and make sure Alembic is enabled"""
    alembic_cfg = Config('alembic.ini')
    inspector = inspect(engine)
    if not inspector.has_table('alembic_version'):
        if inspector.has_table('viewpresets'):
            # pre-alembic schema seems to exist, so stamp current database with initial version
            stamp(alembic_cfg, revision='c90d94dc8e42')


@app.command()
def clear() -> None:
    """Clear all system presets"""

    # Delete all system workspace-presets
    logging.info('Deleting workspace-presets')
    with Session(engine) as session:
        workspace_preset(session).system_clear()

    # Delete all system view-presets
    logging.info('Deleting view-presets')
    with Session(engine) as session:
        view_preset(session).system_clear()


@app.command()
def seed() -> None:
    """Seed the database with default viewpresets"""

    # get deploy environment
    # if not set --> load a default configuration?
    # if not set, it can be None or a blank string
    # in this case, set variable to equal "open"
    if not (deploy_config := os.getenv('DEPLOY_ENVIRONMENT', None)):
        deploy_config = 'open'

    # load config presets if set
    try:
        logging.info('Load %s config view presets', deploy_config)
        with open(f'./staticpresets/{deploy_config}.view-presets.json',
                  mode='rb') as file:
            view_presets: list = json.load(file)
    except FileNotFoundError:
        logging.error(
            'View-presets file not found, no data can be seeded into the database'
        )
        return

    # store presets to database
    view_preset_failed = 0
    logging.info('inserting view-presets')
    with Session(engine) as session:
        for _view_preset in view_presets:
            try:
                with session.begin():
                    view_preset(session).seed_create(
                        ViewPresetCreate(**_view_preset))
            except CreateError as err:
                view_preset_failed += 1
                logging.info(str(err),
                             extra={'preset': _view_preset.get('title')})

    # try to load workspace presets
    try:
        logging.info('Load %s config workspace presets', deploy_config)
        with open(f'./staticpresets/{deploy_config}.workspace-presets.json',
                  mode='rb') as file:
            workspace_presets: list = json.load(file)
    except FileNotFoundError:
        logging.error(
            'Workspace-presets file not found, no data can be seeded into the database'
        )
        return

    # store workspace presets to database
    workspace_preset_failed = 0
    logging.info('inserting workspace-presets')
    with Session(engine) as session:
        for _workspace_preset in workspace_presets:
            try:
                with session.begin():
                    workspace_preset(session).seed_create(
                        WorkspacePresetCreate(**_workspace_preset))
            except CreateError as err:
                workspace_preset_failed += 1
                logging.info(str(err),
                             extra={'presets': _workspace_preset.get('title')})

    if view_preset_failed > 0:
        logging.info('Failed to seed %s/%s view-presets', view_preset_failed,
                     len(view_presets))

    if workspace_preset_failed > 0:
        logging.info('Failed to seed %s/%s workspace-presets',
                     workspace_preset_failed, len(workspace_presets))
    logging.info('Seeding finished')


@app.command()
def backup(file="dump.json") -> None:
    """
        Create a backup of the database to a json file
        Usage: python cli.py backup --file presetsdb.json
    """
    logging.info("Create a backup of presets database into file %s", file)
    dump = dump_sqlalchemy()
    with open(file, "w", encoding='utf-8') as outfile:
        outfile.write(dump)
    logging.info("*** Database contents written to %s", file)


@app.command()
def restore(file="dump.json", username=None) -> None:
    """
        Restore database from a json file 
        Usage: python cli.py restore --file presetsdb.json --username testuser
        file is the json with the database dum
        username is optional, allows to restore presets for a specific user
    """
    logging.info("Restoring from %s", file)
    with open(file, 'r', encoding='utf-8') as openfile:
        json_object = json.load(openfile)
    restore_sqlalchemy(json_object, username)
    logging.info("Restored from %s", file)


if __name__ == '__main__':
    app()
