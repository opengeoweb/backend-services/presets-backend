# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
""" Cron like scheduler to make backups every hour """

import logging

from apscheduler.schedulers.asyncio import AsyncIOScheduler  # type: ignore
from fastapi import FastAPI

from app.utils.backup import make_backup


async def init_apscheduler(_fastapiapp: FastAPI):
    """Captures FASTAPI Lifespan events to start the AsyncIOScheduler"""

    logging.info("=== Starting AP Scheduler ===")
    # start scheduler to run functions periodically
    scheduler = AsyncIOScheduler()
    scheduler.add_job(
        make_backup,
        "cron",
        [],
        hour="*",
        jitter=10,  # delay up to 10 seconds
        max_instances=1,
        coalesce=True)
    scheduler.start()

    yield

    logging.info("=== Stopping AP Scheduler ===")
    scheduler.shutdown()
