# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This module handles the interaction with the database"""
from __future__ import annotations

import logging
import uuid

from fastapi.encoders import jsonable_encoder
from pydantic import validate_call
from sqlalchemy import and_, delete, func, or_, select
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import Session
from sqlalchemy.sql.elements import BooleanClauseList

from app.crud.errors import CreateError, DeleteError, ReadError, UpdateError
from app.models import ScopeEnum, ViewPreset, ViewPresetCreate


class CRUD:
    """CRUD operations for view-presets"""

    _session: Session

    def __init__(self, session: Session) -> None:
        self._session = session

    def read_many(self,
                  username: str | None = None,
                  scope: list[ScopeEnum] | None = None,
                  search: list[str] | None = None) -> list[ViewPreset] | None:
        """returns: a list of system view-presets, and optionally
        view-presets for the given user
        :param username: current user
        :param scope: list of strings with scope(s) to be returned
        :param search: list of strings to search for in title and abstract
        :raises SQLAlchemy error:
        """
        # pylint: disable = singleton-comparison

        # build statement - username condition
        if (username_condition :=
                self._get_username_condition(username)) is not None:
            stmt = select(ViewPreset).filter(
                or_(ViewPreset.scope == ScopeEnum.SYSTEM, username_condition,
                    self._shared_username_condition(username)))
        else:
            stmt = select(ViewPreset).filter(
                or_(ViewPreset.scope == ScopeEnum.SYSTEM,
                    ViewPreset.is_shared == True))

        # build statement - scope conditon
        if isinstance(scope, list):
            stmt = stmt.filter(or_(*(ViewPreset.scope == s for s in scope)))

        # build statement - search
        if isinstance(search, list):
            stmt = stmt.filter(
                and_(
                    # element in search should be in title OR abstract
                    or_(
                        func.lower(ViewPreset.title).contains(v),
                        func.lower(ViewPreset.abstract).contains(v))
                    for v in search))

        # build statement - ensure correct ordering
        stmt = stmt.order_by(ViewPreset.scope).order_by(ViewPreset.title)

        # get result
        try:
            logging.info("loading view-presets from database",
                         extra={"username": username or ''})
            return self._session.scalars(stmt).all()
        except SQLAlchemyError as err:
            logging.error('error while loading view-presets from database',
                          extra={
                              'username': username or '',
                              'err': err,
                          })
        return None

    def read_one(self, preset_id: str) -> ViewPreset | None:
        """Reads one view-preset.
        :param preset_id: ID of the preset
        :returns ViewPreset|None:
        :raises ReadError:
        """
        # pylint: disable = singleton-comparison

        logging.info('loading view-preset from database',
                     extra={
                         'preset_id': preset_id,
                     })
        try:
            stmt = select(ViewPreset).filter(ViewPreset.preset_id == preset_id)
            return self._session.scalar(stmt)  # type: ignore
        except SQLAlchemyError as err:
            logging.error('error while loading view-preset from database',
                          extra={
                              'preset_id': preset_id,
                              'err': err,
                          })
            raise ReadError('Error while reading view-preset') from err

    def create(self, data: ViewPresetCreate, username: str,
               is_admin: bool) -> ViewPreset:
        """Creates a view-preset.

        The title should not be taken by a view-preset for this user or by a system view-preset.

        :param data: ViewPresetCreate
        :param username: current user
        :param is_admin: True if user is admin
        :returns: the created preset
        :raises CreateError: if an error occurs
        """

        # Check if title already exists for given user or for global
        if self._title_exists(data.title, username):
            raise CreateError(
                'Preset title already in use, please choose a different title')

        data.id = str(uuid.uuid1())
        preset = ViewPreset(data, username)
        # force scope to user if not admin
        if not is_admin:
            preset.scope = ScopeEnum.USER

        logging.info('inserting view-preset into database',
                     extra={'username': username})
        try:
            self._session.add(preset)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error('error while inserting view-preset into database',
                          extra={
                              'username': username,
                              'err': err,
                          })
            raise CreateError('Error while creating view-preset') from err
        return preset

    def update(self, preset: ViewPreset, data: ViewPresetCreate, username: str,
               is_admin: bool) -> ViewPreset:
        """Updates the provided view-preset for the given username.
        :param preset: the existing view-preset
        :param data: the data to update
        :param username: current
        :param is_admin: True if user is admin
        :returns: the updated view-preset.
        :raises UpdateError:
        """

        # check username
        if preset.scope != ScopeEnum.SYSTEM and preset.username != username:
            raise UpdateError(
                'Preset cannot be edited by current user: username does not match'
            )

        # check if user is admin for system presets
        if preset.scope == ScopeEnum.SYSTEM and not is_admin:
            raise UpdateError(
                'Preset cannot be edited by current user: user is not an admin')

        # check title (if changed)
        if data.title != preset.title and self._title_exists(
                data.title, username):
            raise UpdateError(
                'Preset title already in use, please choose a different title')

        # update preset
        logging.info('updating view-preset in database',
                     extra={
                         'preset_id': preset.preset_id,
                         'username': username,
                     })
        try:
            preset.update(data, preset.preset_id)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error('error while inserting view-preset into database',
                          extra={
                              'preset_id': preset.preset_id,
                              'username': username,
                              'err': err,
                          })
            raise UpdateError('Error updating view-preset') from err

        return preset

    def delete(self, preset_id: str, username: str, is_admin: bool) -> bool:
        """Deletes the view-preset with the given preset_id and username.
        :param preset_id: ID of the preset
        :param username: username of current user
        :param is_admin: True if user is admin
        :raises DeleteError: if not successfully deleted
        """

        # find preset to delete, system presets can be only deleted by admin
        stmt = select(ViewPreset).where(
            or_(
                and_(ViewPreset.scope == ScopeEnum.USER,
                     ViewPreset.preset_id == preset_id,
                     ViewPreset.username == username),
                and_(ViewPreset.scope == ScopeEnum.SYSTEM,
                     ViewPreset.preset_id == preset_id, is_admin)))

        if not (preset := self._session.scalar(stmt)):
            raise DeleteError('Preset cannot be deleted by current user')

        # delete preset
        logging.info('deleting view-preset from database',
                     extra={
                         'preset_id': preset.preset_id,
                         'username': username,
                     })
        try:
            logging.info("Delete view-preset")
            self._session.delete(preset)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error('error while deleting view-preset from database',
                          extra={
                              'preset_id': preset.preset_id,
                              'username': username,
                              'err': err,
                          })
            raise DeleteError('Error deleting preset') from err
        return True

    def system_clear(self) -> bool:
        """Deletes all system view-presets"""

        # delete presets
        try:
            stmt = delete(ViewPreset).where(ViewPreset.scope == 'system')
            self._session.execute(stmt)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error('error while deleting view-presets from database')
            raise DeleteError('Error deleting view-presets') from err
        logging.info('system view-presets deleted successfully')
        return True

    @validate_call
    def seed_create(self, data: ViewPresetCreate) -> ViewPreset:
        """Creates a view-preset during seeding

        :param data: ViewPresetCreate
        :returns: the created preset
        :raises CreateError: if an error occurs
        """
        username = 'global'

        # Check if title already exists for given user or for global
        if self._title_exists(data.title, username):
            raise CreateError(
                'Preset title already in use, please choose a different title')

        preset = ViewPreset(data, username)
        preset.scope = ScopeEnum.SYSTEM

        logging.info('inserting system view-preset into database',
                     extra={'username': username})

        try:
            ## check id exists to create or update a view preset
            stmt = select(ViewPreset).where(
                ViewPreset.preset_id == preset.preset_id)
            _preset_exists: ViewPreset | None = self._session.scalars(
                stmt).first()
            if not _preset_exists:
                logging.info('add new view preset')
                self._session.add(preset)
                self._session.commit()
            else:
                logging.info('update existing view preset')
                _preset_exists.update(jsonable_encoder(preset.presetjson),
                                      _preset_exists.preset_id)
                self._session.commit()
            self._session.close()
        except SQLAlchemyError as err:
            logging.error('error while inserting view-preset into database',
                          extra={
                              'username': username,
                              'err': err,
                          })
            self._session.rollback()
            raise CreateError('Error while creating view-preset') from err
        return preset

    def _title_exists(self, title: str, username: str) -> bool:
        """Checks if the title already exists.
        :param title: title of view-preset
        :param username: current user
        :returns: False if the title is available, True otherwise
        """

        # Note: With Pydantic 2, these expressions will likely be accepted by mypy
        same_user_title = and_(
            ViewPreset.scope == ScopeEnum.USER,  # type: ignore
            ViewPreset.username == username,
            ViewPreset.title == title)
        same_system_title = and_(
            ViewPreset.scope == ScopeEnum.SYSTEM,  # type: ignore
            ViewPreset.title == title)

        logging.info('checking if view-preset title exists',
                     extra={
                         'title': title,
                         'username': username,
                     })

        try:
            stmt = select(ViewPreset).where(
                or_(same_user_title, same_system_title))
            existing = self._session.scalars(stmt).all()
        except SQLAlchemyError as err:
            logging.error('Error while checking if view-preset title exists',
                          extra={
                              'title': title,
                              'username': username,
                              'err': err,
                          })
            return True

        if len(existing) == 0:
            return False

        logging.info('Title already exists for user',
                     extra={
                         'title': title,
                         'username': username,
                     })
        return True

    def _get_username_condition(
            self, username: str | None) -> BooleanClauseList | None:
        """Returns the username condition for the query, or None if no
        (or empty) username was provided"""
        username_condition = None
        if username:
            # Note: With Pydantic 2, these expressions will likely be accepted by mypy
            username_condition = and_(
                ViewPreset.scope == ScopeEnum.USER,  # type: ignore
                ViewPreset.username == username)
        return username_condition

    def _shared_username_condition(
            self, username: str | None) -> BooleanClauseList | None:
        """
        Returns a condition to return shared presets where the username does
        not equal the provided username. Or all shared presets if no username was
        provided"""
        # pylint: disable=singleton-comparison
        if username:
            return and_(ViewPreset.is_shared == True, ViewPreset.username
                        != username)
        return None

    def count_rows(self) -> int:
        """Counts all objects in the view-preset database table

        Returns:
            int: number of objects in the view-preset database table
        """

        # pylint: disable=not-callable
        query = self._session.scalars(
            select(func.count()).select_from(ViewPreset)).all()
        return int(query.pop())
