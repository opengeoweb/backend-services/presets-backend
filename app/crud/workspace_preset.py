# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This module handles the interaction with the database"""
from __future__ import annotations

import logging
import uuid

from fastapi.encoders import jsonable_encoder
from pydantic import validate_call
from sqlalchemy import and_, delete, func, or_, select
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.sql.elements import BooleanClauseList
from sqlmodel import Session

from app.crud.errors import CreateError, DeleteError, ReadError, UpdateError
from app.models import ScopeEnum, WorkspacePreset, WorkspacePresetCreate
from app.utils.utils import view_presets_exists


class CRUD:
    """CRUD operations for workspace-presets"""

    _session: Session

    def __init__(self, session: Session) -> None:
        self._session = session

    def read_many(
            self,
            username: str | None = None,
            scope: list[ScopeEnum] | None = None,
            search: list[str] | None = None) -> list[WorkspacePreset] | None:
        """
        Returns a list of workspace presets
        :param username: current user or None,
                         if None, no user presets will be returned
        :param scope: list of strings with scope(s) to be returned
        :param search: list of strings to search for in title and abstract
        :returns: a list of system workspace-presets, and optionally
        workspace-presets for the given user
        :raises SQLAlchemy error:
        """
        # pylint: disable = singleton-comparison

        # build query - username condition
        if (username_condition :=
                self._get_username_condition(username)) is not None:
            stmt = select(WorkspacePreset).filter(
                or_(WorkspacePreset.scope == ScopeEnum.SYSTEM,
                    username_condition,
                    self._shared_username_condition(username)))
        else:
            stmt = select(WorkspacePreset).filter(
                or_(WorkspacePreset.scope == ScopeEnum.SYSTEM,
                    WorkspacePreset.is_shared == True))

        # build query - scope conditon
        if isinstance(scope, list):
            stmt = stmt.filter(
                or_(*(WorkspacePreset.scope == s for s in scope)))

        # build query - search
        if isinstance(search, list):
            stmt = stmt.filter(
                # elements in "search" should be found in each item
                and_(
                    # element in search should be in title OR abstract
                    or_(
                        func.lower(WorkspacePreset.title).contains(v),
                        func.lower(WorkspacePreset.abstract).contains(v))
                    for v in search))

        # build  statement - ensure correct ordering
        stmt = stmt.order_by(WorkspacePreset.scope).order_by(
            WorkspacePreset.title)

        # get result
        try:
            logging.info("loading workspace-presets from database",
                         extra={"username": username or ''})
            return self._session.scalars(stmt).all()
        except SQLAlchemyError as err:
            logging.error('error while loading workspace-presets from database',
                          extra={
                              'username': username or '',
                              'err': err,
                          })
        return None

    def read_one(self,
                 preset_id: str,
                 username: str | None = None) -> WorkspacePreset | None:
        """Reads one workspace-preset.
        :param preset_id: ID of the preset
        :param username: current user
        :returns WorkspacePreset|None:
        :raises ReadError:
        """
        # pylint: disable = singleton-comparison

        logging.info('loading workspace-preset from database',
                     extra={
                         'preset_id': preset_id,
                         'username': username or '',
                     })
        try:
            stmt = select(WorkspacePreset).filter(
                WorkspacePreset.preset_id == preset_id,
                or_(WorkspacePreset.scope == ScopeEnum.SYSTEM,
                    self._get_username_condition(username)))
            return self._session.scalar(stmt)  # type: ignore
        except SQLAlchemyError as err:
            logging.error('error while loading workspace-preset from database',
                          extra={
                              'preset_id': preset_id,
                              'username': username or '',
                              'err': err,
                          })
            raise ReadError('Error while reading workspace-preset') from err

    def create(self, data: WorkspacePresetCreate, username: str,
               is_admin: bool) -> WorkspacePreset:
        """Creates a workspace-preset.

        The title should not be taken by a workspace-preset for this user
        or by a system workspace-preset.

        :param data: WorkspacePresetCreate
        :param username: current user
        :param is_admin: True if user is admin
        :returns: the created preset
        :raises CreateError: if an error occurs
        """

        # Check if title already exists for given user or for global
        if self._title_exists(data.title, username):
            raise CreateError(
                'Preset title already in use, please choose a different title')

        # Check if specified presets exist before updating the database
        # For this, we retrieve all preset ids from views field
        if not self._view_presets_exists(
            [view.viewPresetId for view in data.views]):
            raise CreateError('Not all view-presets in workspace-preset found,' \
                   ' workspace-preset could not be created')

        data.id = str(uuid.uuid1())
        preset = WorkspacePreset(data, username)
        # force scope to user if not admin
        if not is_admin:
            preset.scope = ScopeEnum.USER

        logging.info('inserting workspace-preset into database',
                     extra={'username': username})
        try:
            self._session.add(preset)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error(
                'error while inserting workspace-preset into database',
                extra={
                    'username': username,
                    'err': err,
                })
            raise CreateError('Error while creating workspace-preset') from err
        return preset

    def update(self, preset: WorkspacePreset, data: WorkspacePresetCreate,
               username: str, is_admin: bool) -> WorkspacePreset:
        """Updates the provided workspace-preset for the given username.
        :param preset: the existing workspace-preset
        :param data: the data to update
        :param username: current user
        :param is_admin: True if user is admin
        :returns: the updated workspace-preset.
        :raises UpdateError: in case of error while trying to update
        """

        # check username if not system preset
        if preset.scope != ScopeEnum.SYSTEM and preset.username != username:
            raise UpdateError(
                'Preset cannot be edited by current user: username does not match'
            )

        # check if user is admin for system presets
        if preset.scope == ScopeEnum.SYSTEM and not is_admin:
            raise UpdateError(
                'Preset cannot be edited by current user: user is not an admin')

        # check title (if changed)
        if data.title != preset.title and self._title_exists(
                data.title, username):
            raise UpdateError(
                'Workspace-preset title already in use, please choose a different title'
            )

        # check if presets exist before updating the database
        if not self._view_presets_exists(
            [view.viewPresetId for view in data.views]):
            raise UpdateError('Not all view-presets in workspace-preset found')

        # update preset
        logging.info('updating workspace-preset in database',
                     extra={
                         'preset_id': preset.preset_id,
                         'username': username,
                     })
        try:
            preset.update(jsonable_encoder(data), preset.preset_id)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error(
                'error while inserting workspace-preset into database',
                extra={
                    'preset_id': preset.preset_id,
                    'username': username,
                    'err': err,
                })
            raise UpdateError('Error updating workspace-preset') from err

        return preset

    def delete(self, preset_id: str, username: str, is_admin: bool) -> bool:
        """Deletes the workspace-preset with the given preset_id and username.
        :param preset_id: ID of the preset
        :param username: username of current user
        :param is_admin: True if user is admin
        :raises DeleteError: if not successfully deleted
        """

        # find preset to delete, system presets can be only deleted by admin
        stmt = select(WorkspacePreset).where(
            or_(
                and_(WorkspacePreset.scope == ScopeEnum.USER,
                     WorkspacePreset.preset_id == preset_id,
                     WorkspacePreset.username == username),
                and_(
                    WorkspacePreset.scope == ScopeEnum.SYSTEM,
                    WorkspacePreset.preset_id == preset_id,
                    is_admin,
                )))

        if not (preset := self._session.scalar(stmt)):
            raise DeleteError('Preset cannot be deleted by current user')

        # delete preset
        logging.info('deleting workspace-preset from database',
                     extra={
                         'preset_id': preset.preset_id,
                         'username': username,
                     })

        # delete preset
        logging.info("Delete personal workspace-preset")
        try:
            self._session.delete(preset)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error('error while deleting workspace-preset from database',
                          extra={
                              'preset_id': preset.preset_id,
                              'username': username,
                              'err': err,
                          })
            raise DeleteError('Error deleting preset') from err
        return True

    def system_clear(self) -> bool:
        """Deletes all system workspace-presets"""

        # delete presets
        try:
            stmt = delete(WorkspacePreset).where(
                WorkspacePreset.scope == 'system')
            self._session.execute(stmt)
            self._session.commit()
        except SQLAlchemyError as err:
            logging.error(
                'error while deleting workspace-presets from database')
            raise DeleteError('Error deleting workspace-presets') from err
        logging.info('system workspace-presets deleted successfully')
        return True

    @validate_call
    def seed_create(self, data: WorkspacePresetCreate) -> WorkspacePreset:
        """Creates a workspace-preset during seeding

        :param data: WorkspacePresetCreate
        :returns: the created preset
        :raises CreateError: if an error occurs
        """
        username = 'global'

        # Check if title already exists for given user or for global
        if self._title_exists(data.title, username):
            raise CreateError(
                'Preset title already in use, please choose a different title')

        # check if specified presets exist before updating the database
        if not self._view_presets_exists(
            [view.viewPresetId for view in data.views]):
            raise CreateError(
                'Not all mappresets in workspace preset found, workspace preset could not be saved'
            )

        preset = WorkspacePreset(data, username)
        preset.scope = ScopeEnum.SYSTEM

        logging.info('inserting system workspace-preset into database',
                     extra={'username': username})

        try:
            ## check id exists to create or update a workspace preset
            stmt = select(WorkspacePreset).where(
                WorkspacePreset.preset_id == preset.preset_id)
            _preset_exists: WorkspacePreset | None = self._session.scalars(
                stmt).first()
            if not _preset_exists:
                logging.info('add new workspace preset')
                self._session.add(preset)
                self._session.commit()
            else:
                logging.info('update existing workspace preset')
                _preset_exists.update(jsonable_encoder(preset.presetjson),
                                      _preset_exists.preset_id)
                self._session.commit()
            self._session.close()
        except SQLAlchemyError as err:
            logging.error(
                'error while inserting workspace-preset into database',
                extra={
                    'username': username,
                    'err': err,
                })
            self._session.rollback()
            raise CreateError('Error while creating workspace-preset') from err
        return preset

    def _title_exists(self, title: str, username: str) -> bool:
        """Checks if the title already exists.
        :param title: title of workspace-preset
        :param username: current user
        :returns: False if the title is available, True otherwise
        """

        # Note: With Pydantic 2, these expressions will likely be accepted by mypy
        same_user_title = and_(
            WorkspacePreset.scope == ScopeEnum.USER,  # type: ignore
            WorkspacePreset.username == username,
            WorkspacePreset.title == title)
        same_system_title = and_(
            WorkspacePreset.scope == ScopeEnum.SYSTEM,  # type: ignore
            WorkspacePreset.title == title)

        logging.info('checking if workspace-preset title exists',
                     extra={
                         'title': title,
                         'username': username,
                     })

        try:
            stmt = select(WorkspacePreset).where(
                or_(same_user_title, same_system_title))
            existing = self._session.scalars(stmt).all()
        except SQLAlchemyError as err:
            logging.error(
                'Error while checking if workspace-preset title exists',
                extra={
                    'title': title,
                    'username': username,
                    'err': err,
                })
            return True

        if len(existing) == 0:
            return False

        logging.info('Title already exists for user',
                     extra={
                         'title': title,
                         'username': username,
                     })
        return True

    def _get_username_condition(
            self, username: str | None) -> BooleanClauseList | None:
        """Returns the username condition for the query, or None if no
        (or empty) username was provided"""
        username_condition = None
        if username:
            # Note: With Pydantic 2, these expressions will likely be accepted by mypy
            username_condition = and_(
                WorkspacePreset.scope == ScopeEnum.USER,  # type: ignore
                WorkspacePreset.username == username)
        return username_condition

    def _shared_username_condition(
            self, username: str | None) -> BooleanClauseList | None:
        """
        Returns a condition to return shared presets where the username does
        not equal the provided username. Or all shared presets if no username was
        provided"""
        # pylint: disable=singleton-comparison
        if username:
            return and_(WorkspacePreset.is_shared == True,
                        WorkspacePreset.username != username)
        return None

    def _view_presets_exists(self, view_presets: list[str]) -> bool:
        """Returns True if all map-presets used in a workspace-preset
        exist before saving a workspace-preset. Returns False if not
        all presets are found"""
        return view_presets_exists(view_presets, self._session)

    def count_rows(self) -> int:
        """Counts all objects in the workspace-preset database table

        Returns:
            int: number of objects in the workspace-preset database table
        """

        # pylint: disable=not-callable
        count = self._session.scalars(
            select(func.count()).select_from(WorkspacePreset)).all()
        return int(count.pop())
