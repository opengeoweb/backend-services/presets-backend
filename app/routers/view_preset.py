# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Router for view-preset related routes"""
from __future__ import annotations

import logging

from fastapi import (
    APIRouter,
    Depends,
    Header,
    HTTPException,
    Path,
    Request,
    Response,
)
from pydantic import ValidationError
from sqlmodel import Session

from app.crud.errors import CreateError, DeleteError, ReadError, UpdateError
from app.crud.view_preset import CRUD
from app.db import get_session
from app.models import (
    ScopeEnum,
    ViewPreset,
    ViewPresetCreate,
    ViewPresetDetail,
    ViewPresetSummary,
)
from app.utils.utils import _check_authenticated, _check_is_admin

router = APIRouter()


@router.get('/viewpreset',
            response_model=list[ViewPresetSummary],
            response_model_exclude_unset=True,
            response_model_exclude_none=True)
@router.get('/viewpreset/',
            response_model=list[ViewPresetSummary],
            response_model_exclude_unset=True,
            response_model_exclude_none=True)
async def list_view_presets(
        # pylint: disable = too-many-positional-arguments
        response: Response,
        username: str | None = Header(default=None, alias='Geoweb-Username'),
        roles: str | None = Header(default=None, alias='Geoweb-Roles'),
        session: Session = Depends(get_session),
        scope: str | None = None,
        search: str | None = None) -> list[ViewPresetSummary]:
    """Handler for getting a view-preset list."""

    response.headers['username'] = username or ''
    response.headers['roles'] = roles or ''

    # prepare scope parameter for query
    stripped_scope: list[ScopeEnum] | None = [
        ScopeEnum(v.strip())
        for v in scope.lower().split(',')
        if v.strip().upper() in ScopeEnum.__members__
    ] if scope else None

    # Specific case where all scopes provided are invalid
    if scope is not None and not stripped_scope:
        return []

    # prepare search parameter for query
    stripped_search: list[str] | None = [
        v.strip() for v in search.lower().split(',')
    ] if search else None

    # get view-presets for user
    presets: list[ViewPreset] | None = CRUD(session).read_many(
        username, stripped_scope, stripped_search)

    # process result
    # database returns ViewPreset
    # try transformation into ViewPresetSummary format
    result = []
    if presets:
        for preset in presets:
            try:
                result.append(
                    ViewPresetSummary.model_validate({
                        'id': preset.preset_id,
                        'title': preset.title,
                        'abstract': preset.abstract,
                        'date': preset.created_on.isoformat(),
                        'scope': preset.scope,
                        'username': preset.username,
                        'is_shared': preset.is_shared
                    }))
            # log a warning and continue if invalid preset
            # is processed
            except ValidationError as err:
                msg = 'Invalid view-preset retrieved from database'
                logging.warning(msg,
                                extra={
                                    'info': str(err),
                                    'preset_id': preset.preset_id
                                })
    return result


@router.get('/viewpreset/{preset_id}',
            response_model=ViewPresetDetail,
            response_model_exclude_unset=True,
            response_model_exclude_none=True)
async def get_view_preset(
    response: Response,
    preset_id: str = Path(),
    username: str | None = Header(alias='Geoweb-Username', default=None),
    roles: str | None = Header(alias='Geoweb-Roles', default=None),
    session: Session = Depends(get_session)
) -> ViewPresetDetail:
    """Returns the view-preset with the given preset_id, if allowed for the current user."""

    response.headers['username'] = username or ''
    response.headers['roles'] = roles or ''

    try:
        preset = CRUD(session).read_one(preset_id)
    except ReadError as err:
        raise HTTPException(status_code=400, detail=err) from err

    if preset is None:
        raise HTTPException(status_code=404, detail='View-preset not found')

    try:
        return ViewPresetDetail.model_validate(  # type: ignore
            preset.presetjson)
    except ValidationError as err:
        msg = 'Invalid view-preset retrieved from database'
        logging.error(msg,
                      extra={
                          'info': str(err),
                          'preset_id': preset.preset_id
                      })
        raise HTTPException(status_code=400, detail=msg) from err


@router.post('/viewpreset', status_code=200)
@router.post('/viewpreset/', status_code=200)
def post_view_preset(
    # pylint: disable = too-many-positional-arguments
    preset: ViewPresetCreate,
    request: Request,
    response: Response,
    username: str = Header(alias='Geoweb-Username'),
    roles: str | None = Header(alias='Geoweb-Roles', default=None),
    session: Session = Depends(get_session)
) -> None:
    """Saves the given view-preset with a new ID"""

    _check_authenticated(response, username)
    is_admin = _check_is_admin(roles)

    # store view-preset in database
    try:
        if not is_admin:
            preset.scope = ScopeEnum.USER  # force scope to user if not admin
        created_preset = CRUD(session).create(preset, username, is_admin)
    except CreateError as err:
        raise HTTPException(status_code=400, detail=str(err)) from err

    # set location header
    response.headers['Location'] = str(
        request.url_for('get_view_preset', preset_id=created_preset.preset_id))


@router.put('/viewpreset/{preset_id}', status_code=201)
@router.put('/viewpreset/{preset_id}/', status_code=201)
def put_view_preset(
    # pylint: disable = too-many-positional-arguments
    data: ViewPresetCreate,
    response: Response,
    preset_id: str = Path(),
    username: str = Header(alias='Geoweb-Username'),
    roles: str | None = Header(alias='Geoweb-Roles', default=None),
    session: Session = Depends(get_session)
) -> None:
    """Updates a user view-preset"""

    _check_authenticated(response, username)
    is_admin = _check_is_admin(roles)

    # load existing preset
    crud = CRUD(session)
    if not (old_preset := crud.read_one(preset_id)):
        raise HTTPException(status_code=400,
                            detail='Preset could not be found for current user')

    # update preset in database
    try:
        crud.update(old_preset, data, username, is_admin)
    except UpdateError as err:
        logging.error(err)
        raise HTTPException(status_code=400,
                            detail='Could not save preset') from err


@router.delete('/viewpreset/{preset_id}', status_code=204)
def delete_view_preset(response: Response,
                       preset_id: str = Path(),
                       username: str = Header(alias='Geoweb-Username'),
                       roles: str | None = Header(alias='Geoweb-Roles',
                                                  default=None),
                       session: Session = Depends(get_session)):
    """Deletes user view-preset"""

    _check_authenticated(response, username)
    is_admin = _check_is_admin(roles)

    # delete preset
    try:
        CRUD(session).delete(preset_id, username, is_admin)
    except DeleteError as err:
        raise HTTPException(status_code=403,
                            detail=f'Could not delete preset: {err}') from err
