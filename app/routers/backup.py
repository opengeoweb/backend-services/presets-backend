# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Router for view-preset related routes"""
from __future__ import annotations

import logging

from fastapi import APIRouter, Depends, Header, HTTPException, Response
from sqlalchemy.orm import Session

from app.db import get_session
from app.utils.backup import get_backup_ids, make_backup, restore_backup
from app.utils.utils import _check_authenticated, _check_is_admin

router = APIRouter()


@router.get('/backup/list')
async def get_retrieve_backup_id_list(
    response: Response,
    username: str | None = Header(default=None, alias='Geoweb-Username'),
    roles: str | None = Header(default=None, alias='Geoweb-Roles'),
) -> list[str]:
    """Lists all backups"""
    _check_authenticated(response, username)
    if not _check_is_admin(roles):
        raise HTTPException(status_code=403, detail='Not authorized')
    try:
        return get_backup_ids()
    except Exception as exc:
        raise HTTPException(status_code=400, detail='Could not list') from exc


@router.get('/backup/restore')
def get_restore_backup(
    response: Response,
    backup: str,
    user: str | None = None,
    username: str = Header(alias='Geoweb-Username'),
    roles: str | None = Header(alias='Geoweb-Roles', default=None),
    session: Session = Depends(get_session)
) -> object:
    # pylint: disable=too-many-positional-arguments
    """Restores a backup file."""
    _check_authenticated(response, username)
    if not _check_is_admin(roles):
        raise HTTPException(status_code=403, detail='Not authorized')
    # restore it
    try:
        logging.info("Restoring backup %s for user %s by admin %s", backup,
                     user, username)
        if restore_backup(session, backup, user) is True:
            return {"status": "ok"}
        raise ValueError("Failed")
    except Exception as exc:
        logging.exception(exc, exc_info=True)
        raise HTTPException(status_code=400,
                            detail='Could not restore: Check logs') from exc


@router.get('/backup/make')
def get_make_backup(
    response: Response,
    username: str = Header(alias='Geoweb-Username'),
    roles: str | None = Header(alias='Geoweb-Roles', default=None),
    session: Session = Depends(get_session)
) -> object:
    """Trigger a backup outside of the backup scheduler"""
    _check_authenticated(response, username)
    if not _check_is_admin(roles):
        raise HTTPException(status_code=403, detail='Not authorized')
    # restore it
    try:
        logging.info("Making backup by admin %s", username)
        if make_backup(session) is True:
            return {"status": "ok"}
        raise ValueError("Failed")
    except Exception as exc:
        logging.exception(exc, exc_info=True)
        raise HTTPException(status_code=400,
                            detail='Could not make backup: Check logs') from exc
