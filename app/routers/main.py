# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
This modules defines the base endpoints for this application.
"""
import logging

from fastapi import APIRouter, Depends, HTTPException
from fastapi.responses import PlainTextResponse
from sqlalchemy.orm import Session

from app.config import settings
from app.crud.view_preset import CRUD as viewCRUD
from app.crud.workspace_preset import CRUD as workspaceCRUD
from app.db import get_session

router = APIRouter()

logger = logging.getLogger(__name__)


@router.get('/')
async def get_root():
    """Returns a welcome message"""
    return PlainTextResponse('GeoWeb Presets API')


@router.get('/healthcheck')
async def get_healthcheck(session: Session = Depends(get_session)):
    """Checks healthcheck information of the service

    Returns:
        JSON containing the healthcheck status

    Example:
        Successful response::

            {
              "database": {
                "status": "OK",
                "view-presets": {
                  "status": "OK"
                },
                "workspace-presets": {
                  "status": "OK"
                }
              },
              "status": "OK",
              "service": "Presets"
            }
        Response with failure::

            {
              "database": {
                "status": "FAILED"
              },
              "status": "FAILED",
              "service": "Presets"
            }
    """

    status_message: dict = {
        "database": {
            "status": "OK"
        },
        "status": "OK",
        "service": "Presets"
    }
    try:
        tables: dict = {
            "view-presets": viewCRUD,
            "workspace-presets": workspaceCRUD
        }
        for table, crud_class in tables.items():
            crud = crud_class(session)
            preset_count = crud.count_rows()
            # pylint: disable=consider-using-assignment-expr
            if preset_count > 0:
                status_message["database"][table] = {"status": "OK"}
            else:
                status_message["database"][table] = {"status": "FAILED"}
                status_message["status"] = "FAILED"
                status_message["database"].update({"status": "FAILED"})

    except Exception as e:  # pylint: disable=broad-except
        logger.error(e)
        status_message["database"].update({"status": "FAILED"})
        status_message["status"] = "FAILED"

    if status_message["status"] == "FAILED":
        logger.error("Healthcheck failed: %s", status_message)
        raise HTTPException(503, detail=status_message)
    return status_message


@router.get('/version')
async def get_version():
    """Returns the application's version"""
    return {'version': settings.version}
