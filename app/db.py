# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This module handles the interaction with the database"""

import json
import logging

from sqlalchemy import create_engine, insert, text, update
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm import Session

from app.config import settings
from app.models import SQLModel
from app.utils.constants import ALEMBIC_VERSION_SKIP

engine = create_engine(settings.presets_backend_db,
                       pool_recycle=120,
                       pool_pre_ping=True)


def get_session():
    """Yields a database session"""
    with Session(engine) as session:
        yield session


def query_alembic_version(session):
    """Retrieve alembic version with a db query"""
    with session:
        try:
            result = session.execute(
                text("SELECT version_num FROM alembic_version"))
            revision = result.scalar()
        except OperationalError:
            logging.error("alembic_version table not found in database")
            return ""
        if revision:
            logging.info("Current Alembic revision: %s", revision)
            return revision
        logging.error("alembic_version table is empty")
        return ""


def dump_sqlalchemy(session: Session | None = None):
    """ Returns the entire content of a database as lists of dicts"""
    meta = SQLModel.metadata
    result = {}

    # if no session was passed, set it up here
    if not session:
        session = Session(engine)

    with session:
        for table in meta.sorted_tables:
            table_rows = session.execute(table.select())
            resultrows = [
                dict(zip(table.columns.keys(), list(row))) for row in table_rows
            ]
            result[table.name] = resultrows
            session.commit()
    # format alembic_version for backwards compatibility
    if 'alembic_version' not in result:
        result['alembic_version'] = [{
            "version_num": query_alembic_version(session)
        }]
    return json.dumps(result, default=str)


def get_sqlalchemy_table(meta, tablename):
    """ Get sqlalchemy database table by tablename """
    presetsdbtable = [
        tables for tables in meta.sorted_tables if tables.name == tablename
    ][0]
    return presetsdbtable


def check_alembic_version(backupdumpjson, session) -> bool:
    """Ensure that alembic version in backup file is the same as in the database table"""
    if (dumpsjon := backupdumpjson.get('alembic_version')) and isinstance(
            dumpsjon, list) and len(dumpsjon) > 0:
        alembic_version_num_in_backupfile = dumpsjon[0].get('version_num')
    else:
        logging.warning("Could not retrieve alembic_version from dumpfile")
        return False
    if alembic_version_num_in_backupfile == ALEMBIC_VERSION_SKIP:
        return True
    alembic_version_num_in_db = query_alembic_version(session)
    if alembic_version_num_in_db != alembic_version_num_in_backupfile:
        logging.info("Alembic version differs")
        raise AssertionError(
            f'Alembic version in backup file [{alembic_version_num_in_backupfile}]'
            f' differs from version in database [{alembic_version_num_in_db}]')
    return True


def restore_sqlalchemy(session: Session, dump, username=None) -> bool:
    """ Restores the entire content of a database from a lists of dicts,
        table needs to exist prior to restore"""
    logging.info("restore_sqlalchemy")
    meta = SQLModel.metadata
    logging.info("restore_sqlalchemy Session")
    with session:
        # check alembic version match
        logging.info("Checking alembic version")
        if not check_alembic_version(dump, session):
            return False
        # Loop through found tablenames in json file
        for tablename in dump:
            if tablename in {'viewpresets', 'workspacepresets'}:
                logging.info("Restoring %s", tablename)
                # Get database table by tablename
                if (presetsdbtable := get_sqlalchemy_table(meta,
                                                           tablename)) is None:
                    logging.info(
                        "Table %s does not exist. First run admin script?",
                        tablename)
                    return False
                # Get the data to restore from the json
                preset_contents_to_restore = dump[tablename]
                logging.info("preset_contents_to_restore has  %d items ",
                             len(preset_contents_to_restore))
                for item_to_upsert in preset_contents_to_restore:
                    if (username is None or 'username' not in item_to_upsert or
                            username == item_to_upsert['username']):
                        if session.query(presetsdbtable).filter_by(
                                preset_id=item_to_upsert['preset_id']).first():
                            # Update existing preset
                            logging.info("Updating preset %s",
                                         item_to_upsert['preset_id'])
                            session.execute(
                                update(presetsdbtable).where(
                                    presetsdbtable.c.preset_id ==
                                    item_to_upsert['preset_id']).values(
                                        **item_to_upsert))
                        else:
                            # New preset
                            logging.info("New preset %s",
                                         item_to_upsert['preset_id'])
                            session.execute(
                                insert(presetsdbtable).values(**item_to_upsert))
                    else:
                        logging.info(
                            "Skipping because username %s does not match",
                            username)
                session.commit()
    return True
