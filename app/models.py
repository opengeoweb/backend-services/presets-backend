# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
'Models mapping to the database'
from __future__ import annotations

from datetime import datetime, timezone
from enum import Enum

from fastapi.encoders import jsonable_encoder
from pydantic import BaseModel
from sqlmodel import JSON, Column, Field, SQLModel


class ScopeEnum(str, Enum):
    """Enumeration of possible scope values"""
    SYSTEM = 'system'
    USER = 'user'


class ViewPresetCreate(BaseModel):
    """Model for creating a view-preset"""
    id: str | None = None
    title: str
    abstract: str | None = None
    scope: ScopeEnum
    keywords: str
    initialProps: dict = {}
    componentType: str | None = None
    is_shared: bool | None = None


class ViewPreset(SQLModel, table=True):  # type: ignore
    # pylint: disable=too-many-instance-attributes
    'Class representing view-presets stored in database table viewpresets'

    __tablename__ = 'viewpresets'

    preset_id: str = Field(primary_key=True)
    username: str
    preset_type: str  # 'map'
    title: str
    abstract: str | None = None
    keywords: str = Field(default='')
    created_on: datetime
    presetjson: dict = Field(default={}, sa_column=Column(JSON))
    scope: str  # 'system' or 'user'
    is_shared: bool | None = None

    def __init__(self,
                 preset: ViewPresetCreate,
                 username: str = 'global',
                 preset_type: str = 'map') -> None:
        super().__init__()

        self.presetjson = dict(preset)
        self.preset_type = preset_type
        self.username = username
        self.created_on = datetime.now(tz=timezone.utc)

        self.preset_id = self.presetjson['id']
        self.scope = self.presetjson['scope']
        self.update_derived_attrs()

    def update(self, data: ViewPresetCreate, preset_id: str) -> None:
        """Updates the contents of the view-preset with the provided data"""
        self.presetjson = dict(data, id=preset_id)  #Set id in presetjson
        print(self.presetjson.get('is_shared'))
        self.update_derived_attrs()

    def update_derived_attrs(self) -> None:
        """Updates attributes that are derived from the provided data"""
        self.title = self.presetjson['title']
        self.keywords = self.presetjson['keywords']
        self.abstract = self.presetjson.get('abstract', None)
        if (shared := self.presetjson.get('is_shared')) is not None:
            self.is_shared = shared


class ViewPresetDetail(BaseModel):
    """View-preset summary"""
    id: str  #= fields.Str(required=True)
    title: str  #= fields.Str(required=True)
    scope: ScopeEnum  #= Field(required=True, validate=OneOf(["user", "system"]))
    abstract: str | None = None
    keywords: str
    initialProps: dict = {}
    componentType: str | None = None


class ViewPresetSummary(BaseModel):
    """View-preset summary, used in a collection resonse"""
    id: str  #= fields.Str(required=True)
    date: datetime  #= fields.DateTime(format="iso", required=True)
    title: str  #= fields.Str(required=True)
    scope: ScopeEnum  #= Field(required=True, validate=OneOf(["user", "system"]))
    abstract: str | None = None
    username: str
    is_shared: bool | None = None


class ViewTypeEnum(str, Enum):
    '''Defintion of viewTypes'''
    SINGLE_WINDOW = 'singleWindow'
    MULTI_WINDOW = 'multiWindow'
    TILED_WINDOW = 'tiledWindow'


class ViewDefinition(BaseModel):
    '''Definition of a view'''
    mosaicNodeId: str
    viewPresetId: str


class SyncGroupDefinition(BaseModel):
    '''Definition of syncGroups fields'''
    id: str
    type: str


class MosaicNodeDefinition(BaseModel):
    '''Definition of mosaicNode class'''
    direction: str  # row, column
    first: str | MosaicNodeDefinition
    second: str | MosaicNodeDefinition
    splitPercentage: float | None = None


class WorkspacePresetCreate(BaseModel):
    '''Class for workspace creation'''
    id: str | None = None
    title: str
    views: list[ViewDefinition]
    syncGroups: list[SyncGroupDefinition] | None = None
    viewType: ViewTypeEnum
    mosaicNode: str | MosaicNodeDefinition
    abstract: str | None = None
    keywords: str | None = None
    scope: ScopeEnum
    isTimeScrollingEnabled: bool | None = None
    linking: dict | None = None
    is_shared: bool | None = None


class WorkspacePreset(SQLModel, table=True):  # type: ignore
    # pylint: disable=too-many-instance-attributes
    """Class represeting workspace-presets stored in database"""

    __tablename__ = 'workspacepresets'

    preset_id: str = Field(primary_key=True)
    username: str
    scope: str
    title: str
    keywords: str | None = Field(default=None)
    abstract: str | None = None
    created_on: datetime
    presetjson: dict = Field(default={}, sa_column=Column(JSON))
    is_shared: bool | None = None

    def __init__(self,
                 preset: WorkspacePresetCreate,
                 username: str = 'global') -> None:
        super().__init__()

        self.presetjson = jsonable_encoder(preset)
        self.username = username
        self.created_on = datetime.now(tz=timezone.utc)

        self.preset_id = self.presetjson['id']
        self.scope = self.presetjson['scope']
        self.update_derived_attrs()

    def update(self, data: WorkspacePresetCreate, preset_id: str) -> None:
        """Updates the contents of the view-preset with the provided data"""
        self.presetjson = dict(data, id=preset_id)  #Set id in presetjson
        self.update_derived_attrs()

    def update_derived_attrs(self) -> None:
        """Updates attributes that are derived from the provided data"""
        self.title = self.presetjson['title']
        self.keywords = self.presetjson['keywords']
        self.abstract = self.presetjson['abstract']
        if (shared := self.presetjson.get('is_shared')) is not None:
            self.is_shared = shared


class WorkspacePresetDetail(BaseModel):
    """Workspace preset detail"""
    id: str
    title: str
    scope: ScopeEnum
    abstract: str | None = None
    viewType: ViewTypeEnum
    views: list[ViewDefinition]
    syncGroups: list[SyncGroupDefinition] | None = None
    mosaicNode: str | MosaicNodeDefinition
    isTimeScrollingEnabled: bool | None = None
    linking: dict | None = None


class WorkspacePresetSummary(BaseModel):
    '''Workspace preset summary'''
    id: str
    title: str
    date: datetime
    scope: ScopeEnum
    abstract: str | None = None
    viewType: ViewTypeEnum
    username: str
    is_shared: bool | None = None
