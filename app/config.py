# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Module for application-wide settings"""
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    """BaseSettings class"""
    version: str = "0.0.0"
    log_level: str = "WARN"
    presets_backend_db: str = 'postgresql://geoweb:geoweb@localhost:5432/presetsbackenddb'

    # Logging configuration
    enable_console_logger_handler: bool = False
    enable_uvicorn_error_console_handler: bool = False
    enable_uvicorn_access_console_handler: bool = False

    # Application path values
    application_root_path: str = ""
    application_doc_root: str = "/api"

    model_config = SettingsConfigDict(env_file='.env',
                                      env_file_encoding='utf-8',
                                      extra='allow')


settings = Settings()
