# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Fixtures for tests"""
import json
from collections.abc import Generator

import pytest
from fastapi import FastAPI
from fastapi.testclient import TestClient
from sqlmodel import Session, SQLModel, create_engine
from sqlmodel.pool import StaticPool

from app.db import get_session
from app.main import app as test_app
from app.models import (
    ViewDefinition,
    ViewPreset,
    ViewPresetCreate,
    WorkspacePreset,
    WorkspacePresetCreate,
)

# pylint: disable=unused-argument,redefined-outer-name


@pytest.fixture
def session():
    """Yields a database session fixture"""
    engine = create_engine('sqlite://',
                           connect_args={'check_same_thread': False},
                           poolclass=StaticPool)
    SQLModel.metadata.create_all(engine)
    with Session(engine) as session:
        yield session


@pytest.fixture
def app() -> Generator[FastAPI]:
    """Yields the application configured in testing mode"""
    yield test_app


@pytest.fixture
def client(app: FastAPI, session: Session) -> Generator[TestClient]:
    """Yields a test client"""

    def get_session_override():
        return session

    app.dependency_overrides[get_session] = get_session_override
    client = TestClient(app)
    yield client
    app.dependency_overrides.clear()


# @pytest.fixture
# def db(app: FastAPI) -> Generator[SQLAlchemy, None, None]:
#     """Yields a configured db instance"""
#     sqlalchemy.create_all()
#     yield sqlalchemy
#     sqlalchemy.session.remove()
#     sqlalchemy.drop_all()


@pytest.fixture
def view_presets():
    """Returns a list of view-presets"""
    return [
        ViewPreset(preset=ViewPresetCreate(
            id='system-1',
            title='System Preset #1',
            scope='system',
            keywords='test,preset,system',
            componentType='map',
            initialProps={'test': 'test'},
        )),
        ViewPreset(preset=ViewPresetCreate(
            id='system-2',
            title='System Preset #2',
            scope='system',
            keywords='test,preset,system',
            componentType='map',
            initialProps={'test': 'test'},
        )),
        ViewPreset(preset=ViewPresetCreate(
            id='user-1',
            scope='user',
            title='User Preset #1',
            keywords='test,preset,user',
            componentType='map',
            initialProps={'test': 'test'},
        ),
                   username='test-user-1'),
        ViewPreset(preset=ViewPresetCreate(
            id='user-2',
            scope='user',
            title='User Preset #2',
            keywords='test,preset,user',
            componentType='map',
            initialProps={'test': 'test'},
        ),
                   username='test-user-2'),
        ViewPreset(preset=ViewPresetCreate(
            id='user-3',
            scope='user',
            title='Shared User Preset #3',
            keywords='test,preset,user,shared',
            is_shared=True,
            componentType='map',
            initialProps={'test': 'test'},
        ),
                   username='test-user-3'),
    ]


@pytest.fixture
def seeded_view_presets(app: FastAPI, session: Session,
                        view_presets: list[ViewPreset]) -> None:
    """Seeds a table with view_presets"""

    filename = './app/tests/testdata/view-presets.json'
    with open(filename, 'rb') as fh:
        for preset in json.load(fh):
            session.add(ViewPreset(ViewPresetCreate(**preset)))
        session.commit()

    # seed table
    for preset in view_presets:
        session.add(preset)
    session.commit()


@pytest.fixture
def workspace_presets():
    """Returns a list of workspace-presets"""
    return [
        WorkspacePreset(preset=WorkspacePresetCreate(
            id='system-1',
            title='System Preset #1',
            views=[ViewDefinition(mosaicNodeId='A', viewPresetId='test')],
            keywords='test,preset,system',
            viewType='singleWindow',
            mosaicNode='A',
            scope='system'),
                        username='global'),
        WorkspacePreset(preset=WorkspacePresetCreate(
            id='system-2',
            title='System Preset #2',
            views=[ViewDefinition(mosaicNodeId='A', viewPresetId='test')],
            keywords='test,preset,system',
            viewType='singleWindow',
            mosaicNode='A',
            scope='system'),
                        username='global'),
        WorkspacePreset(preset=WorkspacePresetCreate(
            id='user-1',
            title='User Preset #1',
            views=[ViewDefinition(mosaicNodeId='A', viewPresetId='test')],
            keywords='test,preset,user',
            viewType='singleWindow',
            mosaicNode='A',
            scope='user'),
                        username='test-user-1'),
        WorkspacePreset(preset=WorkspacePresetCreate(
            id='user-2',
            title='User Preset #2',
            views=[ViewDefinition(mosaicNodeId='A', viewPresetId='test')],
            keywords='test,preset,user',
            viewType='singleWindow',
            mosaicNode='A',
            scope='user',
            isTimeScrollingEnabled=True,
            linking={'test': 'test'}),
                        username='test-user-2'),
        WorkspacePreset(preset=WorkspacePresetCreate(
            id='user-3',
            title='User Preset #3',
            views=[ViewDefinition(mosaicNodeId='A', viewPresetId='test')],
            keywords='test,preset,user',
            viewType='singleWindow',
            mosaicNode='A',
            scope='user',
            isTimeScrollingEnabled=True,
            is_shared=True,
            linking={'test': 'test'}),
                        username='test-user-3')
    ]


@pytest.fixture
def seeded_workspace_presets(app: FastAPI, session: Session,
                             workspace_presets: list[WorkspacePreset]) -> None:
    """Seeds a table with workspace_presets"""

    filename = './app/tests/testdata/workspace-presets.json'
    with open(filename, 'rb') as fh:
        for preset in json.load(fh):
            session.add(WorkspacePreset(WorkspacePresetCreate(**preset)))
        session.commit()

    # seed table
    for preset in workspace_presets:
        session.add(preset)
    session.commit()
