# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for view-preset CRUD actions"""

import pytest
from sqlmodel import Session

from app.crud.errors import CreateError, DeleteError, UpdateError
from app.crud.view_preset import CRUD
from app.models import ScopeEnum, ViewPreset, ViewPresetCreate

# pylint: disable=unused-argument


def test_read_many(session: Session, seeded_view_presets: None) -> None:
    """Tests read_many"""

    view_preset = CRUD(session)
    presets = view_preset.read_many()
    assert presets is not None
    assert len(presets) == 12

    presets = view_preset.read_many('')
    assert presets is not None
    assert len(presets) == 12

    # users can only read_many system presets and their own presets
    presets = view_preset.read_many('test-user-1')
    assert presets is not None
    assert len(presets) == 13

    # users can only read_many system presets and their own presets
    presets = view_preset.read_many('test-user-2')
    assert presets is not None
    assert len(presets) == 13


def test_read_one(session: Session, seeded_view_presets: None) -> None:
    """Tests read_one"""

    view_preset = CRUD(session)

    preset = view_preset.read_one('system-1')
    assert preset is not None
    assert preset.preset_id == 'system-1'

    # any user can read any view-preset by ID
    preset = view_preset.read_one('user-1')
    assert preset is not None
    assert preset.preset_id == 'user-1'

    # any user can read any view-preset by ID
    preset = view_preset.read_one('user-2')
    assert preset is not None
    assert preset.preset_id == 'user-2'


def test_create(session: Session) -> None:
    """Tests create"""

    data = ViewPresetCreate(
        title='my-view-preset',
        scope=ScopeEnum.USER,
        keywords='',
        componentType='map',
        initialProps={},
    )

    view_preset = CRUD(session)

    # insert view-preset
    preset: ViewPreset = view_preset.create(data, 'test-user-1', False)
    assert preset is not None
    assert preset.preset_id is not None
    assert preset.username == 'test-user-1'
    # if is_shared not set, it defaults to None
    assert not preset.is_shared

    # insert view-preset with same title and username
    with pytest.raises(
            CreateError,
            match='Preset title already in use, please choose a different title'
    ):
        view_preset.create(data, 'test-user-1', False)


def test_update(session: Session, view_presets: list[ViewPreset]) -> None:
    """Tests update"""

    crud = CRUD(session)

    view_preset = ViewPresetCreate(
        title='my-view-preset',
        scope=ScopeEnum.USER,
        keywords='',
        componentType='map',
        initialProps={},
    )

    # insert view-preset
    preset: ViewPreset = crud.create(view_preset, 'test-user-1', False)

    # update with other username
    with pytest.raises(UpdateError,
                       match='Preset cannot be edited by current user'):
        crud.update(preset, view_preset, 'test-user-2', False)

    # insert view-preset
    view_preset = ViewPresetCreate(
        title='another-view-preset',
        scope=ScopeEnum.USER,
        keywords='',
        componentType='map',
        initialProps={},
    )
    crud.create(view_preset, 'test-user-1', False)

    # update the first preset with the title of the second preset, using the same username
    with pytest.raises(
            UpdateError,
            match='Preset title already in use, please choose a different title'
    ):
        crud.update(preset, view_preset, 'test-user-1', False)

    # update the first preset with the title of the second preset, using the same username
    view_preset.title = 'a-complete-new-title'
    crud.update(preset, view_preset, 'test-user-1', False)

    # insert system view-preset
    system_view_preset = ViewPresetCreate(
        title='system-view-preset',
        scope=ScopeEnum.SYSTEM,
        keywords='',
        componentType='map',
        initialProps={},
    )
    old_system_view_preset: ViewPreset = crud.create(system_view_preset,
                                                     'test-admin', True)

    # update system view-preset as admin
    updated_system_view_preset = ViewPresetCreate(
        title='updated-title',
        scope=ScopeEnum.SYSTEM,
        keywords='',
        componentType='map',
        initialProps={},
    )
    crud.update(old_system_view_preset, updated_system_view_preset,
                'test-admin', True)


def test_delete(session: Session, seeded_view_presets: None) -> None:
    """Tests delete"""

    view_preset = CRUD(session)

    # can delete user viewpreset
    view_preset.delete('user-1', 'test-user-1', False)

    # cannot delete the same preset again
    with pytest.raises(DeleteError):
        view_preset.delete('user-1', 'test-user-1', False)

    # cannot delete system preset
    with pytest.raises(DeleteError):
        view_preset.delete('system-2', 'test-user-1', False)

    # admin can delete system preset
    view_preset.delete('system-2', 'test-admin', True)


def test_system_clear(session: Session, seeded_view_presets: None) -> None:
    """Tests system clear"""

    view_preset = CRUD(session)

    # system preset is found before clear
    preset = view_preset.read_one('system-2')
    assert preset is not None

    # can delete system viewpresets
    view_preset.system_clear()

    # system preset is not found after clear
    deleted_preset = view_preset.read_one('system-2')
    assert deleted_preset is None
