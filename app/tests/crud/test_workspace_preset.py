# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Tests for workspace-preset CRUD actions"""
# pylint: disable=unused-argument

import pytest
from sqlmodel import Session

from app.crud.errors import CreateError, DeleteError, UpdateError
from app.crud.workspace_preset import CRUD
from app.models import (
    ScopeEnum,
    ViewDefinition,
    ViewTypeEnum,
    WorkspacePreset,
    WorkspacePresetCreate,
)


def test_read_many(session: Session, seeded_workspace_presets: None) -> None:
    """Tests read_many"""

    workspace_preset = CRUD(session)
    presets = workspace_preset.read_many()
    assert presets is not None
    assert len(presets) == 5

    presets = workspace_preset.read_many('')
    assert presets is not None
    assert len(presets) == 5

    presets = workspace_preset.read_many('test-user-1')
    assert presets is not None
    assert len(presets) == 6


def test_read_many_sorting(session: Session,
                           seeded_workspace_presets: None) -> None:
    """Test sorting of read_many"""

    ## retrieve system and shared presets
    workspace_preset = CRUD(session)
    presets = workspace_preset.read_many()

    ## check sorting of presets
    preset_titles = []
    assert presets is not None
    for preset in presets:
        preset_titles.append(preset.title)
    assert preset_titles == sorted(preset_titles)
    ## check shared preset is present
    shared_preset = [preset for preset in presets if preset.is_shared]
    assert len(shared_preset) == 1

    ## retrieve presets for user (including system and shared presets)
    presets = workspace_preset.read_many('test-user-1')

    ## check that first presets are system presets
    assert presets is not None
    for preset in presets[:-2]:
        assert preset.scope == 'system'
    ## check that the last presets are user presets
    assert presets[-1].scope == 'user'
    assert presets[-2].scope == 'user'
    ## check shared preset is present
    shared_preset = [preset for preset in presets if preset.is_shared]
    assert len(shared_preset) == 1


def test_read_one(session: Session, seeded_workspace_presets: None) -> None:
    """Tests read_one"""

    workspace_preset = CRUD(session)

    preset = workspace_preset.read_one('system-1')
    assert preset is not None
    assert preset.preset_id == 'system-1'

    preset = workspace_preset.read_one('user-1')
    assert preset is None


def test_create(session: Session, seeded_view_presets: None) -> None:
    """Tests create with a user preset and system preset"""

    data = {
        'title': 'my-workspace-preset',
        'scope': 'user',
        'views': [{
            'mosaicNodeId': 'A',
            'viewPresetId': 'user-1'
        }, {
            'mosaicNodeId': 'B',
            'viewPresetId': 'system-1'
        }],
        'keywords': '',
        'viewType': 'singleWindow',
        'mosaicNode': {
            'direction': 'row',
            'first': 'A',
            'second': 'B',
            'splitPercentage': 50
        },
        'isTimeScrollingEnabled': 'True',
        'linking': {
            'test': 'testA'
        }
    }

    workspace_preset = CRUD(session)

    # insert workspace-preset
    preset: WorkspacePreset = workspace_preset.create(
        WorkspacePresetCreate(**data), 'test-user-1', False)  # type: ignore
    assert preset is not None
    assert preset.preset_id is not None
    assert preset.username == 'test-user-1'
    # not set - so should be none
    assert not preset.is_shared

    # insert workspace-preset with same title and username
    with pytest.raises(
            CreateError,
            match='Preset title already in use, please choose a different title'
    ):
        workspace_preset.create(
            WorkspacePresetCreate(**data),  #type: ignore
            'test-user-1',
            False)


def test_create_incorrect_user(session: Session,
                               seeded_view_presets: None) -> None:
    """Tests create with a user preset and system preset"""

    data = {
        'title': 'my-workspace-preset',
        'scope': 'user',
        'views': [{
            'mosaicNodeId': 'A',
            'viewPresetId': 'user-1'
        }, {
            'mosaicNodeId': 'B',
            'viewPresetId': 'system-1'
        }],
        'keywords': '',
        'viewType': 'multiWindow',
        'mosaicNode': {
            'direction': 'row',
            'first': 'A',
            'second': 'B',
            'splitPercentage': 50
        }
    }

    workspace_preset = CRUD(session)

    # insert workspace-preset successfully
    workspace_preset.create(WorkspacePresetCreate(**data), 'test-user-2', False)


def test_create_nonexisting_preset(session: Session,
                                   seeded_view_presets: None) -> None:
    """Tests create with a user preset and system preset"""

    data = {
        'title': 'my-workspace-preset',
        'scope': 'user',
        'views': [{
            'mosaicNodeId': 'A',
            'viewPresetId': 'radar'
        }],
        'keywords': '',
        'viewType': 'tiledWindow',
        'mosaicNode': 'A',
    }

    workspace_preset = CRUD(session)

    # insert workspace-preset
    with pytest.raises(
            CreateError,
            match=
            'Not all view-presets in workspace-preset found, workspace-preset could not be created'
    ):
        workspace_preset.create(WorkspacePresetCreate(**data), 'test-user-1',
                                False)


def test_update(session: Session, workspace_presets: list[WorkspacePreset],
                seeded_view_presets) -> None:
    """Tests update"""

    data = WorkspacePresetCreate(
        title='my-workspace-preset',
        scope=ScopeEnum.USER,
        views=[ViewDefinition(mosaicNodeId='test', viewPresetId='system-1')],
        keywords='',
        viewType=ViewTypeEnum.TILED_WINDOW,
        mosaicNode='A',
        isTimeScrollingEnabled=True,
        linking={'test': 'testA'})
    workspace_preset = CRUD(session)

    # insert workspace-preset
    preset: WorkspacePreset = workspace_preset.create(data, 'test-user-1',
                                                      False)  #type: ignore

    # update with other username
    with pytest.raises(UpdateError,
                       match='Preset cannot be edited by current user'):
        workspace_preset.update(preset, data, 'test-user-2', False)

    # insert workspace-preset
    data2 = WorkspacePresetCreate(
        title='another-workspace-preset',
        scope=ScopeEnum.USER,
        views=[ViewDefinition(mosaicNodeId='A', viewPresetId='system-2')],
        keywords='',
        viewType=ViewTypeEnum.TILED_WINDOW,
        mosaicNode='A')

    second_preset: WorkspacePreset = workspace_preset.create(
        data2, 'test-user-1', False)

    # update the first preset with the title of the second preset, using the same username
    with pytest.raises(
            UpdateError,
            match=
            'Workspace-preset title already in use, please choose a different title'
    ):
        workspace_preset.update(preset, data2, 'test-user-1', False)

    # update a preset with a new title, using the same username
    new_preset: WorkspacePresetCreate = data2
    new_preset.title = 'a-completely-new-title'
    workspace_preset.update(second_preset, new_preset, 'test-user-1', False)

    # insert system workspace-preset
    system_workspace_preset = WorkspacePresetCreate(
        title='system-workspace-preset',
        scope=ScopeEnum.SYSTEM,
        views=[ViewDefinition(mosaicNodeId='A', viewPresetId='system-2')],
        keywords='',
        viewType=ViewTypeEnum.TILED_WINDOW,
        mosaicNode='A')
    old_system_workspace_preset: WorkspacePreset = workspace_preset.create(
        system_workspace_preset, 'test-admin', True)

    # update system workspace-preset as admin
    updated_system_workspace_preset = WorkspacePresetCreate(
        title='updated-title',
        scope=ScopeEnum.SYSTEM,
        views=[ViewDefinition(mosaicNodeId='A', viewPresetId='system-2')],
        keywords='',
        viewType=ViewTypeEnum.TILED_WINDOW,
        mosaicNode='A')
    workspace_preset.update(old_system_workspace_preset,
                            updated_system_workspace_preset, 'test-admin', True)


def test_delete(session: Session, seeded_workspace_presets: None) -> None:
    """Tests delete"""

    workspace_preset = CRUD(session)

    # can delete user workspacepreset
    workspace_preset.delete('user-1', 'test-user-1', False)

    # cannot delete the same preset again
    with pytest.raises(DeleteError):
        workspace_preset.delete('user-1', 'test-user-1', False)

    # cannot delete system preset
    with pytest.raises(DeleteError):
        workspace_preset.delete('system-2', 'test-user-1', False)

    # admin can delete system preset
    workspace_preset.delete('system-2', 'test-admin', True)


def test_system_clear(session: Session, seeded_workspace_presets: None) -> None:
    """Tests system clear"""

    workspace_preset = CRUD(session)

    # system preset is found before clear
    preset = workspace_preset.read_one('system-2', 'test-user-1')
    assert preset is not None

    # can delete system workspacepresets
    workspace_preset.system_clear()

    # system preset is not found after clear
    deleted_preset = workspace_preset.read_one('system-2', 'test-user-1')
    assert deleted_preset is None
