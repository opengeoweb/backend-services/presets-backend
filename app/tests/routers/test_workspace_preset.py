# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Module to test presets backend"""

# pylint: disable=unused-argument,too-many-lines
import json

from fastapi.encoders import jsonable_encoder
from fastapi.testclient import TestClient
from sqlalchemy import select
from sqlmodel import Session

from app.models import (
    ScopeEnum,
    ViewDefinition,
    ViewTypeEnum,
    WorkspacePreset,
    WorkspacePresetCreate,
)


def test_list_workspacepresets_without_username(
        client: TestClient, seeded_workspace_presets: None) -> None:
    """Tests listing workspace-presets, without providing a username"""

    response = client.get('/workspacepreset')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    shared_preset = [
        preset for preset in body if preset.get('is_shared') is True
    ]
    assert len(shared_preset) == 1


def test_list_workspacepresets_with_username(
        client: TestClient, seeded_workspace_presets: None) -> None:
    """Tests listing workspace-presets, with providing a username"""

    response = client.get('/workspacepreset/',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    shared_preset = [
        preset for preset in body if preset.get('is_shared') is True
    ]
    assert len(shared_preset) == 1
    user_preset = [preset for preset in body if preset.get('scope') == 'user']
    assert len(user_preset) == 2


def test_list_workspacepresets_with_username_and_scope(
        client: TestClient, seeded_workspace_presets: None) -> None:
    """Tests listing workspace-presets, with providing a username
       and using different values for scope"""

    # first make a call without a scope parameter set
    response = client.get('/workspacepreset/',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain user and system scopes
    scope_vals = [result['scope'] for result in body]
    assert 'user' in scope_vals
    assert 'system' in scope_vals

    # now set scope to user
    response = client.get('/workspacepreset?scope=user',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain user scope only
    scope_vals = [result['scope'] for result in body]
    assert 'user' in scope_vals
    assert 'system' not in scope_vals

    # now set scope to system
    response = client.get('/workspacepreset/?scope=system',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain system scope only
    scope_vals = [result['scope'] for result in body]
    assert 'user' not in scope_vals
    assert 'system' in scope_vals

    # also try mulitple scope values
    response = client.get('/workspacepreset?scope=system,user',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain user and system scope
    scope_vals = [result['scope'] for result in body]
    assert 'user' in scope_vals
    assert 'system' in scope_vals

    # verify that spaces in request are stripped
    response = client.get('/workspacepreset?scope=user, system',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain user and system
    scope_vals = [result['scope'] for result in body]
    assert 'user' in scope_vals
    assert 'system' in scope_vals


def test_list_workspacepresets_without_username_and_scope(
        client: TestClient, seeded_workspace_presets: None) -> None:
    """Tests listing workspace-presets, without providing a username
       and with setting a scope"""

    # check that only system and shared presets are returned with a default call
    response = client.get('/workspacepreset')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain system scope and a shared user preset
    scope_vals = [result['scope'] for result in body]
    assert 'user' in scope_vals
    assert 'system' in scope_vals
    shared_preset = [
        preset['is_shared'] for preset in body if preset['scope'] == 'user'
    ]
    assert all(shared_preset) is True
    test_length = len(body)

    # if scope is set to user, the list should return one shared preset
    response = client.get('/workspacepreset?scope=user')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 1
    assert body[0].get('is_shared') is True
    assert body[0].get('username') == 'test-user-3'

    # if scope is set to system, a non-empty list should be returned
    response = client.get('/workspacepreset/?scope=system')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain system scope only
    scope_vals = [result['scope'] for result in body]
    assert 'user' not in scope_vals
    assert 'system' in scope_vals
    # response should be of same length as the default call above - 1
    assert len(body) == test_length - 1

    # check that still no user presets are returned if multiple
    # scope values are queried
    response = client.get('/workspacepreset/?scope=system,user')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain system scope and shared presets
    scope_vals = [result['scope'] for result in body]
    assert 'user' in scope_vals
    assert 'system' in scope_vals
    # response should be of same length as the default call above
    assert len(body) == test_length


def test_list_workspacepresets_with_username_and_search(
        client: TestClient, seeded_workspace_presets: None) -> None:
    """Tests listing workspace-presets, with providing a username
       and using different words to search"""
    # pylint: disable=too-many-statements

    # first make a call without a search parameter set
    response = client.get('/workspacepreset/',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)

    # now try a single search term lowercase
    response = client.get('/workspacepreset?search=preset',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    results_1 = len(body)
    # view-presets in response should contain "preset" in title or abstract
    title_vals = ['preset' in result['title'].lower() for result in body]
    abstract_vals = [
        'preset' in result.get('abstract', None).lower()
        for result in body
        if result.get('abstract')
    ]
    assert all(title_vals) is True
    if abstract_vals:
        assert all(abstract_vals) is True

    # now try a single search term with uppercase,
    # result should be the same
    response = client.get('/workspacepreset?search=PRESET',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # view-presets in response should contain "preset" in title or abstract
    title_vals = ['preset' in result['title'].lower() for result in body]
    abstract_vals = [
        'preset' in result.get('abstract', None).lower()
        for result in body
        if result.get('abstract')
    ]
    assert all(title_vals) is True
    if abstract_vals:
        assert all(abstract_vals) is True
    assert len(body) == results_1

    # try multiple search terms and get an empty response
    response = client.get('/workspacepreset?search=preset,abstract',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 0

    # also try multiple search terms
    response = client.get('/workspacepreset?search=preset,system',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # workspace-presets in response should contain "preset" AND "system"
    # in title OR abstract
    check_vals = [(('preset' in result['title'].lower()) or
                   ('preset' in result.get('abstract', '').lower())) and
                  (('system' in result['title'].lower()) or
                   ('system' in result.get('abstract', '').lower()))
                  for result in body]
    assert all(check_vals) is True

    # also check search in abstract
    response = client.get('/workspacepreset?search=radar,abstract',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # workspace-presets in response should contain "radar" AND "abstract"
    # in title OR abstract
    check_vals = [(('radar' in result['title'].lower()) or
                   ('radar' in result.get('abstract', '').lower())) and
                  ('abstract' in result.get('abstract', '').lower())
                  for result in body]
    assert all(check_vals) is True

    # and a part of a word
    response = client.get('/workspacepreset?search=pre',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # view-presets in response should contain "pre"
    # in title or abstract
    check_vals = [(('pre' in result['title'].lower()) or
                   ('pre' in result.get('abstract', '').lower()))
                  for result in body]
    assert all(check_vals) is True


def test_list_workspacepresets_with_scope_and_search(
        client: TestClient, seeded_workspace_presets: None) -> None:
    """Tests listing workspace-presets, with providing a username,
      a value for scope and search"""

    # make a call with scope and search
    response = client.get('/workspacepreset?scope=user&search=preset',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)

    test_scope = ['user' in result['scope'] for result in body]
    assert all(test_scope) is True
    test_search = [('preset' in result['title'].lower()) or
                   (('preset') in result.get('abstract', '').lower())
                   for result in body]
    assert all(test_search) is True

    # make a call with multiple values for scope and search
    response = client.get(
        '/workspacepreset?scope=user,system&search=preset,system',
        headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)

    test_scope = [(('user' in result['scope']) or ('system' in result['scope']))
                  for result in body]
    assert all(test_scope) is True

    test_search = [(('preset' in result['title'].lower()) or
                    ('preset') in result.get('abstract', '').lower()) and
                   (('system' in result['title'].lower()) or
                    ('system' in result.get('abstract', '').lower()))
                   for result in body]
    assert all(test_search) is True


def test_list_workspacepresets_invalid_scope_value(
        client: TestClient, seeded_workspace_presets: None) -> None:
    """Tests listing workspace-presets, with providing a username
       and using different values for scope"""

    # verify that an "sys" (so not "system") returns an empty list
    response = client.get('/workspacepreset?scope=sys',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert body == []

    # verify that response is not empty if one scope is correct
    response = client.get('/workspacepreset?scope=use,system',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    scope_vals = [item['scope'] for item in body]
    assert 'system' in scope_vals
    assert not 'user' in scope_vals
    assert not 'use' in scope_vals


def test_list_workspacepresets_invalid_preset(client: TestClient,
                                              session: Session,
                                              seeded_workspace_presets: None,
                                              caplog) -> None:
    '''Test listing workspace-preset with an invalid entry
    in the database. Make sure a message is logged but
    that service does not crash'''

    ## verify database works
    response = client.get('/workspacepreset?scope=user,system',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    result_length = len(body)

    ## force add an invalid workspace-preset
    update_preset = {
        'id': 'emptyMap',
        'title': 'this is a preset',
        'keywords': 'preset, testing',
        'scope': 'system',
        'abstract': "test",
        'viewType': 'invalidViewType',
        'mosaicNode': 'test'
    }
    stmt = select(WorkspacePreset).where(
        WorkspacePreset.preset_id == 'emptyMap')
    preset: WorkspacePreset = session.scalar(stmt)

    # use the update function incorrectly to create an invalid preset
    assert preset is not None
    preset.update(update_preset, preset.preset_id)
    session.commit()

    ## again make list call
    response = client.get('/workspacepreset?scope=user,system',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    ## make sure that one item is not returned
    assert len(body) == result_length - 1

    ## capture warning
    captured = caplog.text
    assert 'Invalid workspace-preset retrieved from database' in captured


def test_get_workspacepreset_non_existing(client: TestClient) -> None:
    """Tests getting a workspace-preset by a non-existing ID"""
    response = client.get('/workspacepreset/non-existing-id',
                          headers={'Geoweb-Username': 'test-user-2'})
    assert response.status_code == 404
    assert {'message': 'Workspace-preset not found'} == response.json()


def test_get_workspacepreset_existing_without_username(
        client: TestClient, session: Session) -> None:
    """Tests getting a workspace-preset by ID, without providing a username"""

    # add a preset
    preset = WorkspacePresetCreate(
        id='this-is-a-preset-id',
        title='this is a preset',
        keywords='preset, testing',
        scope=ScopeEnum.SYSTEM,
        views=[ViewDefinition(mosaicNodeId='test', viewPresetId='system-1')],
        viewType=ViewTypeEnum.MULTI_WINDOW,
        mosaicNode='test')
    session.add(WorkspacePreset(preset))
    session.commit()

    response = client.get('/workspacepreset/this-is-a-preset-id')
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200
    found_preset = response.json()
    assert found_preset['id'] == 'this-is-a-preset-id'
    assert found_preset['title'] == 'this is a preset'
    assert found_preset['scope'] == 'system'


def test_get_workspacepreset_existing_with_username(client: TestClient,
                                                    session: Session) -> None:
    """Tests getting a workspace-preset by ID, with providing a username"""

    # add a preset
    preset = WorkspacePresetCreate(
        id='this-is-a-preset-id',
        title='this is a preset',
        keywords='preset, testing',
        scope=ScopeEnum.USER,
        views=[ViewDefinition(mosaicNodeId='test', viewPresetId='system-1')],
        viewType=ViewTypeEnum.SINGLE_WINDOW,
        mosaicNode='test')
    session.add(WorkspacePreset(preset, 'test-user-1'))
    session.commit()

    response = client.get('/workspacepreset/this-is-a-preset-id',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200
    found_preset = response.json()
    assert found_preset['id'] == 'this-is-a-preset-id'
    assert found_preset['title'] == 'this is a preset'
    assert found_preset['scope'] == 'user'


def test_get_workspacepreset_existing_with_invalid_username(
        client: TestClient, session: Session) -> None:
    """Tests getting a workspace-preset by ID, with providing an invalid username"""

    # add a preset
    preset = WorkspacePresetCreate(
        id='this-is-a-preset-id',
        title='this is a preset',
        keywords='preset, testing',
        scope=ScopeEnum.USER,
        views=[ViewDefinition(mosaicNodeId='test', viewPresetId='system-1')],
        viewType=ViewTypeEnum.MULTI_WINDOW,
        mosaicNode='test')

    session.add(WorkspacePreset(preset, 'test-user-1'))
    session.commit()

    response = client.get('/workspacepreset/this-is-a-preset-id',
                          headers={'Geoweb-Username': 'test-user-2'})
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 404
    assert {'message': 'Workspace-preset not found'} == response.json()


def test_get_workspacepreset_invalid_preset(client: TestClient,
                                            session: Session,
                                            seeded_workspace_presets: None,
                                            caplog) -> None:
    """Test get workspace-preset with an invalid entry
    in the database. Make sure that you get a response describing
    the error but that the service does not crash"""

    ## verify database works
    response = client.get('/workspacepreset/emptyMap',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200
    found_preset = response.json()
    assert found_preset['id'] == 'emptyMap'
    assert found_preset['title'] == 'Empty Map'
    assert found_preset['scope'] == 'system'

    ## force add an invalid workspace-preset
    update_preset = WorkspacePresetCreate(
        id='emptyMap',
        title='this is a preset',
        views=[ViewDefinition(mosaicNodeId='test', viewPresetId='system-1')],
        viewType=ViewTypeEnum.SINGLE_WINDOW,
        mosaicNode='test',
        keywords='preset, testing',
        scope=ScopeEnum.SYSTEM)

    stmt = select(WorkspacePreset).where(
        WorkspacePreset.preset_id == 'emptyMap')
    preset: WorkspacePreset = session.scalar(stmt)
    # use the update function incorrectly to create an invalid preset
    assert preset is not None
    preset.update(jsonable_encoder(WorkspacePreset(update_preset)),
                  preset.preset_id)
    session.commit()

    ## again make list call
    response = client.get('/workspacepreset/emptyMap',
                          headers={'Geoweb-Username': 'test-user-1'})
    ## check response
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 400
    assert response.json() == {
        'message': 'Invalid workspace-preset retrieved from database'
    }

    ## capture error log
    captured = caplog.text
    assert 'Invalid workspace-preset retrieved from database' in captured


def test_post_workspacepreset_without_slash(client: TestClient,
                                            seeded_view_presets,
                                            seeded_workspace_presets) -> None:
    """Tests posting a workspace-preset to URL without trailing slash"""

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        response = client.post('/workspacepreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers['Location'] is not None
    assert 'http://testserver/workspacepreset/' in response.headers['Location']


def test_post_workspacepreset_with_slash(client: TestClient,
                                         seeded_view_presets) -> None:
    """Tests posting a workspace-preset to URL with trailing slash"""

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        response = client.post('/workspacepreset/',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers['Location'] is not None
    assert 'http://testserver/workspacepreset/' in response.headers['Location']


def test_post_workspacepreset_same_user(client: TestClient, session: Session,
                                        seeded_view_presets) -> None:
    """Test the post workspacepreset route when title already exists for given user"""

    user = 'gwuser'
    preset = WorkspacePresetCreate(
        id='id_for_same_title',
        title='Radar and temperature test',
        keywords='post, test, user',
        scope=ScopeEnum.USER,
        views=[ViewDefinition(mosaicNodeId='A', viewPresetId='system-1')],
        viewType=ViewTypeEnum.TILED_WINDOW,
        mosaicNode='A')
    session.add(WorkspacePreset(preset, user))
    session.commit()

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        response = client.post('/workspacepreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': user})
    assert response.status_code == 400
    assert response.json() == {
        'message':
            'Preset title already in use, please choose a different title'
    }


def test_post_workspacepreset_conflicting_system_title(
        client: TestClient, session: Session, seeded_workspace_presets,
        seeded_view_presets) -> None:
    """Test the post workspacepreset route when title already exists for a system preset"""

    user = 'gwuser'

    with open('app/tests/testdata/workspacepreset_system_title.json',
              'rb') as fh:
        response = client.post('/workspacepreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': user})
    assert response.status_code == 400
    assert response.json() == {
        'message':
            'Preset title already in use, please choose a different title'
    }


def test_post_workspacepreset_invalid(client: TestClient) -> None:
    """Test the post workspacepreset route when no user is authenticated"""

    with open('app/tests/testdata/invalidinputpreset.json', 'rb') as fh:
        response = client.post('/workspacepreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400


def test_post_workspacepreset_invalid_scope(client: TestClient,
                                            seeded_view_presets) -> None:
    """
    Test the post workspacepreset route when a workspacepreset
    with invalid scope is posted
    """

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        data = json.load(fh)
        data['scope'] = 'invalid'
        response = client.post('/workspacepreset',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400

    ## check that posting succeeds when using a correct scope
    data['scope'] = 'user'
    response = client.post('/workspacepreset',
                           json=data,
                           headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200


def test_post_workspacepreset_nouser(client: TestClient, session: Session,
                                     seeded_view_presets,
                                     seeded_workspace_presets) -> None:
    """Test the post workspacepreset route when no user is authenticated"""

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        response = client.post('/workspacepreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': ''})
    assert response.status_code == 401


def test_post_workspacepreset_new(client: TestClient, seeded_workspace_presets,
                                  seeded_view_presets) -> None:
    """Test the post workspacepreset route with a new preset"""

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        response = client.post('/workspacepreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'testuser'})
    assert response.status_code == 200

    location = response.headers.get('Location')
    new_id = location.split('/')[-1]

    response = client.get(f'/workspacepreset/{new_id}',
                          headers={'Geoweb-Username': 'testuser'})
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200
    found_preset = response.json()
    assert found_preset['id'] == new_id
    assert found_preset['scope'] == 'user'


def test_post_workspacepreset_invalid_viewType(client: TestClient,
                                               seeded_workspace_presets,
                                               seeded_view_presets) -> None:
    """Test the post workspacepreset route with an invalid viewType"""

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        testdata = json.load(fh)
        testdata['viewType'] = 'invalid'
        response = client.post('/workspacepreset',
                               json=testdata,
                               headers={'Geoweb-Username': 'testuser'})
    assert response.status_code == 400


def test_put_updated_workspace_preset(client: TestClient,
                                      seeded_workspace_presets,
                                      seeded_view_presets) -> None:
    """Test the upating the posted workspacepreset route with a new preset"""

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        response = client.post('/workspacepreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'testuser'})
    assert response.status_code == 200
    new_id = response.headers.get('Location').split('/')[-1]

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        response = client.put(f'/workspacepreset/{new_id}',
                              json=json.load(fh),
                              headers={'Geoweb-Username': 'testuser'})
    assert response.status_code == 201

    response = client.get(f'/workspacepreset/{new_id}',
                          headers={'Geoweb-Username': 'testuser'})
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200
    found_preset = response.json()
    assert found_preset['id'] == new_id


def test_put_workspacepreset_without_username(client: TestClient) -> None:
    """Test response when updating preset without username"""

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        response = client.put('/workspacepreset/the-preset-id',
                              json=json.load(fh))
    assert response.status_code == 400


def test_put_workspacepreset_empty_username(client: TestClient) -> None:
    """Test response when updating preset without username"""

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        response = client.put('/workspacepreset/the-preset-id',
                              headers={'Geoweb-Username': ''},
                              json=json.load(fh))
    assert response.status_code == 401
    assert response.json() == {'message': 'Not authenticated'}


def test_put_workspacepreset_without_data(client: TestClient) -> None:
    """Test response when updating preset without data"""

    response = client.put('/workspacepreset/the-preset-id',
                          json={},
                          headers={'Geoweb-Username': 'geoweb-user'})
    assert response.status_code == 400
    assert response.json() == {'message': {}}


def test_put_workspacepreset_with_invalid_data(client: TestClient) -> None:
    """Test response when updating preset with invalid data"""

    response = client.put(
        '/workspacepreset/the-preset-id',
        data='{',  # type: ignore
        headers={'Geoweb-Username': 'geoweb-user'})
    assert response.status_code == 400
    assert response.json() == {'message': '{'}


def test_put_workspacepreset_nonexisting(client: TestClient,
                                         seeded_view_presets) -> None:
    """Test response when updating preset with non-existing workspace-preset"""

    with open('app/tests/testdata/post_workspacepreset.json', 'rb') as fh:
        response = client.put('/workspacepreset/the-preset-id',
                              json=json.load(fh),
                              headers={'Geoweb-Username': 'testuser'})

    assert response.status_code == 400
    expected_message = 'Workspace-preset could not be found for current user'
    assert response.json() == {'message': expected_message}


def test_put_workspacepreset_nonexisting_viewpresets(
        client: TestClient, seeded_workspace_presets,
        seeded_view_presets) -> None:
    """Test response when updating preset with non-existing view-presets"""

    # first post a workspacepreset
    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        response = client.post('/workspacepreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'testuser'})
    assert response.status_code == 200
    new_id = response.headers.get('Location').split('/')[-1]

    with open('app/tests/testdata/post_workspacepreset.json', 'rb') as fh:

        # change data to non-existing view-presets
        testdata = json.load(fh)
        testdata['views'][0]['viewPresetId'] = 'non-exist-1'
        testdata['views'][0]['viewPresetId'] = 'non-exist-2'

        response = client.put(f'/workspacepreset/{new_id}',
                              json=testdata,
                              headers={'Geoweb-Username': 'testuser'})
    assert response.status_code == 400

    expected_message = 'Not all view-presets in workspace-preset found'
    assert response.json() == {
        'message': f'Could not save workspace-preset: {expected_message}'
    }


def test_put_workspacepreset(client: TestClient, session: Session,
                             seeded_view_presets) -> None:
    """Test response when updating preset"""

    # create preset
    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        testdata = json.load(fh)
        test_id = testdata['id']
        preset = WorkspacePresetCreate(**testdata)
    session.add(WorkspacePreset(preset, 'geoweb-user'))
    session.commit()

    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        response = client.put(f'/workspacepreset/{test_id}',
                              json=json.load(fh),
                              headers={'Geoweb-Username': 'geoweb-user'})
    assert response.status_code == 201


def test_put_workspacepreset_different_user(client: TestClient,
                                            session: Session,
                                            seeded_view_presets) -> None:
    """Test response when updating preset with different user"""

    # create preset
    with open('app/tests/testdata/post_workspacepreset.json', 'rb') as fh:
        testdata = json.load(fh)
        test_id = testdata['id']
        preset = WorkspacePresetCreate(**testdata)
    session.add(WorkspacePreset(preset, 'geoweb-user'))
    session.commit()

    with open('app/tests/testdata/post_workspacepreset.json', 'rb') as fh:
        response = client.put(f'/workspacepreset/{test_id}',
                              json=json.load(fh),
                              headers={'Geoweb-Username': 'test-user'})
    assert response.status_code == 400

    expected_message = 'Workspace-preset could not be found for current user'
    assert response.json() == {'message': expected_message}


def test_delete_workspacepreset(
        client: TestClient, session: Session,
        workspace_presets: list[WorkspacePreset]) -> None:
    """
    Tests response status code when deleting a preset.
    """

    # add a preset
    session.add(workspace_presets[2])  # user-1
    session.commit()

    response = client.delete('/workspacepreset/user-1',
                             headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 204


def test_delete_system_workspacepreset(
        client: TestClient, session: Session,
        workspace_presets: list[WorkspacePreset]) -> None:
    """
    Tests response status code when deleting a preset with system scope.
    """

    # add a preset

    session.add(workspace_presets[0])
    session.commit()

    response = client.delete('/workspacepreset/system-1',
                             headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 403


def test_delete_workspacepreset_invalid_user(
        client: TestClient, session: Session,
        workspace_presets: list[WorkspacePreset]) -> None:
    """
    Tests response status code when deleting a preset owned by another user.
    """

    # add a preset

    session.add(workspace_presets[0])
    session.commit()

    response = client.delete('/workspacepreset/user-1',
                             headers={'Geoweb-Username': 'test-user-2'})
    assert response.status_code == 403


def test_delete_workspacepreset_invalid_id(
        client: TestClient, session: Session,
        workspace_presets: list[WorkspacePreset]) -> None:
    """
    Tests response status code when deleting a preset that does no exist.
    """

    response = client.delete('/workspacepreset/this-is-a-preset-id',
                             headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 403


def test_delete_workspacepreset_no_auth(
        client: TestClient, session: Session,
        workspace_presets: list[WorkspacePreset]) -> None:
    """
    Tests response status code when deleting a preset without being authenticated.
    """

    response = client.delete('/workspacepreset/this-is-a-preset-id')
    assert response.status_code == 400


def test_delete_workspacepreset_empty_username(
        client: TestClient, session: Session,
        workspace_presets: list[WorkspacePreset]) -> None:
    """
    Tests response status code when deleting a preset without being authenticated.
    """

    response = client.delete('/workspacepreset/this-is-a-preset-id',
                             headers={'Geoweb-Username': ''})
    assert response.status_code == 401


def test_delete_system_workspacepreset_as_admin(
        client: TestClient, session: Session,
        workspace_presets: list[WorkspacePreset]) -> None:
    """
    Tests that admin can delete a system workspacepreset
    """

    # add a preset
    session.add(workspace_presets[0])
    session.commit()

    # verify list length
    response = client.get('/workspacepreset',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert len(response.json()) == 1

    # delete system preset as admin
    response = client.delete('/workspacepreset/system-1',
                             headers={
                                 'Geoweb-Username': 'test-user-1',
                                 'Geoweb-Roles': 'ROLE_PRESET_ADMIN'
                             })
    assert response.status_code == 204

    # verify list length
    response = client.get('/workspacepreset',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert len(response.json()) == 0

    # verify you cannot retrieve the preset based on id
    response = client.get('/workspacepreset/system-1',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 404


def test_share_and_unshare_worksacepreset(client: TestClient,
                                          seeded_workspace_presets: None,
                                          seeded_view_presets: None) -> None:
    """
    Tests that setting is_shared to False makes preset no longer
    visible in list. Then set is_sharedto True and verify preset
    is again visible in list"""

    response = client.get('/workspacepreset')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    shared_preset = [preset for preset in body if preset['scope'] == 'user']
    assert len(shared_preset) == 1
    assert shared_preset[0]['is_shared'] is True

    # now test-user 3 updates preset with is_shared = False
    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        data = json.load(fh)
        data['is_shared'] = False
        response = client.put('/workspacepreset/user-3',
                              json=data,
                              headers={'Geoweb-Username': 'test-user-3'})
    assert response.status_code == 201

    # again make list request - verify no shared preset is present
    response = client.get('/workspacepreset')
    body = response.json()
    shared_preset = [preset for preset in body if preset['scope'] == 'user']
    assert len(shared_preset) == 0

    # again set is_shared to True and verify list response
    with open('app/tests/testdata/workspacepreset.json', 'rb') as fh:
        data = json.load(fh)
        data['is_shared'] = True
        response = client.put('/workspacepreset/user-3',
                              json=data,
                              headers={'Geoweb-Username': 'test-user-3'})
    assert response.status_code == 201

    response = client.get('/workspacepreset')
    body = response.json()
    shared_preset = [preset for preset in body if preset['scope'] == 'user']
    assert len(shared_preset) == 1
