# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Module to test presets backend backup system"""

# pylint: disable=unused-argument

import os

from fastapi.testclient import TestClient

from app.utils.constants import ALEMBIC_VERSION_SKIP, ROLE_PRESET_ADMIN

os.environ["BACKUP_PATH"] = "/tmp/"


def test_list_backups_without_username(client: TestClient,
                                       seeded_workspace_presets: None) -> None:
    """Tests listing workspace-presets, without providing a username"""

    response = client.get('/backup/list')
    assert response.status_code == 401


def test_list_backups_with_username(client: TestClient,
                                    seeded_workspace_presets: None) -> None:
    """Tests listing workspace-presets, with providing a username"""

    response = client.get('/backup/list',
                          headers={
                              'Geoweb-Username': 'test-user-1',
                              'Geoweb-Roles': ROLE_PRESET_ADMIN
                          })
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)


def test_list_backups_with_non_admin_user(
        client: TestClient, seeded_workspace_presets: None) -> None:
    """Tests listing workspace-presets, with providing a username"""

    response = client.get('/backup/list',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 403


def test_restore_backups_without_username(
        client: TestClient, seeded_workspace_presets: None) -> None:
    """Tests restoring workspace-presets, without providing a username"""

    response = client.get('/backup/restore?backup=test')
    assert response.status_code == 400


def test_restore_backups_with_non_admin_user(
        client: TestClient, seeded_workspace_presets: None) -> None:
    """Tests restoring workspace-presets, without providing a username"""

    response = client.get('/backup/restore?backup=test',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 403


def test_restore_backups_with_username(client: TestClient,
                                       seeded_workspace_presets: None) -> None:
    """Tests restoring workspace-presets, with providing a username"""

    with open("/tmp/testbackup.json", "w", encoding='utf-8') as file1:
        file1.write('{"alembic_version": [{"version_num": "%s"}]}' %
                    ALEMBIC_VERSION_SKIP)

    response = client.get('/backup/restore?backup=testbackup.json',
                          headers={
                              'Geoweb-Username': 'test-user-1',
                              'Geoweb-Roles': ROLE_PRESET_ADMIN
                          })
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert body == {"status": "ok"}


def test_make_backup_without_username(client: TestClient,
                                      seeded_workspace_presets: None) -> None:
    """Tests restoring workspace-presets, without providing a username"""

    response = client.get('/backup/make')
    assert response.status_code == 400


def test_make_backup_with_non_admin_user(
        client: TestClient, seeded_workspace_presets: None) -> None:
    """Tests restoring workspace-presets, without providing a username"""

    response = client.get('/backup/make',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 403


def test_make_backup_with_username(client: TestClient,
                                   seeded_workspace_presets: None) -> None:
    """Tests restoring workspace-presets, with providing a username"""

    with open("/tmp/testbackup.json", "w", encoding='utf-8') as file1:
        file1.write('{"alembic_version": [{"version_num": "%s"}]}' %
                    ALEMBIC_VERSION_SKIP)

    response = client.get('/backup/make',
                          headers={
                              'Geoweb-Username': 'test-user-1',
                              'Geoweb-Roles': ROLE_PRESET_ADMIN
                          })
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert body == {"status": "ok"}
