# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Module to test presets backend"""
import json

from fastapi.testclient import TestClient
from sqlalchemy import select
from sqlmodel import Session

from app.models import ScopeEnum, ViewPreset, ViewPresetCreate

# pylint: disable=unused-argument


def test_list_viewpresets_without_username(client: TestClient,
                                           seeded_view_presets: None) -> None:
    """Tests listing view-presets, without providing a username"""

    response = client.get('/viewpreset')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    shared_preset = [
        preset for preset in body if preset.get('is_shared') is True
    ]
    assert len(shared_preset) == 1


def test_list_viewpresets_with_username(client: TestClient,
                                        seeded_view_presets: None) -> None:
    """Tests listing view-presets, with providing a username"""

    response = client.get('/viewpreset/',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    shared_preset = [
        preset for preset in body if preset.get('is_shared') is True
    ]
    assert len(shared_preset) == 1
    user_preset = [preset for preset in body if preset.get('scope') == 'user']
    assert len(user_preset) == 2


def test_list_viewpresets_with_username_and_scope(
        client: TestClient, seeded_view_presets: None) -> None:
    """Tests listing view-presets, with providing a username
       and using different values for scope"""

    # first make a call without a scope parameter set
    response = client.get('/viewpreset/',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain user and system scopes
    scope_vals = [result['scope'] for result in body]
    assert 'user' in scope_vals
    assert 'system' in scope_vals

    # now set scope to user
    response = client.get('/viewpreset?scope=user',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain user scope only
    scope_vals = [result['scope'] for result in body]
    assert 'user' in scope_vals
    assert 'system' not in scope_vals

    # now set scope to system
    response = client.get('/viewpreset/?scope=system',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain system scope only
    scope_vals = [result['scope'] for result in body]
    assert 'user' not in scope_vals
    assert 'system' in scope_vals

    # also try mulitple scope values
    response = client.get('/viewpreset?scope=system,user',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain user and system scope
    scope_vals = [result['scope'] for result in body]
    assert 'user' in scope_vals
    assert 'system' in scope_vals


def test_list_viewpresets_without_username_and_scope(
        client: TestClient, seeded_view_presets: None) -> None:
    """Tests listing view-presets, without providing a username
       and with setting a scope"""

    # check that only system presets are returned with a default call
    response = client.get('/viewpreset')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain system and user (shared) scope
    scope_vals = [result['scope'] for result in body]
    assert 'user' in scope_vals
    assert 'system' in scope_vals
    shared_preset = [
        preset['is_shared'] for preset in body if preset['scope'] == 'user'
    ]
    assert all(shared_preset) is True
    test_length = len(body)

    # if scope is set to user, the list should return one shared preset
    response = client.get('/viewpreset?scope=user')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 1
    assert body[0].get('is_shared') is True
    assert body[0].get('username') == 'test-user-3'

    # if scope is set to system, a non-empty list should be returned
    response = client.get('/viewpreset/?scope=system')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain system scope only
    scope_vals = [result['scope'] for result in body[:-1]]
    assert 'user' not in scope_vals
    assert 'system' in scope_vals
    # response should be length as the default call above - 1
    assert len(body) == test_length - 1

    # check that still no user presets are returned if multiple
    # scope values are queried
    response = client.get('/viewpreset/?scope=system,user')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # response should contain system scope and one shared user
    scope_vals = [result['scope'] for result in body]
    assert 'user' in scope_vals
    assert 'system' in scope_vals
    # response should be of same length as the default call above
    assert len(body) == test_length


def test_list_viewpresets_invalid_scope_value(
        client: TestClient, seeded_view_presets: None) -> None:
    """Tests listing view-presets, with providing a username
       and using different values for scope"""

    # verify that an "sys" (so not "system") returns an empty list
    response = client.get('/viewpreset?scope=sys',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert body == []

    # verify that response is not empty if one scope is correct
    response = client.get('/viewpreset?scope=use,system',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    scope_vals = [item['scope'] for item in body]
    assert 'system' in scope_vals
    assert not 'user' in scope_vals
    assert not 'use' in scope_vals


def test_get_viewpreset_non_existing(client: TestClient) -> None:
    """Tests getting a view-preset by a non-existing ID"""
    response = client.get('/viewpreset/non-existing-id',
                          headers={'Geoweb-Username': 'test-user-2'})
    assert response.status_code == 404
    assert {'message': 'View-preset not found'} == response.json()


def test_get_viewpreset_existing_without_username(client: TestClient,
                                                  session: Session) -> None:
    """Tests getting a view-preset by ID, without providing a username"""

    # add a preset
    preset = ViewPresetCreate(id='this-is-a-preset-id',
                              title='this is a preset',
                              keywords='preset, testing',
                              scope=ScopeEnum.SYSTEM,
                              componentType='Map',
                              initialProps={'test': 'test'})
    session.add(ViewPreset(preset))
    session.commit()

    response = client.get('/viewpreset/this-is-a-preset-id')
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200
    found_preset = response.json()
    assert found_preset['id'] == 'this-is-a-preset-id'
    assert found_preset['title'] == 'this is a preset'
    assert found_preset['keywords'] == 'preset, testing'
    assert found_preset['scope'] == 'system'
    assert found_preset['initialProps']['test'] == 'test'


def test_get_viewpreset_existing_with_username(client: TestClient,
                                               session: Session) -> None:
    """Tests getting a view-preset by ID, with providing a username"""

    # add a preset
    preset = ViewPresetCreate(id='this-is-a-preset-id',
                              title='this is a preset',
                              keywords='preset, testing',
                              scope=ScopeEnum.USER,
                              componentType='Map',
                              initialProps={'test': 'test'})
    session.add(ViewPreset(preset, 'test-user-1'))
    session.commit()

    response = client.get('/viewpreset/this-is-a-preset-id',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200
    found_preset = response.json()
    assert found_preset['id'] == 'this-is-a-preset-id'
    assert found_preset['title'] == 'this is a preset'
    assert found_preset['keywords'] == 'preset, testing'
    assert found_preset['scope'] == 'user'
    assert found_preset['initialProps']['test'] == 'test'


def test_get_viewpreset_existing_with_invalid_username(
        client: TestClient, session: Session) -> None:
    """Tests getting a view-preset by ID, with providing an invalid username"""

    # add a preset
    preset = ViewPresetCreate(id='this-is-a-preset-id',
                              title='this is a preset',
                              keywords='preset, testing',
                              scope=ScopeEnum.USER,
                              componentType='Map',
                              initialProps={'test': 'test'})
    session.add(ViewPreset(preset, 'test-user-1'))
    session.commit()

    response = client.get('/viewpreset/this-is-a-preset-id',
                          headers={'Geoweb-Username': 'test-user-2'})
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200


def test_get_viewpreset_invalid_preset(client: TestClient,
                                       seeded_view_presets: None,
                                       session: Session, caplog) -> None:
    """Test get view-preset with an invalid entry
    in the database. Make sure that you get a response describing
    the error but that the service does not crash"""

    ## make sure that view-prest is valid
    response = client.get('/viewpreset/DWDWarnings',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    assert response.json()['id'] == 'DWDWarnings'

    ## force add an invalid view-preset
    update_presetjson = {
        'id': 'DWDWarnings',
        'title': 3.5,
        'scope': 'system',
        'keywords': 'WMS,Radar'
    }
    stmt = select(ViewPreset).where(ViewPreset.preset_id == 'DWDWarnings')
    viewpreset: ViewPreset = session.scalar(stmt)
    viewpreset.update(
        {
            "scope": update_presetjson['scope'],
            "presetjson": update_presetjson,
            "title": update_presetjson['title'],
            "keywords": update_presetjson['keywords']
        },
        'DWDWarnings',
    )
    session.commit()

    ## request same view-preset
    response = client.get('/viewpreset/DWDWarnings',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 400
    assert response.json() == {
        'message': 'Invalid view-preset retrieved from database'
    }

    ## capture error log
    captured = caplog.text
    assert 'Invalid view-preset retrieved from database' in captured


def test_get__user_viewpreset_unauthenticated(client: TestClient,
                                              session: Session) -> None:
    """Tests getting a user view-preset by ID, without providing a username"""

    # add a preset
    preset = ViewPresetCreate(id='this-is-a-preset-id',
                              title='this is a preset',
                              keywords='preset, testing',
                              scope=ScopeEnum.USER,
                              componentType='Map',
                              initialProps={'test': 'test'})
    session.add(ViewPreset(preset, 'test-user-1'))
    session.commit()

    # make request for user view-preset without username header
    response = client.get('/viewpreset/this-is-a-preset-id')
    # verify response is OK
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200
    found_preset = response.json()
    assert found_preset['id'] == 'this-is-a-preset-id'
    assert found_preset['title'] == 'this is a preset'
    assert found_preset['keywords'] == 'preset, testing'
    assert found_preset['scope'] == 'user'
    assert found_preset['initialProps']['test'] == 'test'


def test_list_viewpresets_with_username_and_search(
        client: TestClient, seeded_view_presets: None) -> None:
    """Tests listing viewpresets, with providing a username
       and using different words to search"""

    # first make a call without a search parameter set
    response = client.get('/viewpreset/',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)

    # now try a single search term lowercase
    response = client.get('/viewpreset?search=harmonie',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # view-presets in response should contain "Harmonie" in title or abstract
    check_vals = [(('harmonie' in result['title'].lower()) or
                   ('harmonie' in result.get('abstract', None).lower()))
                  for result in body]
    assert all(check_vals) is True
    # now try a single search term with uppercase,
    # result should be the same
    response = client.get('/viewpreset?search=HARMONIE',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # view-presets in response should contain "harmonie" in title or abstract
    check_vals = [(('harmonie' in result['title'].lower()) or
                   ('harmonie' in result.get('abstract', '').lower()))
                  for result in body]
    assert all(check_vals) is True

    # try multiple search terms and get an empty response
    response = client.get('/viewpreset?search=harmonie,asdf',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    assert len(body) == 0

    # also try multiple search terms
    response = client.get('/viewpreset?search=harmonie,and',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # workspace-presets in response should contain "preset" AND "system"
    # in title OR abstract
    check_vals = [(('harmonie' in result['title'].lower()) or
                   ('harmonie' in result.get('abstract', '').lower())) and
                  (('and' in result['title'].lower()) or
                   ('and' in result.get('abstract', '').lower()))
                  for result in body]
    assert all(check_vals) is True

    # and a part of a word
    response = client.get('/viewpreset?search=harm',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    assert isinstance(body, list)
    # view-presets in response should contain "pre"
    # in title or abstract
    check_vals = [(('harm' in result['title'].lower()) or
                   ('harm' in result.get('abstract', '').lower()))
                  for result in body]
    assert all(check_vals) is True


def test_post_viewpreset_without_slash(client: TestClient) -> None:
    """Tests posting a view-preset to URL without trailing slash"""

    with open('app/tests/testdata/inputpreset.json', 'rb') as fh:
        response = client.post('/viewpreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers['Location'] is not None
    assert 'http://testserver/viewpreset/' in response.headers['Location']


def test_post_viewpreset_with_slash(client: TestClient) -> None:
    """Tests posting a view-preset to URL with trailing slash"""

    with open('app/tests/testdata/inputpreset.json', 'rb') as fh:
        response = client.post('/viewpreset/',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200
    assert response.headers['Location'] is not None
    assert 'http://testserver/viewpreset/' in response.headers['Location']


def test_post_viewpreset_same_user(client: TestClient,
                                   session: Session) -> None:
    """Test the post viewpreset route when title already exists for given user"""

    user = 'gwuser'
    preset = ViewPresetCreate(id='RenewalWarningsOriginal',
                              title='Renewal warnings convection',
                              keywords='post, test, global',
                              componentType='Map',
                              scope=ScopeEnum.USER)
    session.add(ViewPreset(preset, user))
    session.commit()

    with open('app/tests/testdata/existingpreset.json', 'rb') as fh:
        response = client.post('/viewpreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': user})
    assert response.status_code == 400
    assert response.json() == {
        'message':
            'Preset title already in use, please choose a different title'
    }


def test_post_viewpreset_conflicting_system_title(client: TestClient,
                                                  session: Session) -> None:
    """Test the post viewpreset route when title already exists for a system preset"""

    user = 'gwuser'
    preset = ViewPresetCreate(id='RenewalWarningsOriginal',
                              title='Renewal warnings convection',
                              keywords='post, test, global',
                              componentType='Map',
                              scope=ScopeEnum.SYSTEM)
    session.add(ViewPreset(preset))
    session.commit()

    with open('app/tests/testdata/existingpreset.json', 'rb') as fh:
        response = client.post('/viewpreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': user})
    assert response.status_code == 400
    assert response.json() == {
        'message':
            'Preset title already in use, please choose a different title'
    }


def test_post_viewpreset_invalid(client: TestClient) -> None:
    """Test the post viewpreset route when no user is authenticated"""

    with open('app/tests/testdata/invalidinputpreset.json', 'rb') as fh:
        response = client.post('/viewpreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400


def test_post_viewpreset_invalid_scope(client: TestClient) -> None:
    """Test the post viewpreset route with an invalid scope"""

    with open('app/tests/testdata/inputpreset.json', 'rb') as fh:
        data = json.load(fh)
        data['scope'] = 'invalid'
        response = client.post('/viewpreset',
                               json=data,
                               headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 400

    ## check that POST is possible with valid scope
    data['scope'] = 'user'
    response = client.post('/viewpreset',
                           json=data,
                           headers={'Geoweb-Username': 'gwuser'})
    assert response.status_code == 200


def test_post_viewpreset_nouser(client: TestClient) -> None:
    """Test the post viewpreset route when no user is authenticated"""

    with open('app/tests/testdata/inputpreset.json', 'rb') as fh:
        response = client.post('/viewpreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': ''})
    assert response.status_code == 401


def test_post_viewpreset_new(client: TestClient) -> None:
    """Test the post viewpreset route with a new preset"""

    with open('app/tests/testdata/newpreset.json', 'rb') as fh:
        response = client.post('/viewpreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'testuser'})
    assert response.status_code == 200

    location = response.headers.get('Location')
    new_id = location.split('/')[-1]

    response = client.get(f'/viewpreset/{new_id}',
                          headers={'Geoweb-Username': 'testuser'})
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200
    found_preset = response.json()
    assert found_preset['id'] == new_id


def test_put_updated_view_preset(client: TestClient) -> None:
    """Test the upating the posted viewpreset route with a new preset"""

    with open('app/tests/testdata/newpreset.json', 'rb') as fh:
        response = client.post('/viewpreset',
                               json=json.load(fh),
                               headers={'Geoweb-Username': 'testuser'})
    assert response.status_code == 200
    new_id = response.headers.get('Location').split('/')[-1]

    with open('app/tests/testdata/updatednewpreset.json', 'rb') as fh:
        response = client.put(f'/viewpreset/{new_id}',
                              json=json.load(fh),
                              headers={'Geoweb-Username': 'testuser'})
    assert response.status_code == 201

    response = client.get(f'/viewpreset/{new_id}',
                          headers={'Geoweb-Username': 'testuser'})
    assert response.headers.get('content-type') == 'application/json'
    assert response.status_code == 200
    found_preset = response.json()
    assert found_preset['id'] == new_id


def test_put_viewpreset_without_username(client: TestClient) -> None:
    """Test response when updating preset without username"""

    with open('app/tests/testdata/inputpreset.json', 'rb') as fh:
        response = client.put('/viewpreset/the-preset-id', json=json.load(fh))
    assert response.status_code == 400


def test_put_viewpreset_empty_username(client: TestClient) -> None:
    """Test response when updating preset without username"""

    with open('app/tests/testdata/inputpreset.json', 'rb') as fh:
        response = client.put('/viewpreset/the-preset-id',
                              headers={'Geoweb-Username': ''},
                              json=json.load(fh))
    assert response.status_code == 401


def test_put_viewpreset_without_data(client: TestClient) -> None:
    """Test response when updating preset without username"""

    response = client.put('/viewpreset/the-preset-id',
                          json={},
                          headers={'Geoweb-Username': 'geoweb-user'})
    assert response.status_code == 400


def test_put_viewpreset_with_invalid_data(client: TestClient) -> None:
    """Test response when updating preset without username"""

    response = client.put(
        '/viewpreset/the-preset-id',
        data='{',  # type: ignore
        headers={'Geoweb-Username': 'geoweb-user'})
    assert response.status_code == 400


def test_put_viewpreset_nonexisting(client: TestClient) -> None:
    """Test response when updating preset without username"""

    response = client.put('/viewpreset/the-preset-id',
                          json={},
                          headers={'Geoweb-Username': 'geoweb-user'})
    assert response.status_code == 400


def test_put_viewpreset(client: TestClient, session: Session) -> None:
    """Test response when updating preset"""

    # create preset
    preset = ViewPresetCreate(id='the-preset-id',
                              title='Awsome preset!',
                              keywords='preset',
                              componentType='Map',
                              scope=ScopeEnum.USER)
    session.add(ViewPreset(preset, 'geoweb-user'))
    session.commit()

    response = client.put('/viewpreset/the-preset-id',
                          json={
                              'componentType': 'Map',
                              'id': 'Empty',
                              'keywords': 'WMS',
                              'scope': 'system',
                              'title': 'Empty map',
                              'initialProps': {}
                          },
                          headers={'Geoweb-Username': 'geoweb-user'})
    assert response.status_code == 201


def test_delete_viewpreset(client: TestClient, session: Session) -> None:
    """
    Tests response status code when deleting a preset.
    """

    # add a preset
    preset = ViewPresetCreate(id='this-is-a-preset-id',
                              title='this is a preset',
                              keywords='preset, testing',
                              componentType='Map',
                              scope=ScopeEnum.USER)
    session.add(ViewPreset(preset, 'test-user-1'))
    session.commit()

    response = client.delete('/viewpreset/this-is-a-preset-id',
                             headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 204


def test_delete_system_viewpreset(client: TestClient, session: Session) -> None:
    """
    Tests response status code when deleting a preset with system scope.
    """

    # add a preset
    preset = ViewPresetCreate(id='this-is-a-preset-id',
                              title='this is a preset',
                              keywords='preset, testing',
                              componentType='Map',
                              scope=ScopeEnum.SYSTEM)
    session.add(ViewPreset(preset))
    session.commit()

    response = client.delete('/viewpreset/this-is-a-preset-id',
                             headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 403


def test_delete_viewpreset_invalid_user(client: TestClient,
                                        session: Session) -> None:
    """
    Tests response status code when deleting a preset owned by another user.
    """

    # add a preset
    preset = ViewPresetCreate(id='this-is-a-preset-id',
                              title='this is a preset',
                              keywords='preset, testing',
                              componentType='Map',
                              scope=ScopeEnum.USER)

    session.add(ViewPreset(preset, 'test-user-1'))
    session.commit()

    response = client.delete('/viewpreset/this-is-a-preset-id',
                             headers={'Geoweb-Username': 'test-user-2'})
    assert response.status_code == 403


def test_delete_viewpreset_invalid_id(client: TestClient,
                                      session: Session) -> None:
    """
    Tests response status code when deleting a preset that does no exist.
    """

    response = client.delete('/viewpreset/this-is-a-preset-id',
                             headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 403


def test_delete_viewpreset_no_auth(client: TestClient,
                                   session: Session) -> None:
    """
    Tests response status code when deleting a preset without being authenticated.
    """

    response = client.delete('/viewpreset/this-is-a-preset-id')
    assert response.status_code == 400


def test_delete_viewpreset_empty_username(client: TestClient,
                                          session: Session) -> None:
    """
    Tests response status code when deleting a preset without being authenticated.
    """

    response = client.delete('/viewpreset/this-is-a-preset-id',
                             headers={'Geoweb-Username': ''})
    assert response.status_code == 401


def test_delete_system_viewpreset_as_admin(client: TestClient,
                                           session: Session) -> None:
    """
    Tests that an admin can delete a system viewpreset
    """

    # add a preset
    preset = ViewPresetCreate(id='this-is-a-preset-id',
                              title='this is a preset',
                              keywords='preset, testing',
                              componentType='Map',
                              scope=ScopeEnum.SYSTEM)
    session.add(ViewPreset(preset))
    session.commit()

    # check list length is equal to 1
    response = client.get('/viewpreset',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert len(response.json()) == 1

    # delete system preset as admin
    response = client.delete('/viewpreset/this-is-a-preset-id',
                             headers={
                                 'Geoweb-Username': 'test-user-1',
                                 'Geoweb-Roles': 'ROLE_PRESET_ADMIN'
                             })
    assert response.status_code == 204

    # check list lenght is equal to 0
    response = client.get('/viewpreset',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert len(response.json()) == 0

    # verify you cannot retrieve the preset based on id
    response = client.get('/viewpreset/this-is-a-preset-id',
                          headers={'Geoweb-Username': 'test-user-1'})
    assert response.status_code == 404


def test_share_and_unshare_viewpreset(client: TestClient,
                                      seeded_view_presets: None) -> None:
    """
    Tests that setting is_shared to False makes preset no longer
    visible in list. Then set is_sharedto True and verify preset
    is again visible in list"""

    response = client.get('/viewpreset')
    assert response.status_code == 200
    assert response.headers.get('content-type') == 'application/json'
    body = response.json()
    shared_preset = [preset for preset in body if preset['scope'] == 'user']
    assert len(shared_preset) == 1
    assert shared_preset[0]['is_shared'] is True

    # now test-user 3 updates preset with is_shared = False
    response = client.put('/viewpreset/user-3',
                          json={
                              'componentType': 'Map',
                              'id': 'user-3',
                              'keywords': 'WMS',
                              'scope': 'user',
                              'title': 'Empty map',
                              'initialProps': {},
                              'is_shared': False
                          },
                          headers={'Geoweb-Username': 'test-user-3'})
    assert response.status_code == 201

    # again make list request - verify no shared preset is present
    response = client.get('/viewpreset')
    body = response.json()
    shared_preset = [preset for preset in body if preset['scope'] == 'user']
    assert len(shared_preset) == 0

    # again set is_shared to True and verify list response
    response = client.put('/viewpreset/user-3',
                          json={
                              'componentType': 'Map',
                              'id': 'user-3',
                              'keywords': 'WMS',
                              'scope': 'user',
                              'title': 'Empty map',
                              'initialProps': {},
                              'is_shared': True
                          },
                          headers={'Geoweb-Username': 'test-user-3'})
    assert response.status_code == 201

    response = client.get('/viewpreset')
    body = response.json()
    shared_preset = [preset for preset in body if preset['scope'] == 'user']
    assert len(shared_preset) == 1
