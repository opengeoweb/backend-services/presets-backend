# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
'Models mapping to the database'
import pytest

from app.models import (
    ScopeEnum,
    ViewDefinition,
    ViewPreset,
    ViewPresetCreate,
    ViewTypeEnum,
    WorkspacePreset,
    WorkspacePresetCreate,
)

# pylint: disable=redefined-outer-name


@pytest.fixture
def viewpreset() -> ViewPreset:
    """Returns a ViewPreset fixture."""
    return ViewPreset(
        ViewPresetCreate(id='unique-id',
                         scope=ScopeEnum.USER,
                         title='My Title',
                         keywords='word1, word2',
                         componentType='Map',
                         is_shared=False))


def test_init_viewpreset(viewpreset: ViewPreset) -> None:
    """Tests that derived attributes are set."""
    assert viewpreset.preset_id == 'unique-id'
    assert viewpreset.scope == 'user'
    assert viewpreset.title == 'My Title'
    assert viewpreset.keywords == 'word1, word2'
    assert viewpreset.is_shared is False


def test_update_viewpreset(viewpreset: ViewPreset) -> None:
    """Tests that derived attributes are updated."""
    data = ViewPresetCreate(id='another-unique-id',
                            scope=ScopeEnum.SYSTEM,
                            title='My Better Title',
                            keywords='word2, word3',
                            componentType='Map',
                            is_shared=True)
    viewpreset.update(data, viewpreset.preset_id)

    # not updated
    assert viewpreset.preset_id == 'unique-id'
    assert viewpreset.scope == 'user'
    assert viewpreset.presetjson['id'] == 'unique-id'

    # updated
    assert viewpreset.presetjson == dict(data, **{'id': 'unique-id'})
    assert viewpreset.title == 'My Better Title'
    assert viewpreset.keywords == 'word2, word3'
    assert viewpreset.is_shared is True


@pytest.fixture
def workspacepreset() -> WorkspacePreset:
    """Returns a WorkspacePreset fixture."""
    return WorkspacePreset(
        WorkspacePresetCreate(
            id='unique-id',
            scope=ScopeEnum.USER,
            title='My Title',
            keywords='word1, word2',
            abstract='test abstract',
            views=[ViewDefinition(mosaicNodeId='test', viewPresetId='radar')],
            viewType=ViewTypeEnum.SINGLE_WINDOW,
            mosaicNode='test',
            isTimeScrollingEnabled=True,
            linking={'test': 'test'},
            is_shared=True))


def test_init_workspacepreset(workspacepreset: WorkspacePreset) -> None:
    """Tests that derived attributes are set."""
    assert workspacepreset.preset_id == 'unique-id'
    assert workspacepreset.scope == 'user'
    assert workspacepreset.title == 'My Title'
    assert workspacepreset.keywords == 'word1, word2'
    assert workspacepreset.abstract == 'test abstract'
    assert workspacepreset.presetjson['isTimeScrollingEnabled'] is True
    assert workspacepreset.presetjson['linking'] == {'test': 'test'}
    assert workspacepreset.is_shared is True


def test_update_workspacepreset(workspacepreset: WorkspacePreset) -> None:
    """Tests that derived attributes are updated."""
    data = WorkspacePresetCreate(
        id='another-unique-id',
        scope=ScopeEnum.SYSTEM,
        title='My Better Title',
        keywords='word2, word3',
        abstract='update abstract',
        views=[ViewDefinition(mosaicNodeId='test', viewPresetId='radar')],
        viewType=ViewTypeEnum.SINGLE_WINDOW,
        mosaicNode='test',
        isTimeScrollingEnabled=False,
        linking={'test': 'testA'},
        is_shared=False)
    workspacepreset.update(data, workspacepreset.preset_id)

    # not updated
    assert workspacepreset.preset_id == 'unique-id'
    assert workspacepreset.scope == 'user'
    assert workspacepreset.presetjson['id'] == 'unique-id'

    # updated
    assert workspacepreset.presetjson == dict(data, **{'id': 'unique-id'})
    assert workspacepreset.title == 'My Better Title'
    assert workspacepreset.keywords == 'word2, word3'
    assert workspacepreset.abstract == 'update abstract'
    assert workspacepreset.presetjson['isTimeScrollingEnabled'] is False
    assert workspacepreset.presetjson['linking'] == {'test': 'testA'}
    assert workspacepreset.is_shared is False
