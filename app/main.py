# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""Main module, defines the application"""

import os

from fastapi import FastAPI, Request
from fastapi.exceptions import HTTPException, RequestValidationError
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import JSONResponse

from app.config import settings
from app.logger import configure_logger
from app.routers import backup, main, view_preset, workspace_preset
from app.scheduler import init_apscheduler

configure_logger()

application_root_path = os.getenv("APPLICATION_ROOT_PATH", "")

app = FastAPI(docs_url=settings.application_doc_root,
              root_path=settings.application_root_path,
              lifespan=init_apscheduler)
app.include_router(main.router)
app.include_router(view_preset.router)
app.include_router(workspace_preset.router)
app.include_router(backup.router)
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=['OPTIONS', 'GET', 'POST', 'PUT', 'PATCH', 'DELETE'],
    expose_headers=['Authorization', 'Content-Type', 'Location'])


@app.middleware('http')
async def add_headers(request: Request, call_next):
    """Adds security headers"""
    response = await call_next(request)
    response.headers['X-Content-Type-Options'] = 'nosniff'
    response.headers[
        'Strict-Transport-Security'] = 'max-age=31536000; includeSubDomains'
    return response


@app.exception_handler(HTTPException)
async def http_exception_handler(_: Request, exc: HTTPException):
    """Converts a HTTPException to a HTTP response"""

    return JSONResponse({'message': exc.detail},
                        status_code=exc.status_code,
                        headers=exc.headers)


@app.exception_handler(RequestValidationError)
async def request_validation_error_handler(_: Request,
                                           exc: RequestValidationError):
    """Converts a HTTPException to a HTTP response"""

    return JSONResponse({'message': exc.body}, status_code=400)
