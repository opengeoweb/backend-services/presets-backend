# Copyright 2024 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""This module provide utils for the app"""

import logging
from typing import Optional

from fastapi import Depends, HTTPException, Response
from sqlmodel import Session

from app.crud.view_preset import CRUD
from app.db import get_session
from app.utils.constants import ROLE_PRESET_ADMIN


def view_presets_exists(
    view_presets: list[str], session: Session = Depends(get_session)) -> bool:
    """Function to check if view_preset exists"""

    logging.info('Check if all mappresets exist')
    for view_preset in view_presets:
        preset = CRUD(session).read_one(view_preset)
        logging.info(preset)

        if not preset:
            logging.info('Mappreset %s not found', view_preset)
            return False

    logging.info('All presets found')
    return True


def _check_authenticated(response: Response,
                         username: str | None = None) -> None:
    """Checks if user is authenticated"""
    if not username:
        raise HTTPException(status_code=401, detail='Not authenticated')
    response.headers['username'] = username


# pylint: disable=consider-alternative-union-syntax
def _check_is_admin(roles: Optional[str]) -> bool:
    """Check if user is admin"""

    if roles is None:
        logging.info('Roles is None. User is not admin.')
        is_admin = False
    else:
        is_admin = ROLE_PRESET_ADMIN in roles.split(', ')
        logging.info('Check if user is admin: %s', is_admin)
    return is_admin
