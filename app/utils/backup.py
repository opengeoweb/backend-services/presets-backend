# Copyright 2025 Koninklijk Nederlands Meteorologisch Instituut (KNMI)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
""" This module provides utils for backups """

import json
import logging
import os
import re
from datetime import datetime, timezone

from sqlalchemy.orm import Session

from ..db import dump_sqlalchemy, restore_sqlalchemy

SECONDS_IN_TWO_WEEKS = 60 * 60 * 24 * 7 * 2


def get_backup_path() -> str | None:
    """ Returns the path where backups are stored, or None when not set """
    if (backup_path := os.getenv('BACKUP_PATH')) is None:
        logging.info(
            "Backups disabled because BACKUP_PATH environment is not set")
        return None
    return backup_path


def clean_files_in_folder_older_than_n_seconds(path: str, seconds: int):
    """Deletes files older then N seconds"""
    list_of_files = os.listdir(path)
    current_time = datetime.now(timezone.utc).timestamp()
    for i in list_of_files:
        file_location = os.path.join(path, i)
        if not os.path.isfile(file_location):
            continue  # skip when it no longer exists
        try:
            if os.stat(file_location).st_mtime < current_time - seconds:
                logging.info("Delete %s", i)
                os.remove(file_location)
        except FileNotFoundError:
            logging.debug("Not found: %s", file_location)


def get_backup_ids() -> list[str]:
    """ Restores a backup for given id and optionally username"""
    if (backup_path := get_backup_path()) is None:
        return []
    return sorted(
        [file for file in os.listdir(backup_path) if file.endswith(".json")])


def valid_file_match(strg, search=re.compile(r'[^a-zA-Z0-9._]').search):
    """Checks if only valid tokens are used for the backup file to restore"""
    return strg.endswith(".json") and not bool(search(strg))


def restore_backup(session: Session, backup_id: str, username=None) -> bool:
    """ Restores a backup for given id and optionally username"""
    if (backup_path := get_backup_path()) is None:
        logging.info("restore_backup: BACKUP_PATH is not set")
        return False
    if not valid_file_match(backup_id):
        logging.info("restore_backup: backup_id is not valid")
        return False
    file = os.path.join(backup_path, backup_id)
    logging.info("Restoring file %s", file)
    with open(file, 'r', encoding='utf-8') as openfile:
        json_object = json.load(openfile)
        return restore_sqlalchemy(session, json_object, username)


def make_backup(session: Session | None = None):
    """ Function is ran every hour with apscheduler """

    if (backup_path := get_backup_path()) is None:
        return False
    clean_files_in_folder_older_than_n_seconds(backup_path,
                                               SECONDS_IN_TWO_WEEKS)
    now = datetime.now(timezone.utc).strftime("%Y_%m_%dT%H_%M_%S")

    filename = f"{backup_path}/backuppresetsdb_{now}.json"

    logging.info("Create a backup of presets database into file %s", filename)
    dump = dump_sqlalchemy(session)
    with open(filename, "w", encoding='utf-8') as outfile:
        outfile.write(dump)
    logging.info("*** Database contents written to %s", filename)
    return True
