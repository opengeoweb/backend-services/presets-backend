# GeoWeb presets backend

This repository contains code to set up, develop and deploy the presets-backend. The presets backend provides view- and map-presets for the GeoWeb front-end.

View-presets are configurations of a single map-view. View-presets allow you to configure settings like layers, overlays, bounding boxes, projections and more.

Workspace-presets are configurations of one or more view-presets. Workspace-presets configure the layout of view-presets, syncing of multiple view-presets and more.

In this backend, some predefined presets are provided in the [staticpresets directory](./staticpresets/) - but users can also create their own custom view- and workspace-presets.

The presets backend consists of the following components:

```mermaid
flowchart TD

id1{{Frontend application}}<-->|Requests from and reponses to FE|id2[NGINX]
id2:::beclass<-->|Validated response to FE \n\n Authenticated and authorized request \n to the main Fastapi BE container.|id3[Presets FastAPI BE]
id3:::beclass<-->|Retrieve and update presets data|id4[(Presets Database)]:::beclass

style id1 stroke-width:3px

classDef beclass fill:#bbe3a4,stroke:#597549,stroke-width:3px
```

## Table of Contents

[[_TOC_]]

## Coding standards

### Formatting

This project follows the [Google Python Style Guide](https://github.com/google/styleguide/blob/gh-pages/pyguide.md). The [YAPF](https://github.com/google/yapf) tool can be used to enforce this. If you installed all dependencies using `pip install -r requirements.txt -r requirements-dev.txt`, you should be able to run YAPF by executing `yapf -ir app/`. This will format all Python files in [app](app), using the configuration specified in [.style.yapf](.style.yapf).

### Organizing imports

This project uses [isort](https://pycqa.github.io/isort/) for organizing import statements. This is a tool to sort imports alphabetically, and automatically separates them into sections and by type. To sort imports for the project run `isort app/`.

### IDE support

If you are using Visual Studio Code, consider updating [.vscode/settings.json](.vscode/settings.json) with the contents below. This will format code and organize imports on save.

```
{
    "editor.codeActionsOnSave": {
        "source.organizeImports": true
    },
    "python.formatting.provider": "yapf",
    "[python]": {
        "editor.formatOnSave": true
    }
}
```

## Presets

This repository contains code to set up the preset-backend. In the [Container Registry](https://gitlab.com/opengeoweb/backend-services/presets-backend/container_registry) you can find two containers that can be set up locally or in a cloud environment.

### View-presets and workspace-presets

After deploying the presets-backend, you will be able to access two types of presets: view-presets and workspace-presets.

View-presets contain data for a single view, that can contain multiple data layers or a single module.

Workspace-presets consist of one or more view-presets. **Important:** This means that workspace-presets are dependent on view-presets: if a workspace-preset refers to a non-existing view-preset, the workspace-preset is invalid and will not be saved in the `workspacepresets` table. On the other hand: a view-preset can exist without belonging to a workspace-preset.

### Ingesting data into the database

While deploying the presets-backend, a database with two tables is created: one table for `viewpresets` and one table for `workspacepresets`.

If a workspace-preset is ingested in the `workspacepresets` table, the following check is done:

    do all view-presets in the workspace-preset exist in the viewpresets table?

If not, the workspace-preset will not be saved in the `workspacepresets` table.

Also note that it is possible to _only_  use the view-presets functionality and leave the `workspacepresets` table empty. However, if you would like to use workspace-presets, it is required to also create the view-presets that are referenced in the workspace-presets.

**Tip:** To successfully create new `workspacepresets`, first the `viewpresets` table with the desired view-presets should be updated, followed by updating the `workspacepresets` table.

### Presets configuration

Institute specific view- and workspace-presets can be loaded into the database with the following steps:

1. Create two files in the directory [staticpresets](staticpresets) named with the following structure:
    - For view-presets: `{var}.view-presets.json`
    - For workspace-presets: `{var}.workspace-presets.json`

    Both files should contain a list of JSONs with view-presets and workspace-presets. For an example, you could have a look at testdata for [view-presets](app/tests/testdata/view-presets.json) and [workspace-presets](app/tests/testdata/workspace-presets.json).
2. Now you can create the presets-backend as desired. During the building and deployment, an environment variable `DEPLOY_ENVIRONMNET` should be set, equal to `var` in the filenames from the first step. Something like:
```
export DEPLOY_ENVIRONMENT="var"
```
If you want to run the backend with containers, you should set this environment variable for the `admin` container. For example, see [docker-compose.yml](docker-compose.yml).

Have a look at the next section on how to run the presets-backend locally with configured data.

**Important:** if no `DEPLOY_ENVIRONMENT` variable has been set, it will default to "open" and load workspacepresets from [opengeoweb](https://opengeoweb.com/).

## Local running for testing and development
The presets-backend can be run locally in a number of ways for different purposes:
1. For developing the Python code - [Development: Local install with Uvicorn](#development-local-install-with-uvicorn)
2. For testing with Docker Compose - [Running with Docker Compose](#running-with-docker-compose)
3. For testing only with Docker - [Running with Docker and without Docker Compose and SSL](#running-with-docker-and-without-docker-compose-and-ssl)
3. For testing with the frontend - [Testing BE in local FE branch with GitLab or Cognito](#testing-be-in-local-fe-branch-with-gitlab-or-cognito)

### Development: Local install with Uvicorn

The application can be started after a local installation with Uvicorn, using the `--reload` flag.
This is useful for development as the preset-backend service is automatically refreshed when code changes are detected.
In order to make it work a Postgres database is needed on the local installation.
Below are the instructions to start the code with Uvicorn and a local Postgres database. _All commands should be run from the root folder._

#### 1. Install Pyenv with Pyenv installer

Check the [prerequisites](https://github.com/pyenv/pyenv/wiki#suggested-build-environment) from the pyenv wiki. for Debian Linux they are:

```bash
sudo apt update; sudo apt install build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev
```

Run the Pyenv installer script:

```bash
curl https://pyenv.run | bash
```

Setup your shell environment with these [instructions](https://github.com/pyenv/pyenv#set-up-your-shell-environment-for-pyenv)

#### 2. Build and install Python

With Pyenv installed, check the available Python versions with

```bash
pyenv install -l
```

Install the latest Python 3.11 version from the list and set it as global Python version for your system.

```bash
pyenv install 3.11.8
pyenv global 3.11.8
```

#### 3. Install Pipx

Pipx is a helper utility for installing system wide Python packages, adding them to your path automatically.

Install pipx with these [instructions](https://pipx.pypa.io/latest/installation/). Linux users can install it with pip:

```bash
python3 -m pip install --user pipx
python3 -m pipx ensurepath

# Optional
sudo pipx ensurepath --global
```

#### 4. Install Poetry

Poetry is used for managing project dependencies and metadata. Install Poetry with

```bash
pipx install poetry
```

and add poetry export plugin to it

```bash
pipx inject poetry poetry-plugin-export
```

#### 5. Activate virtualenv and install dependencies

Activate the virtualenv with poetry

```bash
poetry shell
```

And install project dependencies

```bash
poetry install
```

#### 6. Setup Postgres database

```
sudo apt-get -y install postgresql postgresql-server-dev-all postgresql-client

### Setup postgres database for user geoweb and password geoweb, databasename presetsbackenddb ###
sudo -u postgres createuser --superuser geoweb
sudo -u postgres psql postgres -c "ALTER USER geoweb PASSWORD 'geoweb';"
sudo -u postgres psql postgres -c "CREATE DATABASE presetsbackenddb;"
echo "\q" | psql "dbname=presetsbackenddb user=geoweb password=geoweb host=localhost"
if [ ${?} -eq 0 ];then
    echo "Postgres database setup correctly"
    echo "Use the following setting for the presets backennd:"
    echo "export PRESETS_BACKEND_DB=\"dbname=presetsbackenddb user=geoweb password=geoweb host=localhost\""
else
    echo "Postgres database does not work, please check"
fi
```

Or, alternatively, run a Dockerized Postgres database (while running Uvicorn locally):

```
docker run --rm \
    --name presets-backend-db \
    -e POSTGRES_USER=geoweb \
    -e POSTGRES_PASSWORD=geoweb \
    -e POSTGRES_DB=presetsbackenddb \
    -p 5432:5432 \
    postgres:13.4
```

#### 7. Start the application using:

Optional: `export DEPLOY_ENVIRONMENT="..."`

```
poetry shell
export PRESETS_BACKEND_DB="postgresql://geoweb:geoweb@localhost:5432/presetsbackenddb"
export VERSION=1.0.0
export DEPLOY_ENVIRONMENT="open"
bin/admin.sh
bin/start-dev.sh
```

#### 8. Visit http://127.0.0.1:8080/ or http://127.0.0.1:8080/api

Two endpoints should now be available: http://127.0.0.1:8080/viewpreset and http://127.0.0.1:8080/workspacepreset

#### 9. You can browse the database using

```
psql "${PRESETS_BACKEND_DB}"
select * from viewpresets ;
```

If you want to reset/clean the database, you can drop the `viewpresets` table using:

```
psql "${PRESETS_BACKEND_DB}" -c 'DROP TABLE IF EXISTS viewpresets;'
```

Similarly, you can drop the `workspacepresets` table:
```
psql "${PRESETS_BACKEND_DB}" -c 'DROP TABLE IF EXISTS workspacepresets;'
```


And cleanup the Alembic table as well:
```
psql "${PRESETS_BACKEND_DB}" -c 'DROP TABLE IF EXISTS alembic_version;'
```

When the application is restarted, it will be initialized with fresh data.

### Managing Python dependencies

With Poetry installed from previous step, you can add or update Python dependencies with poetry add command.

```bash
# adding dependency
poetry add fastapi

# updating dependency
poetry update fastapi
```

You can add development dependencies with --group keyword.

```bash
# adding development dependency
poetry add --group dev isort
```

All poetry operations respect version constraints set in pyproject.toml file.

##### Fully updating dependencies

You can see a list of outdated dependencies by running

```bash
poetry show --outdated
```

To update all dependencies, still respecting the version constraints on pyproject.toml, run

```bash
poetry update
```

### Dependabot integration

This repository has a Dependabot integration with the following behavior specified in the `.gitlab/dependabot.yml` file:

| definition               | behavior                                                                                                                                                                                                                                                                   |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| schedule block           | Scan the repository daily at random time between 6-11. Add any pending package update as MR and update any MR with merge conflicts by doing a rebase. If there is a newer release of the dependency that already has an open MR, make a new MR and close the previous one. |
| open-pull-requests-limit | Don't open more than 3 MRs per directory, even if there are more package updates available than 3.                                                                                                                                                                         |
| rebase-strategy          | Always rebase open MRs, if there are new merged changes on the main branch, even if there are no merge conflicts present.                                                                                                                                                  |

More detailed guide on how to work with dependabot can be found in the [opengeoweb wiki](https://gitlab.com/groups/opengeoweb/-/wikis/How-to-work-with-Dependabot-MRs)

### Using pre-commit

You can also use the pre-commit configuration. This is used to inspect the code that's about to be committed, to see if you've forgotten something, to make sure tests run, or to examine whatever you need to inspect in the code. To use pre-commit make sure the `pip install` command has succesfully ran with the `requirements-dev.txt` argument.
Then execute the following command in the terminal in the root folder:
 ```
 pre-commit install
 ```
 This will install the pre-commit functions and now pre-commit will run automatically on `git commit`.
 Only changed files are taken into consideration.

 Optionally you can check all the files with the pre-commit checks:

```
pre-commit run --all-files
```

If you wish to skip the pre-commit test but do have pre-commit setup, you can use the `--no-verify` flag in git to skip the pre-commit checks. For example:
```
git commit -m "Message" --no-verify
```

## Running with Docker Compose
The presets-backend can be started in a Docker container.
This will create a static version of the application which is not automatically refreshed after saving changes in the code.

The [docker-compose.yml](docker-compose.yml) file defines the Nginx, Postgres, presets-backend and presets-backend-admin containers. It wires everthing together using a network. The nginx acts as a reverse proxy and is the gatekeeper in front of the application. The postgres database stores the presets. The presets-backend-admin runs the database migrations and the presets-backend is the python application which makes the presets backend. Some settings need to be configured, such as which port to run the application and which identity provider to use.

During startup, the static json files from the [/staticpresets](staticpresets) directory are ingested into the database. See [Presets configuration](#presets-configuration) for details.

### Starting up
Before starting to build the container, make sure that docker is installed and running on your machine.
To start the application with Docker Compose, set up the environment, build the Docker image and start the infrastructure.
_Important: All commands should be run from the root folder!_

The [auth-backend](https://gitlab.com/opengeoweb/backend-services/auth-backend), presets-backend and frontend can be configured together to authenticate the user and authorize rights and actions the user can execute. The presets-backend and [auth-backend](https://gitlab.com/opengeoweb/backend-services/auth-backend) are configured together by setting the environment variables in a `.env` file. For the front-end, keys need to be set in the `config.json` file. More information about roles in open environments can be found on [this](https://gitlab.com/groups/opengeoweb/-/wikis/Using-roles-in-Open-environments) Wiki page.

#### 1. Authentication

The auth-backend can be configured as an authentication proxy. By using the keys below in an `.env` file, a user can login into GeoWeb, access system presets and create/edit/delete their own personal presets.

1. If GitLab.com is used as an identity provider, use the following command to create the `.env` file:

```
cat <<EOF > .env
ENABLE_SSL=TRUE
ENABLE_DEBUG_LOG=TRUE
BACKEND_PORT=4443
BACKEND_PORT_HTTP=80
OAUTH2_USERINFO=https://gitlab.com/oauth/userinfo
GEOWEB_USERNAME_CLAIM="email"
# GEOWEB_REQUIRE_READ_PERMISSION="groups=opengeoweb/internal"
# GEOWEB_REQUIRE_WRITE_PERMISSION="groups=opengeoweb/internal"
VERSION=1.0.0
DEPLOY_ENVIRONMENT="open"
LOG_LEVEL="INFO"
EOF
```

The following keys should be used in the `config.json` for the FE:
```
  "GW_APP_URL": "http://localhost:5400",
  "GW_AUTH_CLIENT_ID": "{client_id}",
  "GW_AUTH_TOKEN_URL": "https://gitlab.com/oauth/token",
  "GW_AUTH_LOGIN_URL": "https://gitlab.com/oauth/authorize?client_id={client_id}&response_type=code&scope=email+openid&redirect_uri={app_url}/code&state={state}&code_challenge={code_challenge}&code_challenge_method=S256",
  "GW_AUTH_LOGOUT_URL": "http://localhost:5400",
```

2. In case KNMI's Cognito is used as an identity provider:
```
cat <<EOF > .env
ENABLE_SSL=TRUE
ENABLE_DEBUG_LOG=TRUE
BACKEND_PORT=4443
BACKEND_PORT_HTTP=80
OAUTH2_USERINFO={Cognito Userinfo endpoint}
GEOWEB_USERNAME_CLAIM='username'
VERSION=1.0.0
DEPLOY_ENVIRONMENT="open"
LOG_LEVEL="INFO"
EOF
```

The following keys should be used in the `config.json` for the FE:
```
  "GW_APP_URL": "http://localhost:5400",
  "GW_AUTH_CLIENT_ID": "{client_id}",
  "GW_AUTH_TOKEN_URL": "{Token URL}",
  "GW_AUTH_LOGIN_URL": "{Login URL}",
  "GW_AUTH_LOGOUT_URL": "{Logout URL}",
```

#### 2. Authentication & authorization

The auth-backend can be configured as authentication and authorization proxy. In the presets-backend, certain users can be authorized as "administrators" and have the rights to create/edit/delete _system_ presets (in addition to creating/editing/deleting personal presets).

More information about roles in open environments can be found on [this](https://gitlab.com/groups/opengeoweb/-/wikis/Using-roles-in-Open-environments) Wiki page.

1. If Gitlab is used:
```
cat <<EOF > .env
ENABLE_SSL=TRUE
ENABLE_DEBUG_LOG=TRUE
BACKEND_PORT=4443
BACKEND_PORT_HTTP=80
OAUTH2_USERINFO=https://gitlab.com/oauth/userinfo
GEOWEB_USERNAME_CLAIM="email"
# GEOWEB_REQUIRE_READ_PERMISSION="groups=opengeoweb/internal"
# GEOWEB_REQUIRE_WRITE_PERMISSION="groups=opengeoweb/internal"
GEOWEB_ROLE_CLAIM_NAME="groups"
GEOWEB_ROLE_CLAIM_VALUE_PRESETS_ADMIN="opengeoweb/internal/presets-admins"
VERSION=1.0.0
DEPLOY_ENVIRONMENT="open"
LOG_LEVEL="INFO"
EOF
```

The following keys should be used in the `config.json` for the FE:
```
  "GW_APP_URL": "http://localhost:5400",
  "GW_AUTH_CLIENT_ID": "{client_id}",
  "GW_AUTH_TOKEN_URL": "https://gitlab.com/oauth/token",
  "GW_AUTH_LOGIN_URL": "https://gitlab.com/oauth/authorize?client_id={client_id}&response_type=code&scope=email+openid&redirect_uri={app_url}/code&state={state}&code_challenge={code_challenge}&code_challenge_method=S256",
  "GW_AUTH_LOGOUT_URL": "http://localhost:5400",
  "GW_AUTH_ROLE_CLAIM_NAME": "groups_direct",
  "GW_AUTH_ROLE_CLAIM_VALUE_PRESETS_ADMIN": "opengeoweb/internal/presets-admins",
```

2. In case KNMI's Cognito is used:

See the KNMI Presets deployment repo.

2.  After correctly setting `.env`, the container can be build with Docker Compose.


If you work with a `Docker Compose` version 1.x (check `docker-compose -v` or `docker-compose version`), use:

```
docker-compose up -d --build
```


If you have `Compose V2` installed (check `docker compose -v` or `docker compose version`), you should be able to run:
```
docker compose up -d --build
```
according to the [docs](https://docs.docker.com/compose/).


3. After succesfully composing the container, logging can be viewed via:

`docker logs -f presets-backend`

Note: Logging level can be adjusted by using the `LOG_LEVEL` environment variable. See the [logging](#logging) section for more details.

The following endpoints should now be available:

- `curl -kL https://0.0.0.0:4443/`
- `curl -kL https://0.0.0.0:4443/healthcheck` (for the BE itself)
- `curl -kL https://0.0.0.0:4443/health_check` (for NGINX)
- `curl -kL https://0.0.0.0:4443/viewpreset | python -m json.tool`
- `curl -kL https://0.0.0.0:4443/viewpreset/preset1 | python -m json.tool`
- `curl -kL https://0.0.0.0:4443/workspacepreset | python -m json.tool`
- `curl -kL https://0.0.0.0:4443/workspacepreset/preset1 | python -m json.tool`

For Mac users, replace `https://0.0.0.0:4443/` with `https://localhost:4443`:

- `curl -kL https:/localhost:4443/`
- `curl -kL https://localhost:4443/healthcheck` (for the BE itself)
- `curl -kL https://localhost:4443/health_check` (for NGINX)
- `curl -kL https://localhost:4443/viewpreset | python -m json.tool`
- `curl -kL https://localhost:4443/viewpreset/preset1 | python -m json.tool`
- `curl -kL https://localhost:4443/workspacepreset | python -m json.tool`
- `curl -kL https://localhost:4443/workspacepreset/preset1 | python -m json.tool`

### Clean-up

To reset everything, including the database do:

```
docker-compose down --volumes
```
or
```
docker compose down --volumes
```
depending on your `Docker Compose` version.

## Running with Docker and without Docker Compose and SSL

Start the Postgres container at port 5432:

```
docker run -it \
    --name presets-backend-db \
    -e POSTGRES_USER=geoweb \
    -e POSTGRES_PASSWORD=geoweb \
    -e POSTGRES_DB=presetsbackenddb \
    --network host \
    postgres:13.4
```

Build the preset-backend container:

```
docker build -t presets-backend .
```


Start the preset-backend container at port 8085:

```
export PRESETS_BACKEND_DB="postgresql://geoweb:geoweb@localhost:5432/presetsbackenddb"

docker run -it \
    --name presets-backend \
    -e "PRESETS_PORT_HTTP=8085" \
    -e "PRESETS_BACKEND_DB=${PRESETS_BACKEND_DB}" \
    --network host \
    presets-backend
```

Populate the database once the preset-backend is running:

```
docker exec -it \
    presets-backend \
    bash bin/admin.sh
```

Start the Nginx container at port 8081:

```
docker run -it \
    --name presets-backend-nginx \
    -e "ENABLE_SSL=FALSE" \
    -e "ENABLE_DEBUG_LOG=TRUE" \
    -e "NGINX_PORT_HTTP=8081" \
    -e "BACKEND_HOST=0.0.0.0:8085" \
    -e OAUTH2_USERINFO="http://0.0.0.0" \
    -e "ALLOW_ANONYMOUS_ACCESS=TRUE" \
    -v nginxcertvol:/cert \
    -v "$(pwd)"/nginx/nginx.conf:/nginx.conf \
    --network host \
    registry.gitlab.com/opengeoweb/backend-services/auth-backend/auth-backend:v0.1.0
```

Test it:

`curl -kL http://0.0.0.0:8081/health_check` (for NGINX)
`curl -kL http://0.0.0.0:8081/healthcheck` (for the preset-backend itself)

## Testing BE in local FE branch with GitLab or Cognito
To test backend changes in a local [opengeoweb frontend](https://gitlab.com/opengeoweb/opengeoweb) branch, the presets-backend can be setup in a Docker and connected to a local frontend branch with the following steps:

* Make sure that you are _not_ connected to a VPN
* Checkout the branch you want to test locally
* Check that docker is installed and running
* Depending on your wishes, you can configure the the frontend and Docker to authenticate, or authenticate _and_ authorize users. See [this section](#starting-up) for more information.
* In the web browser that you will use to test the app:
  * Go to address: https://localhost:4443/ or https://0.0.0.0:4443/
  * Accept the certificate (this is not possible on connect). Keep this browser open.
* Copy `"GW_PRESET_BACKEND_URL": "https://localhost:4443/"` or  `"GW_PRESET_BACKEND_URL": "https://0.0.0.0:4443/"` into your local config.json in the FE
* Connect to your VPN
* Run your frontend app using the same identity provider set in the presets-backend
* Open the app in the same browser as where you accepted the certificate

## Logging

For this backend, the environment variable `LOG_LEVEL` can be configured to adjust the verbosity of logging.

It is possible to set it to any standard Python logging level: `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`, or `NOTSET`. `INFO` is the default value.

## Authentication mechanism

The authentication is handled in the Nginx reverse proxy. Nginx checks if an authorization header is set and if it contains a valid OAuth2 bearer token. If authorization header is set, Nginx will communicate with the identity provider to get the user information using the provided access token. If an user can be identified, nginx will set a header named `Geoweb-Username` with the identity and proceed with forwarding the request to the presets-backend container. The presets-backend will read this header, it uses this to know who signed in. If the OAuth2 bearer token is not given in the authorization header and `ALLOW_ANONYMOUS_ACCESS` is enabled, Nginx will continue forwarding the request to the presets-backend container without the user identity information. The authentication settings for the API endpoints are defined in the [nginx.conf](./nginx/nginx.conf) file. For authentication details, see the [NGINX reverse proxy documentation](https://gitlab.com/opengeoweb/backend-services/auth-backend).

## Release and deploy a new version of the presets-backend

A new version of the presets-backend can be created and deployed with the Gitlab UI following the steps below:

1. First decide how to bump the version (major, minor or patch) and prepare the release notes:
  - Navigate to [Code -> Tags](https://gitlab.com/opengeoweb/backend-services/presets-backend/-/tags) to retrieve the name of the latest tag
  - Go to [Code -> Compare revisions](https://gitlab.com/opengeoweb/backend-services/presets-backend/-/compare?from=main&to=main) and compare the main branch with the latest tag
  - Based on the code changes, decide how to bump the version using [semantic versioning](https://semver.org/)

    ---
    - _Major: contains breaking changes_
    - _Minor: contains backwards compatible functional changes_
    - _Patch: contains backwards compatible small fixes and/or updates_
    ---

  - Keep this page open as you can base the release notes on the commit messages on this page
2. Then create a new tag:
  - Open another tab and go to [Code -> Tags](https://gitlab.com/opengeoweb/backend-services/presets-backend/-/tags) and click `"New tag"`
  - Fill in the tag name. Make sure to add `"v"` in front so the tag is formatted as: `"vX.Y.Z"`
  - _Optional:_ add the release notes in the tag message. This can be a list of the commit messages retrieved from the [Compare revisions page](https://gitlab.com/opengeoweb/backend-services/presets-backend/-/compare?from=main&to=main) of the previous step
  - Click `"Create tag"`
  - You'll be redirected to the overview page of the tag you just created.
3. Finally, create a new release:
  - On the overview page of the tag, click the `"Create release"` button, to create a release based on this tag
  - Leave the "Release title"-field empty to use the tag name as release title
  - On the new page, you **should** add the release notes. They can be based on the list of commit messages retrieved from the [Compare revisions page](https://gitlab.com/opengeoweb/backend-services/presets-backend/-/compare?from=main&to=main) of the first step
  - Edit the commit messages a bit so you end up with a list of human-readable changes
4. After following these steps, the following actions are automatically triggered:
  - When you have created a new tag from the main branch, a pipeline will start automatically. This pipeline builds and publishes a Docker image to the [GitLab Container Registry](https://gitlab.com/opengeoweb/backend-services/presets-backend/container_registry/). This container will have the same name as the tag name.
  - After publishing the docker image, another pipeline starts that updates the ACC deployment at https://acc.opengeoweb.com/presets/. You can check the current Open ACC version here: https://acc.opengeoweb.com/presets/version.

_Bonus tip:_ the creation of the tag and the release can also be done in one single step (combining step 2 + 3 above):
- Navigate to [Deploy -> Releases](https://gitlab.com/opengeoweb/backend-services/presets-backend/-/releases) and click the button `"New release"`.
- You can create a new tag by clicking on the dropdown menu below "Tag name" and typing a new tag name
- Leave the "Release title"-field empty to use the tag name as release title
- Add a list of human-readable changes to create the release notes (see step 1)
- Finally press `"Create release"` to create the new tag and release

_Note:_ after merging changes into the "main" branch, the current main version is automatically deployed at https://dev.opengeoweb.com/presets/.

## API documentation

The API provides a number of endpoints. A [healthcheck](#get-healthcheck) and [version](#get-version) provide basic information about the status of the backend. Further, the `/viewpreset` and `/workspacepreset` endpoints provide access to view-preset and workspace-preset functionalities.

### Calls

#### GET /healthcheck

Returns status code 200 with `{'status': 'OK', 'service': 'Presets'}` JSON.

Used by infrastructure to check backend is up.

#### GET /health_check

Returns status code 200 with `{'status': 'OK', 'service': 'NGINX'}` JSON.

Used by infrastructure to check NGINX is up.

#### GET /version

Returns status code 200 with “version” string.

Note: To make this call work locally, a `VERSION` env variable is required.

#### GET /viewpreset

Returns a list of view-presets (summary of each preset). If a user is not authenticated, only system view-presets will be returned. If a user is signed in, a list consisting of system view-presets and personal view-presets created by the authenticated user will be returned.

Example summary of a preset:


`date: datestring`

`id: string`

`title: string`

`scope: “user” | “system”`

`abstract: string (optional, only if exists)`

Response example:

```json
[
  {
    "date": "2022-06-01T12:34:27.787184",
    "id": "Radar",
    "title": "Radar",
    "scope": "user"
  },
  {
    "date": "2022-06-01T12:34:27.787192",
    "id": "Temperature",
    "title": "Temperature",
    "abstract": "Temperature at sea level",
    "scope": "system"
  }
]
```
#### Querying

You can refine the returned list with one parameter:

- `search`: use this parameter to search for specific words in the `title` or `abstract` of a viewpreset. Examples:
    - `/viewpreset/?search=radar` will return system presets (and user presets if user is authenticated) where "radar" occurs in the `title` or `abstract`
    - `/viewpreset?search=radar,satellite` will return system presets (and user presets if user is authenticated) where "radar" or "satellite" occurs in the `title` or `abstract`

#### Querying

You can refine the returned list with two query parameters:

- `scope`: use this parameter to filter the returned results based on the `scope` value. Examples:
    - `/viewpreset/?scope=user` will return only user presets for a given user
    - `/viewpreset?scope=system` will return only system presets
    - `/viewpreset?scope=user,system` will return presets with scope `user` or `system`. By providing a comma-separated list, you can query multiple scope values. Note that only user presets created by the authenticated user will be returned.

### GET /viewpreset/`preset_id`

Returns the details of a view-preset:

`id: string`

`keywords: comma-separated string list`

`title: string`

`abstract: string`

`componentType: “Map” | “MultiMap” | ...`

`initialProps: “Object”`

Different component types will have a different format for the initialProps object.

Returns:

- 200 status and the contents of the requested view-preset (see example below)
- 400 status if an error occurred while reading the database
- 404 status if the requested view-preset id was not found

```json
{
  "id": "harmEcWms",
  "keywords": "WMS,harmEcWms",
  "title": "HARM vs EC (via WMS)",
  "abstract": "Harmonie vs EC..."
  "componentType": "Map",
  "initialProps": { … }
}
```


### POST /viewpreset/

Saves a view-preset and assigns it a new id (backend-generated).

Accepts a view-preset (same format as returned by GET /viewpreset/`preset_id`).

```json
{
  "title": "New title ",
  "componentType": "Map",
  "initialProps": { … }
}
```

Returns:

- 200 status and an empty body (+ new id as the last part of a location header).
- 400 status
  - If another preset for the same user, or a system preset has the same title.
  - If the JSON is invalid (format has to be validated by the BE).
- 401 status
  - If no user is authenticated.

### PUT /viewpreset/`preset_id`/

Saves a view-preset for a given id (updated by backend).

Accepts a view-preset (same format as returned by GET /viewpreset/`preset_id`).

```json
{
  "title": "Preset title ",
  "componentType": "Map",
  "initialProps": { … }
}
```

Returns:

- 201 status and an empty body if update successful.
- 400 status
  - If another preset for the same user, or a system preset has the same title.
  - If the JSON is invalid (format has to be validated by the BE).
- 401 status
  - If no user is authenticated.

### DELETE /viewpreset/`preset_id`

Deletes the view-preset owned by the currently authenticated user and identified by `preset_id`. No request body should be sent.

Returns:

- 204 HTTP status code if successfully deleted
- 401 HTTP status code if no user was authenticated
- 403 HTTP status code if the preset is not owned by the authenticated user, or the preset does not exist

### GET /workspacepreset

Returns a list of workspace-presets (summary of each preset). If a user is not authenticated, only system workspace-presets will be returned. If a user is signed in, a list consisting of system workspace-presets and personal workspace-presets created by the authenticated user will be returned.

Example summary of a preset:

`date: datestring`

`id: string`

`title: string`

`scope: “user” | “system”`

`viewType: "singleWindow"| "multiWindow" | "tiledWindow"`

`abstract: string (optional, only if exists)`

#### Querying

You can refine the returned list with two query parameters:

- `scope`: use this parameter to filter the returned results based on the `scope` value. Examples:
    - `/workspacepreset/?scope=user` will return only user presets for a given user
    - `/workspacepreset?scope=system` will return only system presets
    - `/workspacepreset?scope=user,system` will return presets with scope `user` or `system`. By providing a comma-separated list, you can query multiple scope values. Note that only user presets created by the authenticated user will be returned.
- `search`: use this parameter to search for specific words in the `title` or `abstract` of a workspace. Examples:
    - `/workspacepreset/?search=radar` will return system presets (and user presets if user is authenticated) where "radar" occurs in the `title` or `abstract`
    - `/workspacepreset?search=radar,satellite` will return system presets (and user presets if user is authenticated) where "radar" **and** "satellite" occurs in the `title` or `abstract`

Note that these parameters can also be combined: For example:
- `/workspacepreset?scope=user&search=radar,satellite` will return user presets (if user is authenticated) where "radar" **and** "satellite" occurs in the `title` or `abstract`


Response example:

```json
[
  {
    "date": "2022-06-01T12:34:27.787184",
    "id": "Radar",
    "title": "Radar",
    "scope": "user",
    "viewType": "singleWindow"
  },
  {
    "date": "2022-06-01T12:34:27.787192",
    "id": "Temperature",
    "title": "Temperature",
    "scope": "system",
    "viewType": "singleWindow",
    "abstract": "This is an optional abstract"
  }
]
```

### GET /workspacepreset/`preset_id`

Returns the details of a workspace-preset:

`id: string`

`title: string`

`scope: “user” | “system”`

`abstract: string (optional, only if exists)`

`viewType: "singleWindow"| "multiWindow" | "tiledWindow"`

`views: [{ "mosaicNodeId": "mosaic-node-id-1", viewPresetId": "view-preset-id-1" }, ".."]}`

`syncGroups: {"id": string, "type": string}`

`mosaicNode: MosaicNodeObject`

Returns:
- 200 status and the contents of the requested workspace-preset (see example below)
- 400 status if an error occurred while reading the database
- 404 status if the requested workspace-preset id was not found

```json
{
  "id": "screenConfigRadarTemperature",
  "title": "Radar and temperature",
  "scope": "system",
  "viewType": "multiWindow",
  "views": [
    {
      "mosaicNodeId": "viewA",
      "viewPresetId": "Radar"
    },
    {
      "mosaicNodeId": "viewB",
      "viewPresetId": "Temperature"
    }
  ],
  "syncGroups": [
    {
      "id": "Area_radarTemp",
      "type": "SYNCGROUPS_TYPE_SETBBOX"
    },
    {
      "id": "Time_radarTemp",
      "type": "SYNCGROUPS_TYPE_SETTIME"
    }
  ],
  "mosaicNode": {
    "direction": "row",
    "first": "viewA",
    "second": "viewB"
  }
}
```


### POST /workspacepreset

Saves a workspace-preset and assigns it a new id (backend-generated).

Accepts a workspace-preset (same format as returned by GET /workspacepreset/`preset_id`).

```json
{
  "title": "New title ",
  "views": [{
            "mosaicNodeId": "view-preset-id-1",
            "viewPresetId": "user-1"
        }],
  "viewType": "singleWindow",
  "mosaicNode": "view-preset-id-1",
  "abstract": "This is an abstract",
  "scope": "user"
}
```

Returns:

- 200 status and an empty body (+ new id as the last part of a location header).
- 400 status
  - If another preset for the same user, or a system preset has the same title.
  - If the JSON is invalid (format has to be validated by the BE).
- 401 status
  - If no user is authenticated.

### PUT /workspacepreset/`preset_id`/

Saves a workspace-preset for a given id (updated by backend).

Accepts a workspace-preset (same format as returned by GET /workspacepreset/`preset_id`).

```json
{
  "title": "New title ",
  "views": [{ "mosaicNodeId": "mosaic-node-id-1", "viewPresetId": "view-preset-id-1"}],
  "viewType": "singleWindow",
  "mosaicNode": "mosaic-node-id-1",
  "abstract": "This is an abstract",
  "scope": "user"
}
```

Returns:

- 201 status and an empty body if update successful.
- 400 status
  - If another preset for the same user, or a system preset has the same title.
  - If the JSON is invalid (format has to be validated by the BE).
- 401 status
  - If no user is authenticated.

### DELETE /workspacepreset/`preset_id`

Deletes the workspace-preset owned by the currently authenticated user and identified by `preset_id`. No request body should be sent.

Returns:

- 204 HTTP status code if successfully deleted
- 401 HTTP status code if no user was authenticated
- 403 HTTP status code if the preset is not owned by the authenticated user, or the preset does not exist


## Datatypes

As mentioned above, the structure of initialProps may vary depending on the type of component.

### Component type Map: MapPresetInitialProps

Its initial props have two main fields:

`mapPreset: “Object”`

`syncGroupIds: list of string, optional`

```json
"initialProps": {
      "mapPreset": {
        "layers": [
          {
            "service": "https://geoservices.knmi.nl/adaguc-server?DATASET=uwcw_ha43_dini_5p5km",
            "name": "air_pressure_at_mean_sea_level_hagl",
            "layerType": "mapLayer",
            "enabled": true
          },
          {
            "service": "https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserver?dataset=RADAR",
            "name": "precipitation",
            "format": "image/png",
            "enabled": true,
            "style": "radar/nearest",
            "layerType": "mapLayer",
            "id": "RadarHarmoniePrecipSingleMapScreenConfig-radar"
          },
          {
            "service": "https://geoservices.knmi.nl/adaguc-server?DATASET=uwcw_ha43_dini_5p5km",
            "name": "total_precipitation_rate_hagl",
            "layerType": "mapLayer",
            "enabled": true
          },
        ],
        "proj": {
          "srs": "EPSG:3857",
          "bbox": {
            "left": -450651.2255879827,
            "bottom": 6393842.957153378,
            "right": 1428345.8183648037,
            "top": 7342085.640241
          }
        },
        "activeLayerId": "RadarHarmoniePrecipSingleMapScreenConfig-radar",
        "shouldAutoUpdate": true
      }
    }
```

## Database migration

The presets backend hosts a PostgreSQL database with default and customised presets. The database is deployed on AWS where snapshots are made on a daily basis. When changes are made to the presets database schema (e.g. adding or removing columns), the existing database should be updated without the loss of information already in it. Although a best practice is not to edit the table schema (and think beforehand about the schema), making alterations can be inevitable.

In this case, the database migration tool [Alembic](https://alembic.sqlalchemy.org/en/latest/) is used. All files related to database migration can be found in the folder [migrations](migrations). Below, the steps to create a new database migration are described:

### Steps to create a new database migration
1. Run ```alembic revision -m "Revision description"``` in the root folder to generate a new revision file
2. Open the generated revision file.
    * Check that the variable `down_revision` is correctly set, it should be identical to the previous revision ID
3. Complete the function `upgrade()`. Here you describe the database changes with respect to the previous revision.
4. You can complete the `downgrade()` function as well to revert the changes made by the `upgrade()` function if down-revision compatibility is desired.
5. Done! During startup of the backend, the command ```alembic upgrade head``` is used, ensuring that the latest database version is used. Additionally, you can play around with ```alembic upgrade/downgrade``` to verify that the correct changes are made during the migration.


### Some more background information

Alembic can be used for database migrations and can also help you to easily switch between different revisions. Some useful commands:
  * ```alembic current``` show the revision currently in use. If the current revision is equal to the latest revision, `(head)` is printed after the revision ID.
  * ```alembic history (--verbose)``` shows the migration history. The flag ```--verbose``` can be used to display extra information.
  * ```alembic upgrade head``` will migrate to the latest created database revision.
  * ```alembic upgrade [revision ID]``` will migrate to the specified revision ID.
  * ```alembic upgrade +n``` will upgrade to `n` new versions from the current version.
  * ```alembic downgrade base``` will downgrade to the beginning first database revision.
  * ```alembic downgrade -n``` will downgrade to `n` versions from the current.
  * ```alembic revision -m "Revision description"``` This will generate a new revision file in [migrations/versions](migrations/versions/).

In the folder [migrations/versions](migrations/versions/), revision files can be found that describe the database migration:
  * Each migration has a unique Revision ID that is identified with the variable `revision`. The previous revision is identified by the variable `down_revision`. With these variables, all revision files can be linked in order to easily switch between migrations.
  * As stated above, by running ```alembic revision -m "Revision description"``` a new revision file is generated. This revision is the most recent one, so it can be identified as `head`.
  * In each revision file, two functions can be found: `upgrade` and `downgrade`. The `upgrade` function describes how the database changes with respect to the previous revision. The `downgrade` function reverts the changes made with `upgrade` to restore the database to the previous revision.
  * During startup of the backend, the command ```alembic upgrade head``` is used, ensuring that the latest database revision is used.

  For more information, see the [Alembic documentation](https://alembic.sqlalchemy.org/en/latest/).


### Backup system

#### CLI commands for backup and restore

A backup and restore system is available to save the contents of the database to a json file. 

Presets for all users, or presets for a specific user can be restored. 

CLI command to make a backup manually: 
- Command: `python cli.py backup --file contentspresetsdb.json`
- Or with docker: `docker exec -it presets-backend bash -c "python cli.py backup --file /backups/contentspresetsdb.json"`

CLI command to restore a backup:
- Command: `python cli.py restore --file contentspresetsdb.json`
- Or with docker: `docker exec -it presets-backend bash -c "python cli.py restore --file /backups/contentspresetsdb.json"`

CLI command to restore a backup for a specific user (use global for system presets):
- Command: `python cli.py restore --file contentspresetsdb.json --username geoweb`


After this the database changes are restored. 

The restore will not delete anything from the database. It will only update or insert presets from the backup file.

#### Automatic hourly backups

The system can be configured to make automatic backups every hour when the `BACKUP_PATH` environment variable is set. The system will keep backup files from now till two weeks ago in that folder.

To enable generation of hourly backups, set the environment variable named `BACKUP_PATH`:

```
export BACKUP_PATH=`pwd`/backups/
```

Backups of the database will then be written to a json file in the configured path. This is enabled in the docker-compose variant by default.


#### Backup API ####

An API is available to list all available backups and to restore a specific backup. Optionally a specific user can be restored.

First make sure you can authenticate and are authorized to perform this action. Set you OAuth bearer token by:

```
export OAUTHTOKEN="Bearer eyJr........"
```

Next, list all backup files:
```
curl -kL "https://localhost:4443/backup/list" -H "Authorization: ${OAUTHTOKEN}" | python -m json.tool
```

Make a backup
```
curl -kL "https://localhost:4443/backup/make" -H "Authorization: ${OAUTHTOKEN}" | python -m json.tool
```


Restore a backup:
```
curl -kL "https://localhost:4443/backup/restore?backup=backuppresetsdb_2024_10_21T00_00_00.json" -H "Authorization: ${OAUTHTOKEN}" | python -m json.tool
```

Or restore presets for a specific user:
```
curl -kL "https://localhost:4443/backup/restore?backup=backuppresetsdb_2024_10_21T00_00_00.json&user=maarten.plieger" -H "Authorization: ${OAUTHTOKEN}" | python -m json.tool
```