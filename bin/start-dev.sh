#!/bin/bash

uvicorn --host 0.0.0.0 \
    --port=${PRESETS_PORT_HTTP:-8080} \
    --workers=1 \
    --no-access-log \
    --reload \
    "app.main:app"
