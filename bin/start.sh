#!/bin/bash

uvicorn --host 0.0.0.0 \
    --port=${PRESETS_PORT_HTTP:-8080} \
    --workers=4 \
    --access-log \
    --no-use-colors \
    "app.main:app"
