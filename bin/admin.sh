#!/bin/bash
set -e

python cli.py enable-alembic
alembic upgrade head
python cli.py seed
